#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "mgmres.h"
#include "mgmres_csrvi.h"
#include "mgmres_csrvi_ilp.h"
#include "mgmres_csr_vector.h"
#include "mgmres_csrvi_vector.h"
#include "mgmres_csrvi_avx.h"
#include "mgmres_csr_avx.h"

/***
 *      _____            _                 _   _
 *     |  __ \          | |               | | (_)
 *     | |  | | ___  ___| | __ _ _ __ __ _| |_ _  ___  _ __  ___
 *     | |  | |/ _ \/ __| |/ _` | '__/ _` | __| |/ _ \| '_ \/ __|
 *     | |__| |  __/ (__| | (_| | | | (_| | |_| | (_) | | | \__ \
 *     |_____/ \___|\___|_|\__,_|_|  \__,_|\__|_|\___/|_| |_|___/
 *
 *
 */

// Bounds
//const int32_t itr_max = 20;
const int32_t itr_max = 1;
int32_t mr = 10;
const double tol_abs = 1.0E-09;
const double tol_rel = 1.0E-09;

#define PRINT_MATRIX 0
#define PRINT_SOLUTION 0
#define PRINT_COLORS 1
#define EQUALITY_THRESHOLD 1.0
#define CONVERGENCE_THRESHOLD 1.0

#define GRN  "\x1B[32m"
#define RED  "\x1B[31m"
#define RESET "\x1B[0m"

#ifdef PANALYSIS
uint64_t flops;
#endif

int main();
void show_all_options();

/* Debugging Tools */
void st_print_list(int32_t *ia_st, int32_t *ja_st, double *a_st, const int32_t nnz);
void csr_print_list(const int32_t *ia_csr, const int32_t *ja_csr, const double *a_csr, const int32_t m,
                    const int32_t nnz);
void csrvi_print_list(const int32_t *ia_csr, const int32_t *ja_csr, const int32_t *va_csr, const double *vals, const int32_t m,
                      const int32_t nnz, const int32_t nd);
void st_print_table(const int32_t *ia_st, const int32_t *ja_st, const double *a_st, const int32_t m, const int32_t n,
                    const int32_t nnz);
void print_solution(const double *x, const int32_t n);
double l2_square(const double *x, const double *y, const int32_t n);
double l_inf(const double *x, const double *y, const int32_t n);
int32_t hasNaN(const double*x, const int32_t n);

/* Formats + Conversion */
int32_t binary_search(const double *a, const int32_t i, const int32_t j, const double v);
void st_sort(int32_t *ia_st, int32_t *ja_st, double *a_st, const int32_t nnz);
void st_to_csr(const int32_t *ia_st, const int32_t *ja_st, const double *a_st, int32_t **ia_csr, int32_t *ja_csr,
               double *a_csr, const int32_t nnz);
int32_t csr_to_csrvi(const int32_t *ia, const int32_t *ja, const double *a, int32_t *ib, int32_t *jb, int32_t *vb,
                     double **vals, const int32_t m, const int32_t nnz);
int32_t st_to_csrvi(const int32_t *ia_st, const int32_t *ja_st, const double *a_st, int32_t *ib_csrvi, int32_t *jb_csrvi,
                 int32_t *vb_csrvi, double **vals_csrvi, const int32_t m, const int32_t nnz);


int comp_idx_asc(const void *i, const void *j);
int comp_asc(const void *a, const void *b);

/* Matrix generation */
int32_t nz_num_frank(const int32_t n);
int32_t nz_num_band(const int32_t n, const int32_t n_ld, const int32_t n_ud);

void frank_system_st(const int32_t n, int32_t *ia_st, int32_t *ja_st, double *a_st, double *rhs, double *x_estimate);
void band_system_st(const int32_t n, const int32_t n_ld, const int32_t n_ud, int32_t *ia_st, int32_t *ja_st, double *a_st, double *rhs, double *x_estimate);

void test_group_0();

void test_group_frank_csrvi();
void test_group_band_csrvi();
void test_group_band_csrvi_ilp();
void test_group_band_time();
void test_group_band_time_preserve_sparsity();
void test_group_band_correctness_ilp();
void test_group_band_correctness_avx_csrvi();
void test_group_band_correctness_avx_csr();
void test_group_band_correctness_sse_csrvi();
void test_group_band_correctness_sse_csr();
/******************************************************************************/
/***
 *      __  __       _
 *     |  \/  |     (_)
 *     | \  / | __ _ _ _ __
 *     | |\/| |/ _` | | '_ \
 *     | |  | | (_| | | | | |
 *     |_|  |_|\__,_|_|_| |_|
 *
 *
 */
 
int main(int argc, char **argv)
/*
  Purpose:

    MAIN is the main program for MGMRES_PRB.

  Discussion:

    MGMRES_PRB tests the MGMRES library.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    16 July 2007

  Author:

    John Burkardt
*/
{
	#ifdef PANALYSIS
	flops = 0;
    #endif

    // Disable quicksort
    use_quicksort = 0;
    if (use_quicksort)
        printf("QUICKSORT ENABLED\n");

    if (argc == 1) {
        printf("OPTION: empty\n");
        show_all_options();
        return -1;
    }
    else if (argc == 2 && strcmp(argv[1], "-show") == 0) {
        printf("OPTION: -show\n");
        show_all_options();
        return -1;
    }
    else if (argc==2 && strcmp(argv[1], "-test_frank_csrvi") == 0){
        printf("OPTION: -test_frank_csrvi\n");

        printf("Test Group \"Frank\" - mgres_st vs mgres_csrvi\n");
        printf("========================================================\n");
        test_group_frank_csrvi();
    }
    else if (argc==2 && strcmp(argv[1], "-test_band_csrvi") == 0){
        printf("OPTION: -test_band_csrvi\n");
    
        printf("Test Group \"Band\" - mgres_st vs mgres_csrvi\n");
        printf("========================================================\n");
        test_group_band_csrvi();
    }
    else if (argc==2 && strcmp(argv[1], "-test_band_csrvi_ilp") == 0){
        printf("OPTION: -test_band_csrvi_ilp\n");

        printf("Test Group \"Band\" - mgres_st vs mgres_csrvi\n");
        printf("========================================================\n");
        test_group_band_csrvi_ilp();
    }
    else if (argc==2 && strcmp(argv[1], "-test_band_time") == 0){
        printf("OPTION: -test_band_time\n");

        printf("Timing test");
        printf("========================================================\n");
        test_group_band_time();
    }
    else if (argc==2 && strcmp(argv[1], "-test_band_time_preserve_sparsity") == 0){
        printf("OPTION: -test_band_time\n");

        printf("Timing test");
        printf("========================================================\n");
        test_group_band_time_preserve_sparsity();
    }
    else if (argc==2 && strcmp(argv[1], "-test_band_csrvi_ilp_correctness") == 0){
        printf("OPTION: -test_band_csrvi_ilp_correctness\n");

        printf("ILP correctness test");
        printf("========================================================\n");
        test_group_band_correctness_ilp();
    }
    else if (argc==2 && strcmp(argv[1], "-csrvi_avx_correctness") == 0){
        printf("OPTION: -csrvi_avx_correctness\n");

        printf("ILP AVX (CSR-VI) correctness test");
        printf("========================================================\n");
        test_group_band_correctness_avx_csrvi();
    }
    else if (argc==2 && strcmp(argv[1], "-csr_avx_correctness") == 0){
        printf("OPTION: -csr_avx_correctness\n");

        printf("ILP AVX (CSR) correctness test");
        printf("========================================================\n");
        test_group_band_correctness_avx_csr();
    }
    else if (argc==2 && strcmp(argv[1], "-csrvi_sse_correctness") == 0){
        printf("OPTION: -csrvi_sse_correctness\n");

        printf("SSE (CSR-VI) correctness test");
        printf("========================================================\n");
        test_group_band_correctness_sse_csrvi();
    }
    else if (argc==2 && strcmp(argv[1], "-csr_sse_correctness") == 0){
        printf("OPTION: -csr_sse_correctness\n");

        printf("SSE (CSR) correctness test");
        printf("========================================================\n");
        test_group_band_correctness_sse_csr();
    }
    else {
        printf("OPTION: unknown option\n");
        show_all_options();
        return -1;
    }

    return 0;
}

/*
 * [FastCode] Show all options
 */
void show_all_options() {
    printf("-show: Show all options.\n");
    printf("-test_band_csrvi_ilp_correctness: Correctness tests for ILP optimizations.\n");
    printf("-csr_sse_correctness: Correctness tests for SSE vectorization using CSR format.\n");
    printf("-csrvi_sse_correctness: Correctness tests for SSE vectorization using CSR-VI format.\n");
    printf("-csr_avx_correctness: Correctness tests for AVX vectorization using CSR format.\n");
    printf("-csrvi_avx_correctness: Correctness tests for AVX vectorization using CSR-VI format.\n");
    printf("-test_band_time: Runtime evaluation of all optimizations.\n");
    printf("-test_band_time_preserve_sparsity: Runtime evaluation of ILP optimizations and band matrices with configurable sparsity levels.\n");
}


/***
 *      _____       _                       _               _______          _
 *     |  __ \     | |                     (_)             |__   __|        | |
 *     | |  | | ___| |__  _   _  __ _  __ _ _ _ __   __ _     | | ___   ___ | |___
 *     | |  | |/ _ \ '_ \| | | |/ _` |/ _` | | '_ \ / _` |    | |/ _ \ / _ \| / __|
 *     | |__| |  __/ |_) | |_| | (_| | (_| | | | | | (_| |    | | (_) | (_) | \__ \
 *     |_____/ \___|_.__/ \__,_|\__, |\__, |_|_| |_|\__, |    |_|\___/ \___/|_|___/
 *                               __/ | __/ |         __/ |
 *                              |___/ |___/         |___/
 */

/*
 * [FastCode] Print matrix in ST ("Sparse Triplet") format in list form
 */
void st_print_list(int32_t *ia_st, int32_t *ja_st, double *a_st, const int32_t nnz) {

    printf("\n\n");
    int32_t i;
    for (i = 0; i < nnz; i++) {
        printf("(%d, %d) => %f\n", ia_st[i], ja_st[i], a_st[i]);
    }
    printf("\n\n");
}

/*
 * [FastCode] Print matrix in CSR ("Compressed sparse row") format in list form
 * INPUT: ia_csr: Array of row indices (compressed, length: m + 1)
 *        ja_csr: Array of column indices (length: nnz)
 *         a_csr: Array of values (length: nnz)
 *             m: Number of rows that contain -at least- one non-zero element
 */
void csr_print_list(const int32_t *ia_csr, const int32_t *ja_csr, const double *a_csr, const int32_t m,
                    const int32_t nnz) {
    printf("\n\n");
    int32_t i;
    printf("Printing matrix in CSR form..\n");
    printf("a: ");
    for (i = 0; i < nnz; i++) {
        printf("%.2f ", a_csr[i]);
    }
    printf("\n");

    printf("ja: ");
    for (i = 0; i < nnz; i++) {
        printf("%d ", ja_csr[i]);
    }
    printf("\n");

    printf("ia: ");
    for (i = 0; i < m + 1; i++) {
        printf("%d ", ia_csr[i]);
    }
    printf("\n\n");
}

/*
 * [FastCode] Print matrix in CSR-VI ("Compressed sparse row" + Value Compression) format in list form
 * INPUT: ia_csr: Array of row indices (compressed, length: m + 1)
 *        ja_csr: Array of column indices (length: nnz)
 *        va_csr: Array of values indices(length: nnz)
 *          vals: Array of values (length: nd)
 *             m: Number of rows that contain -at least- one non-zero element
 *           nnz: Number of non-zero elements
 *            nd: Number of distinct non-zero elements
 */
void csrvi_print_list(const int32_t *ia_csr, const int32_t *ja_csr, const int32_t *va_csr, const double *vals, const int32_t m,
                    const int32_t nnz, const int32_t nd) {
    printf("\n\n");
    int32_t i;
    printf("Printing matrix in CSR-VI form..\n");
    printf("vals: ");
    for (i = 0; i < nd; i++) {
        printf("%.2f ", vals[i]);
    }
    printf("\n");

    printf("va: ");
    for (i = 0; i < nnz; i++) {
        printf("%d ", va_csr[i]);
    }
    printf("\n");

    printf("ja: ");
    for (i = 0; i < nnz; i++) {
        printf("%d ", ja_csr[i]);
    }
    printf("\n");

    printf("ia: ");
    for (i = 0; i < m + 1; i++) {
        printf("%d ", ia_csr[i]);
    }
    printf("\n\n");
}

/*
 * [FastCode] Print matrix in ST ("Sparse Triplet") format in table form
 * INPUT: ia_st: Array of row pointers
 *        ja_st: Array of column indices (length: nnz)
 *         a_st: Array of matrix values (length: nnz)
 *         m: Number of rows of matrix
 *         n: Number of columns of matrix
 *       nnz: Number of nonzero values
 *
 */
void st_print_table(const int32_t *ia_st, const int32_t *ja_st, const double *a_st, const int32_t m, const int32_t n,
                    const int32_t nnz) {
    assert (n > 0 && m > 0 && nnz > 0);
    assert (nnz <= m * n);
    printf("\n\n");

    // Allocate backups
    int32_t *ia_st_cp = (int32_t *) malloc(nnz * sizeof(int32_t));
    int32_t *ja_st_cp = (int32_t *) malloc(nnz * sizeof(int32_t));
    double *a_st_cp = (double *) malloc(nnz * sizeof(double));

    // Copy values over
    memcpy(ia_st_cp, ia_st, nnz * sizeof(int32_t));
    memcpy(ja_st_cp, ja_st, nnz * sizeof(int32_t));
    memcpy(a_st_cp, a_st, nnz * sizeof(double));


    // Sort in ascending row order
    st_sort(ia_st_cp, ja_st_cp, a_st_cp, nnz);

    //printf("Printing %d x %d matrix... \n", m, n);

    int32_t k = 0;
    int32_t u, v;
    for (u = 0; u < m; u++) {
        for (v = 0; v < n; v++) {

            if (u == ia_st_cp[k]) { // There is one non-zero entry in row
                if (v < ja_st_cp[k]) { // We are currently left of it
                    printf("%10.2f", 0.0);
                } else if (v == ja_st_cp[k]) {
                    if (PRINT_COLORS) {
                        printf(GRN "%10.2f" RESET, a_st_cp[k]);
                    } else {
                        printf("%10.2f", a_st_cp[k]);
                    }
                    k++;
                }
            } else if (u < ia_st_cp[k]) { // Future row has at least one non-zero entry
                printf("%10.2f", 0.0);
            }
        }

        printf("\n");
    }

    // Free backups
    free(ia_st_cp);
    free(ja_st_cp);
    free(a_st_cp);

    printf("\n\n");
}

void print_solution(const double *x, const int32_t n) {
    printf("[SOL]:\n");
    int32_t i;
    for (i = 0; i < n; i++) {
        printf("\t\t%.2f\n", x[i]);
    }
}

/***
 *      ______                         _                  _____                              _
 *     |  ____|                       | |          _     / ____|                            (_)
 *     | |__ ___  _ __ _ __ ___   __ _| |_ ___   _| |_  | |     ___  _ ____   _____ _ __ ___ _  ___  _ __
 *     |  __/ _ \| '__| '_ ` _ \ / _` | __/ __| |_   _| | |    / _ \| '_ \ \ / / _ \ '__/ __| |/ _ \| '_ \
 *     | | | (_) | |  | | | | | | (_| | |_\__ \   |_|   | |___| (_) | | | \ V /  __/ |  \__ \ | (_) | | | |
 *     |_|  \___/|_|  |_| |_| |_|\__,_|\__|___/          \_____\___/|_| |_|\_/ \___|_|  |___/_|\___/|_| |_|
 *
 *
 */

// Temporary arrays used for sorting
int32_t *tmp_ia;
int32_t *tmp_ja;

/*
 * [FastCode] Comparator that is used to retain indices (ASC order)
 */
int comp_idx_asc(const void *i, const void *j) {
    int *u = (int *) i;
    int *v = (int *) j;

    if (tmp_ia[*u] == tmp_ia[*v]) {
        return tmp_ja[*u] - tmp_ja[*v];
    } else {
        return tmp_ia[*u] - tmp_ia[*v];
    }
}


/*
 * [FastCode] Handle to sort in ascending order
 */
int comp_asc(const void *a, const void *b) {
    double *x = (double *) a;
    double *y = (double *) b;

    if (*x < *y) {
        return -1;
    } else if (*x > *y) {
        return 1;
    } else {
        return 0;
    }
}


/*
 * [FastCode] Binary search
 *
 * INPUT: a: Array of sufficient length (i.e. a[j-1] is not out of bounds)
 *        i: Left end of sub-array (inclusive)
 *        j: Right end of sub-array (exclusive)
 *        v: Element to search for
 *
 * OUTPUT: p: Index of value v in array a if v exists, otherwise -1.
 */
int32_t binary_search(const double *a, const int32_t i, const int32_t j, const double v) {
    assert (i <= j);

    int32_t m;
    if (i == j) {
        // Not found
        return -1;
    } else if (i + 1 == j) {
        // One element
        return (a[i] == v) ? i : -1;
    } else {
        // Central element
        m = (i + j) / 2;

        if (v < a[m]) {
            // Recurse left
            return binary_search(a, i, m, v);
        } else {
            // Recurse right
            return binary_search(a, m, j, v);
        }
    }
}


/*
 * [FastCode] Sort matrix in ST("Sparse triplet") format in ascending row order
 */
void st_sort(int32_t *ia_st, int32_t *ja_st, double *a_st, const int32_t nnz) {

    // Allocate index array
    int32_t *idx = malloc(nnz * sizeof(int32_t));

    int32_t i;
    for (i = 0; i < nnz; i++) {
        idx[i] = i;
    }

    // Make arrays visible
    tmp_ia = ia_st;
    tmp_ja = ja_st;

    // Sort (retain indices)
    qsort(idx, nnz, sizeof(int32_t), comp_idx_asc);

    // Allocate backup arrays
    int32_t *ia_st_cp = (int32_t *) malloc(nnz * sizeof(int32_t));
    int32_t *ja_st_cp = (int32_t *) malloc(nnz * sizeof(int32_t));
    double *a_st_cp = (double *) malloc(nnz * sizeof(double));

    // Copy values over
    memcpy(ia_st_cp, ia_st, nnz * sizeof(int32_t));
    memcpy(ja_st_cp, ja_st, nnz * sizeof(int32_t));
    memcpy(a_st_cp, a_st, nnz * sizeof(double));

    int32_t k;
    int32_t curr_idx;
    for (k = 0; k < nnz; k++) {
        curr_idx = idx[k];

        // Permute
        ia_st[k] = ia_st_cp[curr_idx];
        ja_st[k] = ja_st_cp[curr_idx];
        a_st[k] = a_st_cp[curr_idx];
    }

    // Free backup arrays
    free(ia_st_cp);
    free(ja_st_cp);
    free(a_st_cp);
}

/*
 * [FastCode] Convert matrix in ST("Sparse triplet", not necessarily sorted) format to matrix in CSR format ("Compressed sparse row")
 *
 * INPUT: ia_st: Row indices of non-zero values (length: nnz)
 *        ja_st: Column indices of non-zero values (length: nnz)
 *         a_st: Non-zero values (length: nnz)
 *          nnz: Number of non-zero values
 *
 * OUTPUT: ia_csr: Array of row indices (compressed) (length: [INPUT nnz | OUTPUT nd + 1])
 *         ja_csr: Array of column indices (length: nnz)
 *          a_csr: Non-zero values (length: nnz)
 *
 * where 'nd' is the number of rows that contain non-zero elements
 */
void st_to_csr(const int32_t *ia_st, const int32_t *ja_st, const double *a_st, int32_t **ia_csr, int32_t *ja_csr,
               double *a_csr, const int32_t nnz) {
    assert(nnz > 0);

    // Allocate backup arrays
    int32_t *ia_st_cp = (int32_t *) malloc(nnz * sizeof(int32_t));
    int32_t *ja_st_cp = (int32_t *) malloc(nnz * sizeof(int32_t));
    double *a_st_cp = (double *) malloc(nnz * sizeof(double));

    // Copy values over
    memcpy(ia_st_cp, ia_st, nnz * sizeof(int32_t));
    memcpy(ja_st_cp, ja_st, nnz * sizeof(int32_t));
    memcpy(a_st_cp, a_st, nnz * sizeof(double));

    // Sort ST matrix (in ascending row order)
    st_sort(ia_st_cp, ja_st_cp, a_st_cp, nnz);

    // Copy unchanged arrays into result
    memcpy(a_csr, a_st_cp, nnz * sizeof(double));
    memcpy(ja_csr, ja_st_cp, nnz * sizeof(int32_t));

    // Construct array of row indices
    int32_t i;
    int32_t row;

    int32_t k = 0;
    int32_t last_row = -1;
    for (i = 0; i < nnz; i++) {
        row = ia_st_cp[i];

        if (row != last_row) { // New row
            (*ia_csr)[k++] = i;

            last_row = row;
        }
    }

    // Link dummy row
    (*ia_csr)[k] = nnz;

    // Compress row indices array
    *ia_csr = realloc(*ia_csr, (k + 1) * sizeof(int32_t));

    // Free backup arrays
    free(ia_st_cp);
    free(ja_st_cp);
    free(a_st_cp);

}


/*
 * [FastCode] Convert matrix in CSR("Compressed sparse row") format to matrix in CSR-VI format("CSR + Value Compression")
 *
 * INPUT: ia: Array of row pointers (length: m + 1)
 *        ja: Array of column indices (length: nnz)
 *         a: Array of matrix values (length: nnz)
 *       nnz: Number of nonzero values
 *         m: Number of rows of matrix
 *
 * OUTPUT: ib: Array of row pointers (length: m + 1)
 *         jb: Array of column indices (length: nnz)
 *         vb: Array of value pointers (point to one value of array given by value_ptr) (length nnz)
 *       vals: Distinct set of values that are contained in the input matrix (length [INPUT: nnz | OUTPUT: nd])
 *
 * RETURNS: nd: Number of distinct values
 */
int32_t csr_to_csrvi(const int32_t *ia, const int32_t *ja, const double *a, int32_t *ib, int32_t *jb, int32_t *vb,
                     double **vals, const int32_t m, const int32_t nnz) {

    // Column indices are the same in both formats
    memcpy(ib, ia, (m + 1) * sizeof(int32_t));

    // Same applies to row pointers
    memcpy(jb, ja, nnz * sizeof(int32_t));

    // Copy values
    double *a_asc = (double *) malloc(nnz * sizeof(double));
    memcpy(a_asc, a, nnz * sizeof(double));

    // Sort
    qsort(a_asc, nnz, sizeof(double), comp_asc);

    // Count distinct values
    int32_t i;
    int32_t nd = 0;
    for (i = 0; i < nnz; i++) {
        if (i == 0 || a_asc[i] != a_asc[i - 1]) {
            (*vals)[nd++] = a_asc[i];
        }
    }

    // Free temporary array
    free(a_asc);

    // Shrink array
    *vals = (double *) realloc(*vals, nd * sizeof(double));

    // Link array of value pointers with values
    double v;
    int32_t pos;
    for (i = 0; i < nnz; i++) {
        v = a[i];

        // Find position of value in distinct value array (which is sorted)
        pos = binary_search(*vals, 0, nd, v);

        // Store index
        vb[i] = pos;
    }

    // Return number of distinct values
    return nd;
}


/*
 * [FastCode] Convert matrix in ST("Sparse triplet", not necessarily sorted) format to matrix in CSR-VI format ("Compressed sparse row")
 *
 * INPUT: ia_st: Row indices of non-zero values (length: nnz)
 *        ja_st: Column indices of non-zero values (length: nnz)
 *         a_st: Non-zero values (length: nnz)
 *          nnz: Number of non-zero values
 *
 * OUTPUT: ib_csrvi: Array of row pointers
 *         jb_csrvi: Array of column indices (length nnz)
 *         vb_csrvi: Array of value pointers (point to one value of array given by value_ptr) (length nnz)
 *       vals_csrvi: Distinct set of values that are contained in the input matrix (length [INPUT: nnz | OUTPUT: nd])
 *
 * RETURNS: nd: number of rows that contain at least one non-zero element
 */
int32_t st_to_csrvi(const int32_t *ia_st, const int32_t *ja_st, const double *a_st, int32_t *ib_csrvi, int32_t *jb_csrvi,
                 int32_t *vb_csrvi, double **vals_csrvi, const int32_t m, const int32_t nnz) {
    assert(nnz > 0);

    // Convert to CSR
    int32_t *ia_csr = (int32_t *) malloc((nnz + 1) * sizeof(int32_t));
    int32_t *ja_csr = (int32_t *) malloc(nnz * sizeof(int32_t));
    double *a_csr = (double *) malloc(nnz * sizeof(double));

    st_to_csr(ia_st, ja_st, a_st, &ia_csr, ja_csr, a_csr, nnz);

    // Convert to CSR-VI
    int32_t nd = csr_to_csrvi(ia_csr, ja_csr, a_csr, ib_csrvi, jb_csrvi, vb_csrvi, vals_csrvi, m, nnz);

    // Free memory
    free(ia_csr);
    free(ja_csr);
    free(a_csr);

    return nd;
}


/***
 *      _______        _      _____
 *     |__   __|      | |    / ____|
 *        | | ___  ___| |_  | |  __ _ __ ___  _   _ _ __  ___
 *        | |/ _ \/ __| __| | | |_ | '__/ _ \| | | | '_ \/ __|
 *        | |  __/\__ \ |_  | |__| | | | (_) | |_| | |_) \__ \
 *        |_|\___||___/\__|  \_____|_|  \___/ \__,_| .__/|___/
 *                                                 | |
 *                                                 |_|
 */

/*
 * [FastCode] Test group 1 is composed of small size matrices and uniform solution.
 */
void test_group_frank_csrvi() {
    int32_t sizes[] = {64, 128, 256, 512, 1024, 2048, 4096};
    //int32_t sizes[] = {8, 12, 40, 60};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {
        // Size
        int32_t n = sizes[nIdx];

        int32_t nz_num = nz_num_frank(n);
        printf("N = %d:\n", n);

        printf("[MEM]: Allocating memory for matrix in ST format..");
        // Equation system definition
        double *a_st = (double *) malloc(nz_num * sizeof(double));
        int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *rhs = (double *) malloc(n * sizeof(double));
        double *x_estimate = (double *) malloc(n * sizeof(double));
        printf("DONE\n");

        // Fill matrix
        frank_system_st(n, ia_st, ja_st, a_st, rhs, x_estimate);

        // Convert to CSR-VI format
        printf("[MEM]: Allocating memory for matrix in CSR-VI format..");
        int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
        int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *vala_csrvi = (double *) malloc (nz_num * sizeof(double));
        printf("DONE\n");

        printf("[MEM]: Converting from ST to CSR-VI..");
        int32_t nd = st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi, n, nz_num);
        printf("DONE\n");

        // Free memory used for ST format
        printf("[MEM]: Freeing memory used for ST format..");
        printf("DONE\n");

        // Print CSR-VI
        if (PRINT_MATRIX) {
            csrvi_print_list(ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, n, nz_num, nd);
        }

        printf("[EXE]: Executing PMGMRES solver..");
        pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
        //mgmres_st(n, nz_num, ia_st, ja_st, a_st, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
        printf("DONE\n");

        free(a_st);
        free(ia_st);
        free(ja_st);

        // Print solution
        if (PRINT_SOLUTION) {
            print_solution(x_estimate, n);
        }

        // Solve validate
        validate_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);

        // Free memory
        printf("[MEM]: Freeing memory used for CSR-VI format..");
        free(ia_csrvi);
        free(ja_csrvi);
        free(va_csrvi);
        free(vala_csrvi);

        free(rhs);
        free(x_estimate);
        printf("DONE\n");

        printf("\n");
        printf("========================================================\n");
    }
}

/*
 * [FastCode] Test group 1 is composed of small size matrices and uniform solution.
 */
void test_group_band_csrvi() {
    int32_t sizes[] = {8, 128, 256, 512, 1024, 2048, 4096};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    int32_t n_ld = 2;

    // Upper diagonals
    int32_t n_ud = 2;

    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {
        // Size
        int32_t n = sizes[nIdx];

        int32_t nz_num = nz_num_band(n, n_ld, n_ud);
        printf("N = %d:\n", n);

        // Iteration counter(s)
        int32_t i, j, k;
        int32_t base;

        printf("[MEM]: Allocating memory for matrix in ST format..");
        // Equation system definition
        double *a_st = (double *) malloc(nz_num * sizeof(double));
        int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *rhs = (double *) malloc(n * sizeof(double));
        double *x_estimate = (double *) malloc(n * sizeof(double));
        printf("DONE\n");

        // Fill matrix
        band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);

        // Convert to CSR-VI format
        printf("[MEM]: Allocating memory for matrix in CSR-VI format..");
        int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
        int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *vala_csrvi = (double *) malloc (nz_num * sizeof(double));
        printf("DONE\n");

        printf("[MEM]: Converting from ST to CSR-VI..");
        int32_t nd = st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi, n, nz_num);
        printf("DONE\n");

        // Free memory used for ST format
        printf("[MEM]: Freeing memory used for ST format..");
        free(a_st);
        free(ia_st);
        free(ja_st);
        printf("DONE\n");

        // Print CSR-VI
        if (PRINT_MATRIX) {
            csrvi_print_list(ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, n, nz_num, nd);
        }

        printf("[EXE]: Executing PMGMRES solver..");
        //pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
        validate_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
        printf("DONE\n");

        // Print solution
        if (PRINT_SOLUTION) {
            print_solution(x_estimate, n);
        }

        // Free memory
        printf("[MEM]: Freeing memory used for CSR-VI format..");
        free(ia_csrvi);
        free(ja_csrvi);
        free(va_csrvi);
        free(vala_csrvi);

        free(rhs);
        free(x_estimate);
        printf("DONE\n");

        printf("\n");
        printf("========================================================\n");
    }
}

/*
 * [FastCode] Test group 1 is composed of small size matrices and uniform solution.
 */
void test_group_band_csrvi_ilp() {
    int32_t sizes[] = {8, 128, 256, 512, 1024, 2048, 4096};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    int32_t n_ld = 20;

    // Upper diagonals
    int32_t n_ud = 20;

    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {
        // Size
        int32_t n = sizes[nIdx];

        int32_t nz_num = nz_num_band(n, n_ld, n_ud);
        printf("N = %d:\n", n);

        printf("[MEM]: Allocating memory for matrix in ST format..");
        // Equation system definition
        double *a_st = (double *) malloc(nz_num * sizeof(double));
        int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *rhs = (double *) malloc(n * sizeof(double));
        double *x_estimate = (double *) malloc(n * sizeof(double));
        printf("DONE\n");

        // Fill matrix
        band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);

        // Convert to CSR-VI format
        printf("[MEM]: Allocating memory for matrix in CSR-VI format..");
        int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
        int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *vala_csrvi = (double *) malloc (nz_num * sizeof(double));
        printf("DONE\n");

        printf("[MEM]: Converting from ST to CSR-VI..");
        int32_t nd = st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi, n, nz_num);
        printf("DONE\n");

        // Free memory used for ST format
        printf("[MEM]: Freeing memory used for ST format..");
        free(a_st);
        free(ia_st);
        free(ja_st);
        printf("DONE\n");

        // Print CSR-VI
        if (PRINT_MATRIX) {
            csrvi_print_list(ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, n, nz_num, nd);
        }

        printf("[EXE]: Executing PMGMRES solver..");
        // TODO: Add to validator
        pmgmres_ilu_csrvi_ilp_unroll(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs,
                                     itr_max, mr, tol_abs, tol_rel);
        //validate_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
        printf("..DONE\n");

        // Print solution
        if (PRINT_SOLUTION) {
            print_solution(x_estimate, n);
        }

        // Free memory
        printf("[MEM]: Freeing memory used for CSR-VI format..");
        free(ia_csrvi);
        free(ja_csrvi);
        free(va_csrvi);
        free(vala_csrvi);

        free(rhs);
        free(x_estimate);
        printf("DONE\n");

        printf("\n");
        printf("========================================================\n");
    }
}

inline double l2_square(const double *x, const double *y, const int32_t n) {

    double sum = 0.0;
    int32_t i;
    for (i = 0; i < n; i++) {
        sum += (x[i] - y[i]) * (x[i] - y[i]);
    }

    return sum;
}

inline double l_inf(const double *x, const double *y, const int32_t n) {

    double max_dist = 0.0;
    int32_t max_index = 0;
    int32_t i;

    for (i = 0; i < n; i++) {
        double dist = fabs(x[i] - y[i]);

        if (dist > max_dist) {
            max_index = i;
            max_dist = dist;
        }

    }

    return max_dist;
}

inline int32_t hasNaN(const double *x, const int32_t n) {

    int32_t i;

    for (i = 0; i < n; i++) {
        if (x[i] != x[i]) {
            return 1;
        }
    }

    return 0;
}


/*
 * [FastCode] Evaluate runtime of all available ILP levels with respect to a test set
 */
void test_group_band_time() {

    // Disable BUFFERING
    //setbuf(stdout, NULL);

    // Unroll modes (0 denotes CSR baseline, 100 denotes SSE (CSR-VI), 200 SSE (CSR) , 300 AVX (CSR-VI) and 400 AVX(CSR))
    //int32_t modes[] = {0, 1, 4, 8, 16, 32, 100, 200, 300, 400};
    int32_t modes[] = {1};
    int32_t sizesMd = sizeof(modes) / sizeof(modes[0]);

    //int32_t sizes[] = {128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072};
    int32_t sizes[] = {32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    //int32_t n_lds[] = {32, 64, 48,  62};
    //int32_t n_lds[] = {62};
    int32_t n_lds[] = {16};
    int32_t sizesLd = sizeof(n_lds) / sizeof(n_lds[0]);

    // Upper diagonals
    //int32_t n_uds[] = {32, 64, 48, 99};
    //int32_t n_uds[] = {62};
    int32_t n_uds[] = {16};
    int32_t sizesUd = sizeof(n_uds) / sizeof(n_uds[0]);

    // Average speedups
    double *avg_speedup = (double *) malloc(sizesMd * sizeof(double));
    int32_t total_configs = 0;

    // Number of different runs for -per- configuration
    const int32_t n_runs = 5;

    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);


        for (int32_t n_ld_Idx = 0; n_ld_Idx < sizesLd; n_ld_Idx++) {

            int32_t n_ld = n_lds[n_ld_Idx];

            for (int32_t n_ud_Idx = 0; n_ud_Idx < sizesUd; n_ud_Idx++) {

                int32_t n_ud = n_uds[n_ud_Idx];

                // Reference solution (No ILP optimizations)
                double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
                for (int32_t i = 0; i < n; i++) {
                    x_ref[i] = 0;
                }

                int32_t nz_num = nz_num_band(n, n_ld, n_ud);

                // Equation system definition
                double *a_st = (double *) malloc(nz_num * sizeof(double));
                int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *rhs = (double *) malloc(n * sizeof(double));
                double *x_estimate = (double *) malloc(n * sizeof(double));

                // Fill matrix
                band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);


                printf("[GEN]: Allocating memory for CSR-VI format..");
                // Convert to CSR-VI format
                int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
                int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *vala_csrvi = (double *) malloc(nz_num * sizeof(double));
                printf("DONE\n");


                printf("[GEN]: Converting from ST to CSR-VI..");
                st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi,
                            n,
                            nz_num);
                printf("DONE\n");

                // Convert to CSR format
                printf("[GEN]: Allocating memory for CSR format..");
                int32_t *ia_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *a_csr = (double *) malloc(nz_num * sizeof(double));
                printf("..DONE\n");

                printf("[GEN]: Converting from ST to CSR..");
                st_to_csr(ia_st, ja_st, a_st, &ia_csr, ja_csr, a_csr, nz_num);
                printf("..DONE\n");

                // Free memory used for ST format
                free(a_st);
                free(ia_st);
                free(ja_st);

                // Find good mr
                mr = n/16;

                // Time report
                uint64_t *time_report = (uint64_t *) malloc(sizesMd * sizeof(uint64_t));
                uint64_t min_time, max_time;

                // Retrieve reference solution
                printf("[EXEC]: Retrieving reference CSR solution\n");
/*                pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                  vala_csrvi, x_ref, rhs, itr_max, mr,
                                  tol_abs, tol_rel);*/
                pmgmres_ilu_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_ref, rhs, itr_max, mr, tol_abs, tol_rel);

                // Calculate residual from original algorithm (to check proper convergence)
                double *ref_residual = (double *) malloc(n * sizeof(double));
                ax_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_ref, ref_residual);

                // Calculate l-infinity norm
                double ref_residual_norm = l_inf(ref_residual, rhs, n);

                // Reference algorithm can return NaNs AND/OR can diverge. This is not caused by us!
                if (!hasNaN(x_ref, n) && ref_residual_norm < CONVERGENCE_THRESHOLD) {


                    // Execute PMGMRES for each ILP level
                    int32_t i, j, run, mode;
                    for (i = 0; i < sizesMd; i++) {

                        mode = modes[i];

                        double avg_runtime = 0.0;

                        for (run = 0; run < n_runs; run++) {

                            // Neutralize estimate
                            for (j = 0; j < n; j++) {
                                x_estimate[j] = 0.0;
                            }

                            // Start time measurement
                            uint64_t start = start_tsc();

                            // Execute PMGMRES
                            if (i < 6) {

                                if (i == 0) {
                                    // Baseline
                                    pmgmres_ilu_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                                } else {
                                    // Set unroll configuration
                                    set_unroll_factors(mode, mode, mode, mode, mode, mode, mode);

                                    // Scalar mode
                                    pmgmres_ilu_csrvi_ilp_unroll(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi,
                                                                 x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                                }

                            } else {
                                // Vector mode
                                if (i == 6) {
                                    // SSE (CSR-VI)
                                    pmgmres_ilu_csrvi_sse(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi,
                                                          x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                                } else if (i == 7)
                                {
                                    // SSE (CSR)
                                    pmgmres_ilu_csr_sse(n, nz_num, ia_csr, ja_csr, a_csr, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                                }
                                else if (i == 8){
                                    // AVX (CSR-VI)
                                    pmgmres_ilu_csrvi_ilp_vector(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi,
                                                                 x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                                } else if (i == 9){
                                    // AVX (CSR)
                                    pmgmres_ilu_csr_avx(n, nz_num, ia_csr, ja_csr, a_csr,
                                                                 x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                                }
                            }

                            // Stop time measurement
                            uint64_t duration = stop_tsc(start);

                            // Update runtime average
                            avg_runtime += ((double) duration);
                        }

                        avg_runtime /= ((double) n_runs);

                        // Update time report
                        time_report[i] = avg_runtime;

                        // Update extrema
                        if (i > 0) {

                            if (i == 1) {
                                min_time = max_time = avg_runtime;
                            } else {
                                min_time = (avg_runtime < min_time) ? avg_runtime : min_time;
                                max_time = (avg_runtime > max_time) ? avg_runtime : max_time;
                            }

                        }
                    }

                    // Print time report
                    printf("AVG_Time(Runs = %d, N = %d, N_LD = %d, N_UD = %d): ", n_runs, n, n_ld, n_ud);

                    printf("[");
                    char type[32];
                    for (i = 0; i < sizesMd; i++) {

                        if (i == 0) {
                            sprintf(type, "BASELINE (CSR)");
                        } else {

                            if (i < 6) {
                                sprintf(type, "UL(%d) (CSR-VI)", modes[i]);
                            } else if (i == 6) {
                                strcpy(type, "SSE (CSR-VI)");
                            } else if (i == 7) {
                                strcpy(type, "SSE (CSR)");
                            } else if (i == 8) {
                                strcpy(type, "AVX (CSR-VI)");
                            } else if (i == 9) {
                                strcpy(type, "AVX (CSR)");
                            }
                        }

                        if (time_report[i] == min_time) {
                            printf(GRN "%s => %llu" RESET, type, time_report[i]);
                        } else if (time_report[i] == max_time) {
                            printf(RED "%s => %llu" RESET, type, time_report[i]);
                        } else {
                            printf("%s => %llu", type, time_report[i]);
                        }

                        if (i != sizesMd - 1) {
                            printf(", ");
                        }
                    }

                    // Update average speedups
                    double ref_time = (double) time_report[0];
                    for (i = 0; i < sizesMd; i++) {
                        avg_speedup[i] += ref_time / ((double) time_report[i]);
                    }

                    // Add speedup (w.r.t to no loop unrolling)
                    double speedup = (ref_time / ((double) min_time));
                    printf(", SPEEDUP => %f]", speedup);

                    total_configs++;
                    printf("\n");

                } else {
			printf("Not converged. Not possible to print SPEEDUP\n");
		}

                // Free memory
                free(ia_csrvi);
                free(ja_csrvi);
                free(va_csrvi);
                free(vala_csrvi);

                free(ia_csr);
                free(ja_csr);
                free(a_csr);

                free(ref_residual);
                free(x_ref);
                free(rhs);
                free(x_estimate);
                free(time_report);


            }
        }

    }

    printf("========================================================\n");

    int32_t j;
    // Average speedups
    printf("AVG SPEEDUP: [");
    for (j = 0; j < sizesMd; j++) {
        avg_speedup[j] /= total_configs;

        //printf("%d => %f", modes[j], avg_speedup[j]);

        if (j == 0) {
            printf("BASELINE (CSR)");
        } else {

            if (j < 6) {
                printf("UL(%d) (CSR-VI)", modes[j]);
            } else if (j == 6) {
                printf("SSE (CSR-VI)");
            } else if (j == 7) {
                printf("SSE (CSR)");
            } else if (j == 8) {
                printf("AVX (CSR-VI)");
            } else if (j == 9) {
                printf("AVX (CSR)");
            }
        }

        printf(" => %f", avg_speedup[j]);

        if (j != sizesMd - 1) {
            printf(", ");
        }
    }
    printf("]\n");


    free(avg_speedup);

}


/*
 * [FastCode] Evaluate runtime of all available ILP levels with respect to a test set and given sparsity 0 <= p <= 1.
 */
void test_group_band_time_preserve_sparsity() {

    // Disable BUFFERING
    // setbuf(stdout, NULL);

    // Unroll modes (0 denotes CSR baseline, 100 denotes SSE (CSR-VI), 200 SSE (CSR) , 300 AVX (CSR-VI) and 400 AVX(CSR))
    //int32_t modes[] = {0, 1, 4, 8, 16, 32, 100, 200, 300, 400};
    int32_t modes[] = {0};
    int32_t sizesMd = sizeof(modes) / sizeof(modes[0]);

    //int32_t sizes[] = {512, 1024, 2048, 4096, 511, 1025, 2047, 4097};
    //int32_t sizes[] = {128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536};

    int32_t sizes[] = {16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768};

    //int32_t sizes[] = {65536, 131072, 262144, 524288, 1048576};
    //int32_t sizes[] = {128, 256, 512, 1024, 2048, 4096, 8192};
    //int32_t sizes[] = {2, 3, 4, 5, 6, 7, 8, 9};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Average speedups
    double *avg_speedup = (double *) malloc(sizesMd * sizeof(double));
    int32_t total_configs = 0;

    // Number of different runs for -per- configuration
    const int32_t n_runs = 5;

    // Sparsity level
    double sparsity = 0.025;

    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);

        // Calculate number of diagonals to guarantee sparsity
        int32_t n_ld, n_ud;
        double real_part = -0.5 * (sqrt(1.0 - 4.0 * n * n * (sparsity - 1.0)) + 1.0);
        n_ld = n_ud = n + ((int32_t) real_part);

        // Reference solution (No ILP optimizations)
        double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
        for (int32_t i = 0; i < n; i++) {
            x_ref[i] = 0;
        }

        int32_t nz_num = nz_num_band(n, n_ld, n_ud);

        // Equation system definition
        double *a_st = (double *) malloc(nz_num * sizeof(double));
        int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *rhs = (double *) malloc(n * sizeof(double));
        double *x_estimate = (double *) malloc(n * sizeof(double));

        // Fill matrix
        band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);


        printf("[GEN]: Allocating memory for CSR-VI format..");
        // Convert to CSR-VI format
        int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
        int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *vala_csrvi = (double *) malloc(nz_num * sizeof(double));
        printf("DONE\n");


        printf("[GEN]: Converting from ST to CSR-VI..");
        st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi,
                    n,
                    nz_num);
        printf("DONE\n");

        // Convert to CSR format
        printf("[GEN]: Allocating memory for CSR format..");
        int32_t *ia_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
        int32_t *ja_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
        double *a_csr = (double *) malloc(nz_num * sizeof(double));
        printf("..DONE\n");

        printf("[GEN]: Converting from ST to CSR..");
        st_to_csr(ia_st, ja_st, a_st, &ia_csr, ja_csr, a_csr, nz_num);
        printf("..DONE\n");

        // Free memory used for ST format
        free(a_st);
        free(ia_st);
        free(ja_st);

        // Choose "sensible" mr
        mr = n/8;

        // Time report
        uint64_t *time_report = (uint64_t *) malloc(sizesMd * sizeof(uint64_t));
        uint64_t min_time, max_time;

        // Retrieve reference solution
        printf("[EXEC]: Retrieving reference CSR solution\n");
/*                pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                          vala_csrvi, x_ref, rhs, itr_max, mr,
                          tol_abs, tol_rel);*/
        pmgmres_ilu_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_ref, rhs, itr_max, mr, tol_abs, tol_rel);

        // Calculate residual from original algorithm (to check proper convergence)
        double *ref_residual = (double *) malloc(n * sizeof(double));
        ax_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_ref, ref_residual);

        // Calculate l-infinity norm
        double ref_residual_norm = l_inf(ref_residual, rhs, n);

        // Reference algorithm can return NaNs AND/OR can diverge. This is not caused by us!
        if (!hasNaN(x_ref, n) && ref_residual_norm < CONVERGENCE_THRESHOLD) {


            // Execute PMGMRES for each ILP level
            int32_t i, j, run, mode;
            for (i = 0; i < sizesMd; i++) {

                mode = modes[i];

                double avg_runtime = 0.0;

                for (run = 0; run < n_runs; run++) {

                    // Neutralize estimate
                    for (j = 0; j < n; j++) {
                        x_estimate[j] = 0.0;
                    }

                    // Start time measurement
                    uint64_t start = start_tsc();

                    // Execute PMGMRES
                    if (i < 6) {

                        if (i == 0) {
                            // Baseline
                            pmgmres_ilu_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                        } else {
                            // Set unroll configuration
                            set_unroll_factors(mode, mode, mode, mode, mode, mode, mode);

                            // Scalar mode
                            pmgmres_ilu_csrvi_ilp_unroll(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi,
                                                         x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                        }

                    } else {
                        // Vector mode
                        if (i == 6) {
                            // SSE (CSR-VI)
                            pmgmres_ilu_csrvi_sse(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi,
                                                  x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                        } else if (i == 7)
                        {
                            // SSE (CSR)
                            pmgmres_ilu_csr_sse(n, nz_num, ia_csr, ja_csr, a_csr, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                        }
                        else if (i == 8){
                            // AVX (CSR-VI)
                            pmgmres_ilu_csrvi_ilp_vector(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi,
                                                         x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                        } else if (i == 9){
                            // AVX (CSR)
                            pmgmres_ilu_csr_avx(n, nz_num, ia_csr, ja_csr, a_csr,
                                                x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
                        }
                    }

                    // Stop time measurement
                    uint64_t duration = stop_tsc(start);

                    // Update runtime average
                    avg_runtime += ((double) duration);
                }

                avg_runtime /= ((double) n_runs);

                // Update time report
                time_report[i] = avg_runtime;

                // Update extrema
                if (i > 0) {

                    if (i == 1) {
                        min_time = max_time = avg_runtime;
                    } else {
                        min_time = (avg_runtime < min_time) ? avg_runtime : min_time;
                        max_time = (avg_runtime > max_time) ? avg_runtime : max_time;
                    }

                }
            }

            // Print time report
            printf("AVG_Time(Runs = %d, N = %d, N_LD = %d, N_UD = %d): ", n_runs, n, n_ld, n_ud);

            printf("[");
            char type[32];
            for (i = 0; i < sizesMd; i++) {

                if (i == 0) {
                    sprintf(type, "BASELINE (CSR)");
                } else {

                    if (i < 6) {
                        sprintf(type, "UL(%d) (CSR-VI)", modes[i]);
                    } else if (i == 6) {
                        strcpy(type, "SSE (CSR-VI)");
                    } else if (i == 7) {
                        strcpy(type, "SSE (CSR)");
                    } else if (i == 8) {
                        strcpy(type, "AVX (CSR-VI)");
                    } else if (i == 9) {
                        strcpy(type, "AVX (CSR)");
                    }
                }

                if (time_report[i] == min_time) {
                    printf(GRN "%s => %llu" RESET, type, time_report[i]);
                } else if (time_report[i] == max_time) {
                    printf(RED "%s => %llu" RESET, type, time_report[i]);
                } else {
                    printf("%s => %llu", type, time_report[i]);
                }

                if (i != sizesMd - 1) {
                    printf(", ");
                }
            }

            // Update average speedups
            double ref_time = (double) time_report[0];
            for (i = 0; i < sizesMd; i++) {
                avg_speedup[i] += ref_time / ((double) time_report[i]);
            }

            // Add speedup (w.r.t to no loop unrolling)
            double speedup = (ref_time / ((double) min_time));
            printf(", SPEEDUP => %f]", speedup);

            total_configs++;
            printf("\n");

        } else {
            printf("Not converged. Not possible to print SPEEDUP.\n");
        }

        // Free memory
        free(ia_csrvi);
        free(ja_csrvi);
        free(va_csrvi);
        free(vala_csrvi);

        free(ia_csr);
        free(ja_csr);
        free(a_csr);

        free(ref_residual);
        free(x_ref);
        free(rhs);
        free(x_estimate);
        free(time_report);

    }

    printf("========================================================\n");

    int32_t j;
    // Average speedups
    printf("AVG SPEEDUP: [");
    for (j = 0; j < sizesMd; j++) {
        avg_speedup[j] /= ((double) total_configs);

        //printf("%d => %f", modes[j], avg_speedup[j]);

        if (j == 0) {
            printf("BASELINE (CSR)");
        } else {

            if (j < 6) {
                printf("UL(%d) (CSR-VI)", modes[j]);
            } else if (j == 6) {
                printf("SSE (CSR-VI)");
            } else if (j == 7) {
                printf("SSE (CSR)");
            } else if (j == 8) {
                printf("AVX (CSR-VI)");
            } else if (j == 9) {
                printf("AVX (CSR)");
            }
        }

        printf(" => %f", avg_speedup[j]);

        if (j != sizesMd - 1) {
            printf(", ");
        }
    }
    printf("]\n");


    free(avg_speedup);

}
/*
 * [FastCode] Testing whether all ILP optimizations (loop unrolling, scalar replacement etc.) yield the same results
 */
void test_group_band_correctness_ilp() {

    // Unroll modes
    int32_t modes[] = {1, 4, 8, 16, 32};
    int32_t sizesMd = sizeof(modes) / sizeof(modes[0]);

    //int32_t sizes[] = {512, 1024, 2048, 4096};
    int32_t sizes[] = {36};
    //int32_t sizes[] = {511, 1025, 2047, 4097};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    //int32_t n_lds[] = {4, 7, 14, 40, 51};
    int32_t n_lds[] = {2};
    int32_t sizesLd = sizeof(n_lds) / sizeof(n_lds[0]);

    // Upper diagonals
    //int32_t n_uds[] = {0, 5, 7, 39, 61};
    int32_t n_uds[] = {3};
    int32_t sizesUd = sizeof(n_uds) / sizeof(n_uds[0]);

    int64_t total_errors = 0;
    int64_t total_configs = 0;

    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);


        for (int32_t n_ld_Idx = 0; n_ld_Idx < sizesLd; n_ld_Idx++) {

            int32_t n_ld = n_lds[n_ld_Idx];

            for (int32_t n_ud_Idx = 0; n_ud_Idx < sizesUd; n_ud_Idx++) {

                int32_t n_ud = n_uds[n_ud_Idx];

                // Reference solution (No ILP optimizations)
                double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
                for (int32_t i = 0; i < n; i++) {
                    x_ref[i] = 0;
                }

                int32_t nz_num = nz_num_band(n, n_ld, n_ud);

                // Iteration counter(s)
                int32_t i;

                printf("[MEM]: Allocating memory for matrix in ST format..");
                // Equation system definition
                double *a_st = (double *) malloc(nz_num * sizeof(double));
                int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *rhs = (double *) malloc(n * sizeof(double));
                double *x_estimate = (double *) malloc(n * sizeof(double));
                printf("DONE\n");

                // Fill matrix
                band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);


                // Convert to CSR-VI format
                printf("[MEM]: Allocating memory for matrix in CSR-VI format..");
                int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
                int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *vala_csrvi = (double *) malloc(nz_num * sizeof(double));
                printf("DONE\n");

                printf("[MEM]: Converting from ST to CSR-VI..");
                st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi,
                            n,
                            nz_num);
                printf("DONE\n");

                // Free memory used for ST format
                printf("[MEM]: Freeing memory used for ST format..");
                free(a_st);
                free(ia_st);
                free(ja_st);
                printf("DONE\n");

                // Retrieve reference solution
                pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                             vala_csrvi, x_ref, rhs, itr_max, mr,
                                             tol_abs,
                                             tol_rel);

                // Reference algorithm can return NaNs. This is not a bug from our side!
                if (!hasNaN(x_ref, n)) {

                    for (int32_t pmgmresModeIdx = 0; pmgmresModeIdx < sizesMd; pmgmresModeIdx++) {

                        int32_t pmgmresMode = modes[pmgmresModeIdx];

                        for (int32_t axModeIdx = 0; axModeIdx < sizesMd; axModeIdx++) {

                            int32_t axMode = modes[axModeIdx];

                            for (int32_t iluModeIdx = 0; iluModeIdx < sizesMd; iluModeIdx++) {

                                int32_t iluMode = modes[iluModeIdx];

                                for (int32_t lusModeIdx = 0; lusModeIdx < sizesMd; lusModeIdx++) {

                                    int32_t lusMode = modes[lusModeIdx];

                                    for (int32_t diagModeIdx = 0; diagModeIdx < sizesMd; diagModeIdx++) {

                                        int32_t diagMode = modes[diagModeIdx];

                                        for (int32_t rearrangeModeIdx = 0;
                                             rearrangeModeIdx < sizesMd; rearrangeModeIdx++) {

                                            int32_t rearrangeMode = modes[rearrangeModeIdx];

                                            for (int32_t r8ModeIdx = 0; r8ModeIdx < sizesMd; r8ModeIdx++) {
                                                int32_t r8Mode = modes[r8ModeIdx];

                                                // Set unroll factor configuration
                                                set_unroll_factors(pmgmresMode, axMode, iluMode, lusMode, diagMode,
                                                                   rearrangeMode, r8Mode);

                                                // Neutralize estimate
                                                for (i = 0; i < n; i++) {
                                                    x_estimate[i] = 0.0;
                                                }

                                                // Run solver
                                                pmgmres_ilu_csrvi_ilp_unroll(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                                                             vala_csrvi, x_estimate, rhs, itr_max,
                                                                             mr,
                                                                             tol_abs,
                                                                             tol_rel);

                                                // Calculate squared l2 distance
                                                double l2_sq = l2_square(x_ref, x_estimate, n);

                                                // Print configuration
                                                printf("[N = %d, N_LD = %d, N_UD = %d, PMGMRES = %d, AX = %d, ILU = %d, LUS = %d, DIAG = %d, REARR = %d, R8 = %d]: ",
                                                       n, n_ld, n_ud, pmgmresMode, axMode, iluMode, lusMode, diagMode,
                                                       rearrangeMode, r8Mode);

                                                // Solutions are -not- equal
                                                if (l2_sq < EQUALITY_THRESHOLD) {
                                                    printf(GRN "CORRECT" RESET);
                                                } else {
                                                    printf(RED "ERROR" RESET);
                                                    total_errors++;
                                                }
                                                printf("\n");

                                                printf("========================================================\n");

                                                total_configs++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                // Free memory
                printf("[MEM]: Freeing memory used for CSR-VI format..");
                free(ia_csrvi);
                free(ja_csrvi);
                free(va_csrvi);
                free(vala_csrvi);

                free(x_ref);
                free(rhs);
                free(x_estimate);
                printf("DONE\n");

                printf("\n");
            }
        }

    }

    printf("Testing done!\n");
    printf("Tested %d different configurations\n\t", total_configs);

    if (total_errors == 0) {
        printf(GRN "No errors found!" RESET);
    } else {
        printf("Found %d errors!", total_errors);
    }

}


/*
 * [FastCode] Testing whether the AVX (CSR-VI) optimization yields correct results
 */
void test_group_band_correctness_avx_csrvi() {

    int32_t sizes[] = {511, 666, 690, 777, 1222, 444};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    int32_t n_lds[] = {78, 67, 122};
    int32_t sizesLd = sizeof(n_lds) / sizeof(n_lds[0]);

    // Upper diagonals
    int32_t n_uds[] = {55, 120};
    int32_t sizesUd = sizeof(n_uds) / sizeof(n_uds[0]);

    int64_t total_errors = 0;
    int64_t total_configs = 0;


    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);


        for (int32_t n_ld_Idx = 0; n_ld_Idx < sizesLd; n_ld_Idx++) {

            int32_t n_ld = n_lds[n_ld_Idx];

            for (int32_t n_ud_Idx = 0; n_ud_Idx < sizesUd; n_ud_Idx++) {

                int32_t n_ud = n_uds[n_ud_Idx];

                // Reference solution (No ILP optimizations)
                double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
                for (int32_t i = 0; i < n; i++) {
                    x_ref[i] = 0;
                }

                int32_t nz_num = nz_num_band(n, n_ld, n_ud);

                printf("[MEM]: Allocating memory for matrix in ST format..");
                // Equation system definition
                double *a_st = (double *) malloc(nz_num * sizeof(double));
                int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *rhs = (double *) malloc(n * sizeof(double));
                double *x_estimate = (double *) malloc(n * sizeof(double));
                printf("DONE\n");

                // Fill matrix
                band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);


                // Convert to CSR-VI format
                printf("[MEM]: Allocating memory for matrix in CSR-VI format..");
                int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
                int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *vala_csrvi = (double *) malloc(nz_num * sizeof(double));
                printf("DONE\n");

                printf("[MEM]: Converting from ST to CSR-VI..");
                st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi,
                            n,
                            nz_num);
                printf("DONE\n");

                // Free memory used for ST format
                printf("[MEM]: Freeing memory used for ST format..");
                free(a_st);
                free(ia_st);
                free(ja_st);
                printf("DONE\n");

                // Retrieve reference solution
                pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                  vala_csrvi, x_ref, rhs, itr_max, mr,
                                  tol_abs, tol_rel);


                // Calculate residual from original algorithm (to check proper convergence)
                double *ref_residual = (double *) malloc(n * sizeof(double));
                ax_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_ref, ref_residual);

                // Calculate l-infinity norm
                double ref_residual_norm = l_inf(ref_residual, rhs, n);

                // Reference algorithm can return NaNs AND/OR can diverge. This is not caused by us!
                if (!hasNaN(x_ref, n) && ref_residual_norm < CONVERGENCE_THRESHOLD) {
                    // Estimate is already neutralized

                    // Retrieve vector estimate
                    pmgmres_ilu_csrvi_ilp_vector(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                                 vala_csrvi, x_estimate, rhs, itr_max, mr,
                                                 tol_abs, tol_rel);


                    // Calculate squared l2 distance
                    double l2_sq = l2_square(x_ref, x_estimate, n);
                    double l_inf_ = l_inf(x_ref, x_estimate, n);

                    // Print configuration
                    printf("[N = %d, N_LD = %d, N_UD = %d]: ", n, n_ld, n_ud);

                    // Solutions are -not- equal
                    if (l_inf_ < EQUALITY_THRESHOLD) {
                        printf(GRN "CORRECT" RESET);
                    } else {
                        printf(RED "ERROR l2-error: %f, l2-inf: %f [Residual of original algorithm: %f]\n" RESET, l2_sq, l_inf_, ref_residual_norm);
                        total_errors++;
                    }
                    printf("\n");

                    printf("========================================================\n");

                    total_configs++;
                }


                // Free memory
                printf("[MEM]: Freeing memory used for CSR-VI format..");
                free(ia_csrvi);
                free(ja_csrvi);
                free(va_csrvi);
                free(vala_csrvi);

                free(ref_residual);
                free(x_ref);
                free(rhs);
                free(x_estimate);
                printf("DONE\n");

                printf("\n");
            }
        }

    }

    printf("Testing done!\n");
    printf("Tested %d different configurations\n\t", total_configs);

    if (total_errors == 0) {
        printf(GRN "No errors found!\n" RESET);
    } else {
        printf("Found %d errors!\n", total_errors);
    }

}


/*
 * [FastCode] Testing whether the AVX (CSR) optimization yields correct results
 */
void test_group_band_correctness_avx_csr() {

    int32_t sizes[] = {10};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    int32_t n_lds[] = {3};
    int32_t sizesLd = sizeof(n_lds) / sizeof(n_lds[0]);

    // Upper diagonals
    int32_t n_uds[] = {1};
    int32_t sizesUd = sizeof(n_uds) / sizeof(n_uds[0]);

    int64_t total_errors = 0;
    int64_t total_configs = 0;


    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);


        for (int32_t n_ld_Idx = 0; n_ld_Idx < sizesLd; n_ld_Idx++) {

            int32_t n_ld = n_lds[n_ld_Idx];

            for (int32_t n_ud_Idx = 0; n_ud_Idx < sizesUd; n_ud_Idx++) {

                int32_t n_ud = n_uds[n_ud_Idx];

                // Reference solution (No ILP optimizations)
                double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
                for (int32_t i = 0; i < n; i++) {
                    x_ref[i] = 0;
                }

                int32_t nz_num = nz_num_band(n, n_ld, n_ud);

                printf("[MEM]: Allocating memory for matrix in ST format..");
                // Equation system definition
                double *a_st = (double *) malloc(nz_num * sizeof(double));
                int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *rhs = (double *) malloc(n * sizeof(double));
                double *x_estimate = (double *) malloc(n * sizeof(double));
                printf("DONE\n");

                // Fill matrix
                band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);

                // Convert to CSR-VI format
                printf("[MEM]: Allocating memory for matrix in CSR format..");
                int32_t *ia_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *a_csr = (double *) malloc(nz_num * sizeof(double));
                printf("DONE\n");

                printf("[MEM]: Converting from ST to CSR..");
                st_to_csr(ia_st, ja_st, a_st, &ia_csr, ja_csr, a_csr, nz_num);
                printf("DONE\n");

                // Free memory used for ST format
                printf("[MEM]: Freeing memory used for ST format..");
                free(a_st);
                free(ia_st);
                free(ja_st);
                printf("DONE\n");

                // Retrieve reference solution
                pmgmres_ilu_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_ref, rhs, itr_max, mr, tol_abs, tol_rel);

                // Calculate residual from original algorithm (to check proper convergence)
                double *ref_residual = (double *) malloc(n * sizeof(double));
                ax_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_ref, ref_residual);

                // Calculate l-infinity norm
                double ref_residual_norm = l_inf(ref_residual, rhs, n);

                // Reference algorithm can return NaNs AND/OR can diverge. This is not caused by us!
                if (!hasNaN(x_ref, n) && ref_residual_norm < CONVERGENCE_THRESHOLD) {
                    // Estimate is already neutralized

                    // Retrieve vector estimate
                    pmgmres_ilu_csr_avx(n, nz_num, ia_csr, ja_csr, a_csr, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);

                    // Calculate squared l2 distance
                    double l2_sq = l2_square(x_ref, x_estimate, n);
                    double l_inf_ = l_inf(x_ref, x_estimate, n);

                    // Print configuration
                    printf("[N = %d, N_LD = %d, N_UD = %d]: ", n, n_ld, n_ud);

                    // Solutions are -not- equal
                    if (l_inf_ < EQUALITY_THRESHOLD) {
                        printf(GRN "CORRECT" RESET);
                    } else {
                        printf(RED "ERROR l2-error: %f, l2-inf: %f [Residual of original algorithm: %f]\n" RESET, l2_sq, l_inf_, ref_residual_norm);
                        total_errors++;
                    }
                    printf("\n");

                    printf("========================================================\n");

                    total_configs++;
                }


                // Free memory
                printf("[MEM]: Freeing memory used for CSR format..");
                free(ia_csr);
                free(ja_csr);
                free(a_csr);

                free(ref_residual);
                free(x_ref);
                free(rhs);
                free(x_estimate);
                printf("DONE\n");

                printf("\n");
            }
        }

    }

    printf("Testing done!\n");
    printf("Tested %d different configurations\n\t", total_configs);

    if (total_errors == 0) {
        printf(GRN "No errors found!\n" RESET);
    } else {
        printf("Found %d errors!\n", total_errors);
    }

}


/*
 * [FastCode] Testing whether the SSE (CSR-VI) optimization yields correct results
 */
void test_group_band_correctness_sse_csrvi() {

    int32_t sizes[] = {511, 666, 690, 777, 1222, 444};
    //int32_t sizes[] = {32768};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    int32_t n_lds[] = {78, 67, 122};
    int32_t sizesLd = sizeof(n_lds) / sizeof(n_lds[0]);

    // Upper diagonals
    int32_t n_uds[] = {55, 120};
    int32_t sizesUd = sizeof(n_uds) / sizeof(n_uds[0]);

    int64_t total_errors = 0;
    int64_t total_configs = 0;


    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);


        for (int32_t n_ld_Idx = 0; n_ld_Idx < sizesLd; n_ld_Idx++) {

            int32_t n_ld = n_lds[n_ld_Idx];

            for (int32_t n_ud_Idx = 0; n_ud_Idx < sizesUd; n_ud_Idx++) {

                int32_t n_ud = n_uds[n_ud_Idx];

                // Reference solution (No ILP optimizations)
                double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
                for (int32_t i = 0; i < n; i++) {
                    x_ref[i] = 0;
                }

                int32_t nz_num = nz_num_band(n, n_ld, n_ud);

                printf("[MEM]: Allocating memory for matrix in ST format..");
                // Equation system definition
                double *a_st = (double *) malloc(nz_num * sizeof(double));
                int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *rhs = (double *) malloc(n * sizeof(double));
                double *x_estimate = (double *) malloc(n * sizeof(double));
                printf("DONE\n");

                // Fill matrix
                band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);


                // Convert to CSR-VI format
                printf("[MEM]: Allocating memory for matrix in CSR-VI format..");
                int32_t *ia_csrvi = (int32_t *) malloc((n + 1) * sizeof(int32_t));
                int32_t *ja_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *va_csrvi = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *vala_csrvi = (double *) malloc(nz_num * sizeof(double));
                printf("DONE\n");

                printf("[MEM]: Converting from ST to CSR-VI..");
                st_to_csrvi(ia_st, ja_st, a_st, ia_csrvi, ja_csrvi, va_csrvi, &vala_csrvi,
                            n,
                            nz_num);
                printf("DONE\n");

                // Free memory used for ST format
                printf("[MEM]: Freeing memory used for ST format..");
                free(a_st);
                free(ia_st);
                free(ja_st);
                printf("DONE\n");

                // Retrieve reference solution
                pmgmres_ilu_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                  vala_csrvi, x_ref, rhs, itr_max, mr,
                                  tol_abs, tol_rel);


                // Calculate residual from original algorithm (to check proper convergence)
                double *ref_residual = (double *) malloc(n * sizeof(double));
                ax_csrvi(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_ref, ref_residual);

                // Calculate l-infinity norm
                double ref_residual_norm = l_inf(ref_residual, rhs, n);

                // Reference algorithm can return NaNs AND/OR can diverge. This is not caused by us!
                if (!hasNaN(x_ref, n) && ref_residual_norm < CONVERGENCE_THRESHOLD) {
                    // Estimate is already neutralized

                    // Retrieve vector estimate
                    pmgmres_ilu_csrvi_sse(n, nz_num, ia_csrvi, ja_csrvi, va_csrvi,
                                                 vala_csrvi, x_estimate, rhs, itr_max, mr,
                                                 tol_abs, tol_rel);


                    // Calculate squared l2 distance
                    double l2_sq = l2_square(x_ref, x_estimate, n);
                    double l_inf_ = l_inf(x_ref, x_estimate, n);

                    // Print configuration
                    printf("[N = %d, N_LD = %d, N_UD = %d]: ", n, n_ld, n_ud);

                    // Solutions are -not- equal
                    if (l_inf_ < EQUALITY_THRESHOLD) {
                        printf(GRN "CORRECT" RESET);
                    } else {
                        printf(RED "ERROR l2-error: %f, l2-inf: %f [Residual of original algorithm: %f]\n" RESET, l2_sq, l_inf_, ref_residual_norm);
                        total_errors++;
                    }
                    printf("\n");

                    printf("========================================================\n");

                    total_configs++;
                }


                // Free memory
                printf("[MEM]: Freeing memory used for CSR-VI format..");
                free(ia_csrvi);
                free(ja_csrvi);
                free(va_csrvi);
                free(vala_csrvi);

                free(ref_residual);
                free(x_ref);
                free(rhs);
                free(x_estimate);
                printf("DONE\n");

                printf("\n");
            }
        }

    }

    printf("Testing done!\n");
    printf("Tested %d different configurations\n\t", total_configs);

    if (total_errors == 0) {
        printf(GRN "No errors found!\n" RESET);
    } else {
        printf("Found %d errors!\n", total_errors);
    }

}


/*
 * [FastCode] Testing whether the SSE (CSR) optimization yields correct results
 */
void test_group_band_correctness_sse_csr() {

    int32_t sizes[] = {511, 666, 690, 777, 1222, 444};
    int32_t sizesN = sizeof(sizes) / sizeof(sizes[0]);

    // Lower diagonals
    int32_t n_lds[] = {78, 67, 122};
    int32_t sizesLd = sizeof(n_lds) / sizeof(n_lds[0]);

    // Upper diagonals
    int32_t n_uds[] = {55, 120};
    int32_t sizesUd = sizeof(n_uds) / sizeof(n_uds[0]);

    int64_t total_errors = 0;
    int64_t total_configs = 0;


    for (int32_t nIdx = 0; nIdx < sizesN; nIdx += 1) {

        // Size
        int32_t n = sizes[nIdx];

        printf("N = %d:\n", n);


        for (int32_t n_ld_Idx = 0; n_ld_Idx < sizesLd; n_ld_Idx++) {

            int32_t n_ld = n_lds[n_ld_Idx];

            for (int32_t n_ud_Idx = 0; n_ud_Idx < sizesUd; n_ud_Idx++) {

                int32_t n_ud = n_uds[n_ud_Idx];

                // Reference solution (No ILP optimizations)
                double *x_ref = (double *) malloc(sizes[nIdx] * sizeof(double));
                for (int32_t i = 0; i < n; i++) {
                    x_ref[i] = 0;
                }

                int32_t nz_num = nz_num_band(n, n_ld, n_ud);

                printf("[MEM]: Allocating memory for matrix in ST format..");
                // Equation system definition
                double *a_st = (double *) malloc(nz_num * sizeof(double));
                int32_t *ia_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_st = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *rhs = (double *) malloc(n * sizeof(double));
                double *x_estimate = (double *) malloc(n * sizeof(double));
                printf("DONE\n");

                // Fill matrix
                band_system_st(n, n_ld, n_ud, ia_st, ja_st, a_st, rhs, x_estimate);

                // Convert to CSR-VI format
                printf("[MEM]: Allocating memory for matrix in CSR format..");
                int32_t *ia_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
                int32_t *ja_csr = (int32_t *) malloc(nz_num * sizeof(int32_t));
                double *a_csr = (double *) malloc(nz_num * sizeof(double));
                printf("DONE\n");

                printf("[MEM]: Converting from ST to CSR..");
                st_to_csr(ia_st, ja_st, a_st, &ia_csr, ja_csr, a_csr, nz_num);
                printf("DONE\n");

                // Free memory used for ST format
                printf("[MEM]: Freeing memory used for ST format..");
                free(a_st);
                free(ia_st);
                free(ja_st);
                printf("DONE\n");

                // Retrieve reference solution
                pmgmres_ilu_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_ref, rhs, itr_max, mr, tol_abs, tol_rel);

                // Calculate residual from original algorithm (to check proper convergence)
                double *ref_residual = (double *) malloc(n * sizeof(double));
                ax_cr(n, nz_num, ia_csr, ja_csr, a_csr, x_ref, ref_residual);

                // Calculate l-infinity norm
                double ref_residual_norm = l_inf(ref_residual, rhs, n);

                // Reference algorithm can return NaNs AND/OR can diverge. This is not caused by us!
                if (!hasNaN(x_ref, n) && ref_residual_norm < CONVERGENCE_THRESHOLD) {
                    // Estimate is already neutralized

                    // Retrieve vector estimate
                    pmgmres_ilu_csr_sse(n, nz_num, ia_csr, ja_csr, a_csr, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);

                    // Calculate squared l2 distance
                    double l2_sq = l2_square(x_ref, x_estimate, n);
                    double l_inf_ = l_inf(x_ref, x_estimate, n);

                    // Print configuration
                    printf("[N = %d, N_LD = %d, N_UD = %d]: ", n, n_ld, n_ud);

                    // Solutions are -not- equal
                    if (l_inf_ < EQUALITY_THRESHOLD) {
                        printf(GRN "CORRECT" RESET);
                    } else {
                        printf(RED "ERROR l2-error: %f, l2-inf: %f [Residual of original algorithm: %f]\n" RESET, l2_sq, l_inf_, ref_residual_norm);
                        total_errors++;
                    }
                    printf("\n");

                    printf("========================================================\n");

                    total_configs++;
                }


                // Free memory
                printf("[MEM]: Freeing memory used for CSR format..");
                free(ia_csr);
                free(ja_csr);
                free(a_csr);

                free(ref_residual);
                free(x_ref);
                free(rhs);
                free(x_estimate);
                printf("DONE\n");

                printf("\n");
            }
        }

    }

    printf("Testing done!\n");
    printf("Tested %d different configurations\n\t", total_configs);

    if (total_errors == 0) {
        printf(GRN "No errors found!\n" RESET);
    } else {
        printf("Found %d errors!\n", total_errors);
    }

}


/***
 *       _____ _        _   _     _   _
 *      / ____| |      | | (_)   | | (_)
 *     | (___ | |_ __ _| |_ _ ___| |_ _  ___ ___
 *      \___ \| __/ _` | __| / __| __| |/ __/ __|
 *      ____) | || (_| | |_| \__ \ |_| | (__\__ \
 *     |_____/ \__\__,_|\__|_|___/\__|_|\___|___/
 *
 *
 */


int64_t csr_mem_size(const int32_t nnz, const int32_t n) {
    return 12 * nnz + (n + 1) * 4; // Bytes
}

int64_t csrvi_mem_size(const int32_t n_ld, const int32_t n_ud, const int32_t nnz, const int32_t n) {

    // Number of distinct values
    int32_t nd = (n_ld + 1 > n_ud) ? (n_ld + 1) : n_ud;

    return  8 * nnz + 8 * nd + 4 * (n + 1); // Bytes
}

void print_relative_reduction(const int32_t n_ld, const int32_t n_ud, const int32_t nnz, const int32_t n) {

    int64_t csr_size = csr_mem_size(nnz, n);
    int64_t csrvi_size = csrvi_mem_size(n_ld, n_ud, nnz, n);

    printf("[CSR -> CSR-VI]: Matrix representation has been shrunken by %f percent.\n", 100.0 * fabs(((double) csrvi_size - (double) csr_size)/((double) csr_size)));
}


#define BUFFER_SIZE 255
#define KiB 1024
#define MiB 1048576
#define GiB 1073741824LL
void get_size_with_unit(char buf[BUFFER_SIZE], int64_t bytes) {

    if (bytes < KiB) {
        sprintf(buf, "%d bytes", bytes);
    } else if (bytes >= KiB && bytes < MiB) {
        sprintf(buf, "%d KiB", bytes/KiB);
    } else if (bytes >= MiB && bytes < GiB ) {
        sprintf(buf, "%d MiB", bytes/MiB);
    } else {
        sprintf(buf, "%d GiB", bytes/GiB);
    }
}


void print_absolute_reduction(const int32_t n_ld, const int32_t n_ud, const int32_t nnz, const int32_t n) {


    int64_t csr_size = csr_mem_size(nnz, n);
    int64_t csrvi_size = csrvi_mem_size(n_ld, n_ud, nnz, n);
    int64_t reduction = csr_size - csrvi_size;

    // Buffers
    char buf_save[BUFFER_SIZE];
    char buf_csr[BUFFER_SIZE];
    char buf_csrvi[BUFFER_SIZE];

    // Retrieve string
    get_size_with_unit(buf_save, reduction);
    get_size_with_unit(buf_csr, csr_size);
    get_size_with_unit(buf_csrvi, csrvi_size);

    printf("[CSR -> CSR-VI]: Saved %s (CSR: %s, CSR-VI: %s) \n", buf_save, buf_csr, buf_csrvi);
}



/***
 *      __  __       _        _         _____                           _   _
 *     |  \/  |     | |      (_)       / ____|                         | | (_)
 *     | \  / | __ _| |_ _ __ ___  __ | |  __  ___ _ __   ___ _ __ __ _| |_ _  ___  _ __
 *     | |\/| |/ _` | __| '__| \ \/ / | | |_ |/ _ \ '_ \ / _ \ '__/ _` | __| |/ _ \| '_ \
 *     | |  | | (_| | |_| |  | |>  <  | |__| |  __/ | | |  __/ | | (_| | |_| | (_) | | | |
 *     |_|  |_|\__,_|\__|_|  |_/_/\_\  \_____|\___|_| |_|\___|_|  \__,_|\__|_|\___/|_| |_|
 *
 *
 */



// Number of non-zero elements of a Frank matrix of dimension n
int32_t nz_num_frank(const int32_t n) {
    int32_t nz_num = (n * n + 3 * n)/2 -1;
    return nz_num;
}

/*
 * [FastCode] Frank system matrix.
 */
void frank_system_st(const int32_t n,
                    int32_t *ia_st, int32_t *ja_st, double *a_st, double *rhs, double *x_estimate) {

    int32_t nz_num = nz_num_frank(n);

    // Iteration counter(s)
    int i, j, k;
    int base;

    printf("[GEN]: Filling Frank matrix..");

    // System matrix
    k = 0;
    for (i = 0; i < n; i++) {
        base = n - i;

        if (i > 0) {
            // First subdiagonal
            ia_st[k] = i;
            ja_st[k] = i - 1;
            a_st[k] = (double) base;
            k++;
        }

        // Main diagonal & upper diagonals
        for (j = i; j < n; j++) {
            ia_st[k] = i;
            ja_st[k] = j;
            a_st[k] = (double) base;

            base--;
            k++;
        }
    }

    printf("DONE\n");

    if (PRINT_MATRIX) {
        st_print_table(ia_st, ja_st, a_st, n, n, nz_num);
    }

    printf("[GEN]: Defining RHS (incremental from zero to higher values)..");
    // Right-hand side
    for (i = 0; i < n; i++) {
        rhs[i] = (double) i;
    }
    printf("DONE\n");

    // Solution guess (assume we don't know any better)
    printf("[GEN]: Defining solution guess (only zeros)..");
    for (i = 0; i < n; i++) {
        x_estimate[i] = 0;
    }
    printf("DONE\n");

}

// Number of non-zero elements of a Band matrix of dimension n, number of lower diagonals n_ld (upper diagonals n_ud resp.)
int32_t nz_num_band(const int32_t n, const int32_t n_ld, const int32_t n_ud) {
    int32_t nz_num =  (n + n_ld * n - (n_ld * (n_ld + 1))/2 + n_ud * n - (n_ud * (n_ud + 1))/2);
    return nz_num;
}

/*
 * [FastCode] Band system matrix.
 */
void band_system_st(const int32_t n, const int32_t n_ld, const int32_t n_ud,
                    int32_t *ia_st, int32_t *ja_st, double *a_st, double *rhs, double *x_estimate) {

    assert(n_ld < n && n_ud < n);

    // Number of non-zero elements
    int32_t nz_num = nz_num_band(n, n_ld, n_ud);

    // Iteration counter(s)
    int i;
    int j;
    int k = 0;

    printf("[GEN]: Filling band matrix..");
    // System matrix
    for (i = 0; i < n; i++) {
        // Main diagonal
        ia_st[k] = ja_st[k] = i;
        a_st[k] = 1.0;

        k++;
        // Lower diagonals
        for (j = 1; j < n_ld + 1; j++) {
            if (i - j >= 0) {
                ia_st[k] = i;
                ja_st[k] = i - j;
                a_st[k] = (double) (j + 1);
                k++;
            }
        }

        // Upper diagonals
        for (j = 1; j < n_ud + 1; j++) {
            if (i - j >= 0) {
                ia_st[k] = i - j;
                ja_st[k] = i;
                a_st[k] = (double) j;
                k++;
            }
        }

    }
    printf("DONE\n");

    if (PRINT_MATRIX) {
        st_print_table(ia_st, ja_st, a_st, n, n, nz_num);
    }

    // Right-hand side
    printf("[GEN]: Defining RHS (incremental from zero to higher values)..");
    for (i = 0; i < n; i++) {
        rhs[i] = (double) i;
    }
    printf("DONE\n");

    // Solution guess (assume we don't know any better)
    printf("[GEN]: Defining solution guess (only zeros)..");
    for (i = 0; i < n; i++) {
        x_estimate[i] = 0.0;
    }
    printf("DONE\n");

}

