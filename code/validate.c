#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "perf.h"
#include "mgmres.h"
#include <sys/time.h>
#include <sys/resource.h>

#include <float.h>
#include <math.h>

int validate_test_count = 0;

void print_complex(float re, float im) {
    if (im < 0) {
        printf("%08.2lf - %08.2lf", re, im * (-1));
    } else {
        printf("%08.2lf + %08.2lf", re, im);
    }
}

void print_error(float * a, float * b, float * c, float * c_valid, int n, int eIdx) {
    for (int i = 0; i < n; i += 1) {
        int re = 2 * i + 0;
        int im = 2 * i + 1;
        printf("%07d:\t", i + 1);
        print_complex(a[re], a[im]);
        printf("\t*\t");
        print_complex(b[re], b[im]);
        printf("\t=\t");
        print_complex(c[re], c[im]);
        printf("\t/\t");
        print_complex(c_valid[re], c_valid[im]);
        printf("\n");
    }
    printf("\n\nError occurs at position: %d\n", eIdx + 1);
}

int cmp_vectors (double * a, double * b, double tol_diff, int n) {
    int errorPos = -1;
    for (int i = 0; i < n; i += 1) {
        double diff = (a[i]-b[i])*(a[i]-b[i]);
        
        if( tol_diff < diff ){
            errorPos = i;
            printf("a = %f; b = %f; diff = %f; tol_diff = %f\n", a[i], b[i], diff, tol_diff );
            break;
        }
        /*
        if (((uint32_t *) a)[i] != ((uint32_t *) b)[i]) {
            errorPos = i;
            
            printf("ERROR POS:\n");
            printf("\ni = %i\n", i);
            printf("a = %f; b = %f\n", a[i], b[i] );
            printf("a = %i; b = %i", ((uint32_t *) a)[i], ((uint32_t *) b)[i] );
            exit(0);
            break;
        }
        */
    }
    return errorPos;
}

void validate_csrvi(int n, int nz_num, int32_t ia[], int32_t ja[], int32_t va[],
               double vala[], double x_estimate[], double rhs[], int itr_max, int mr, double tol_abs,
               double tol_rel) {

    double tol_diff = 0.05;
    double *x_calc = (double*) malloc(n * sizeof(double));
    double *x_ref = (double*) malloc(n * sizeof(double));
    int runs = 10, repetitions = 5;
    myInt64 values[repetitions];
    myInt64 mgmres_cycles = 0, mymgmres_cycles = 0;

    //MGMRES_ST
    /*
    memcpy(x_calc, x_estimate, sizeof(double) * n); //copy estimate to new vector
    for (int i = 0; i < repetitions; ++i) {
        mgmres_st(n, nz_num, ia, ja, a, x_calc, rhs, itr_max, mr, tol_abs, tol_rel);
    }
    memcpy(x_ref, x_calc, sizeof(double) * n); 
    */
    
    //MGMRES_CSRVI
    memcpy(x_calc, x_estimate, sizeof(double) * n);
    for (int i = 0; i < repetitions; ++i) {
        pmgmres_ilu_csrvi(n, nz_num, ia, ja, va, vala, x_calc, rhs, itr_max, mr, tol_abs, tol_rel);
    }
    
    /*    
    int cmp = cmp_vectors(x_calc, x_ref, tol_diff, n);
    validate_test_count += 1;
    if (cmp == -1) {
        printf("Test %02d: OK\n, ", validate_test_count);
    } else {
        printf("Test %02d: Fail at position %d\n", validate_test_count, cmp);
        //print_error(a, b, c, c_valid, n, cmp);
    }
    */
    
    free(x_calc);
/*
    memcpy(x_calc, x_estimate, sizeof(double) * n);
    measurement_init();
    for (int i = 0; i < repetitions; ++i) {
        measurement_start();
        for (int j = 0; j < runs; ++j) {
            mgmres_st(n, nz_num, ia, ja, a, x_calc, rhs, itr_max, mr, tol_abs, tol_rel);
        }
        measurement_stop();
        meas_values[i] = meas_values[i] / runs;
    }
    measurement_finish(values);
    qsort(values, repetitions, sizeof(myInt64), cmpMyInt64);
    mgmres_cycles = values[repetitions / 2];

    measurement_init();
    for (int i = 0; i < repetitions; ++i) {
        measurement_start();
        for (int j = 0; j < runs; ++j) {
            mgmres_st(n, nz_num, ia, ja, a, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
        }
        measurement_stop();
        meas_values[i] = meas_values[i] / runs;
    }
    measurement_finish(values);
    qsort(values, repetitions, sizeof(myInt64), cmpMyInt64);
    mymgmres_cycles = values[repetitions / 2];

    int cmp = cmp_vectors(x_estimate, x_valid, n);
    validate_test_count += 1;
    if (cmp == -1) {
        double flops = n * 6;
        printf("Test %02d: OK, ", validate_test_count);
        printf("MGMRES %05.4lf, ", flops / mgmres_cycles);
        printf("MY_MGMRES %05.4lf\n", flops / mymgmres_cycles);
    } else {
        printf("Test %02d: Fail at position %d\n", validate_test_count, cmp);
        //print_error(a, b, c, c_valid, n, cmp);
    }

    free(x_calc);
*/
}

