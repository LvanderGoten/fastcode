#ifndef CODE_MGMRES_CSR_VECTOR_H
#define CODE_MGMRES_CSR_VECTOR_H

/*** SSE ***/
void ax_csr_sse(int n, int nz_num, int ia[], int ja[], double a[], double x[], double w[]);
void diagonal_pointer_csr_sse(int n, int nz_num, int ia[], int ja[], int ua[]);
void ilu_csr_sse(int n, int nz_num, int ia[], int ja[], double a[], int ua[], double l[]);
void lus_csr_sse(int n, int nz_num, int ia[], int ja[], double l[], int ua[], double r[], double z[] );
void pmgmres_ilu_csr_sse(int n, int nz_num, int ia[], int ja[], double a[],
                    double x[], double rhs[], int itr_max, int mr, double tol_abs, double tol_rel);
double r8vec_dot_sse(int n, double a1[], double a2[]);
void rearrange_csr_sse(int n, int nz_num, int ia[], int ja[], double a[]);

#endif //CODE_MGMRES_CSR_VECTOR_H

