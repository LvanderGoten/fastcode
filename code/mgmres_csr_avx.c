//
// Created by lennart on 5/29/16.
//

#include "mgmres_csr_avx.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mgmres.h"
#include <string.h>

__m128d dot_product_avx(const __m256d x, const __m256d y) {

    __m256d xy = _mm256_mul_pd(x, y);
    __m256d temp = _mm256_hadd_pd(xy, xy);
    __m128d hi128 = _mm256_extractf128_pd(temp, 1);
    __m128d interm = _mm256_castpd256_pd128(temp);
    __m128d dotproduct = _mm_add_pd(interm , hi128);

    return dotproduct;
}

double r8vec_dot_csr_avx(int n, double a[], double b[])
{
    int i;
    double value;

    value = 0.0;

    int32_t m = 0;

/*
    //__m256d a_vec, b_vec;
    //__m128d val_vec = _mm_setzero_pd();
    for (i = 0; i < n/4; i++) {
        value = value + a[m] * b[m];
        value = value + a[m + 1] * b[m + 1];
        value = value + a[m + 2] * b[m + 2];
        value = value + a[m + 3] * b[m + 3];

*//*
        a_vec = _mm256_loadu_pd(a + m);
        b_vec = _mm256_loadu_pd(b + m);

        val_vec = _mm_add_pd(val_vec, dot_product_avx(a_vec, b_vec));*//*


        m += 4;
    }*/

    //_mm_store_sd(&value, val_vec);

    // New approach (Sum up afterwards)
    __m256d a_vec, b_vec;
    __m256d val_vec = _mm256_setzero_pd();

#pragma ivdep
    for (i = 0; i < n/4; i++) {
/*        value = value + a[m] * b[m];
        value = value + a[m + 1] * b[m + 1];
        value = value + a[m + 2] * b[m + 2];
        value = value + a[m + 3] * b[m + 3];*/

        a_vec = _mm256_loadu_pd(a + m);
        b_vec = _mm256_loadu_pd(b + m);

        val_vec = _mm256_add_pd(val_vec, _mm256_mul_pd(a_vec, b_vec));

        m += 4;
    }
    // Sum up components of accumulator
    val_vec = _mm256_hadd_pd(val_vec, val_vec);

    // Performance loss here is negligible
    value = ((double*) &val_vec)[0] + ((double*) &val_vec)[2];

    // Remainder
    for (i = (n/4) * 4; i < n; i++) {
        value = value + a[i] * b[i];
    }

    return value;
}

void rearrange_csr_avx(int n, int nz_num, int ia[], int ja[], double a[])
{
    if (use_quicksort) {
        rearrange_csr_qs(n, nz_num ,ia, ja, a);
    } else {


        double dtemp;
        int i;
        int is;
        int itemp;
        int j;
        int j1;
        int j2;
        int k;


        for (i = 0; i < n; i++) {
            j1 = ia[i];
            j2 = ia[i + 1];
            is = j2 - j1;

            for (k = 1; k < is; k++) {

                for (j = j1; j < j2 - k; j++) {
                    if (ja[j + 1] < ja[j]) {
                        itemp = ja[j + 1];
                        ja[j + 1] = ja[j];
                        ja[j] = itemp;

                        dtemp = a[j + 1];
                        a[j + 1] = a[j];
                        a[j] = dtemp;
                    }
                }

            }
        }
    }
    return;
}

void ax_csr_avx(int n, int nz_num, int ia[], int ja[], double a[], double x[],
           double w[])
{
    int i;
    int k;
    int k1;
    int k2;

    for (i = 0; i < n; i++) {
        w[i] = 0.0;
        k1 = ia[i];
        k2 = ia[i + 1];


/*        for (k = k1; k < k2; k++) {
            w[i] = w[i] + a[k] * x[ja[k]];
        }*/


        int32_t K = k2 - k1;
/*        for (k = 0; k < K; k++) {
            w[i] = w[i] + a[k1 + k] * x[ja[k1 + k]];
        }*/

        int32_t m = 0;
/*        for (k = 0; k < K/4; k++) {
            w[i] = w[i] + a[k1 + m] * x[ja[k1 + m]];
            w[i] = w[i] + a[k1 + m + 1] * x[ja[k1 + m + 1]];
            w[i] = w[i] + a[k1 + m + 2] * x[ja[k1 + m + 2]];
            w[i] = w[i] + a[k1 + m + 3] * x[ja[k1 + m + 3]];

            m += 4;
        }*/

/*
        // "No holes" assumption
        for (k = 0; k < K/4; k++) {
            w[i] = w[i] + a[k1 + m] * x[ja[k1 + m]];
            w[i] = w[i] + a[k1 + m + 1] * x[ja[k1 + m] + 1];
            w[i] = w[i] + a[k1 + m + 2] * x[ja[k1 + m] + 2];
            w[i] = w[i] + a[k1 + m + 3] * x[ja[k1 + m] + 3];

            m += 4;
        }*/

/*
        // Scalar replacement
        int32_t p;
        for (k = 0; k < K/4; k++) {
            p = ja[k1 + m];

            w[i] = w[i] + a[k1 + m] * x[p];
            w[i] = w[i] + a[k1 + m + 1] * x[p + 1];
            w[i] = w[i] + a[k1 + m + 2] * x[p + 2];
            w[i] = w[i] + a[k1 + m + 3] * x[p + 3];

            m += 4;
        }*/

/*
        // Vectorization is applicable now
        __m256d a_vec, x_vec;
        __m128d acc_vec = _mm_setzero_pd();
        int32_t p;
        for (k = 0; k < K/4; k++) {
            p = ja[k1 + m];
*//*
            w[i] = w[i] + a[k1 + m] * x[p];
            w[i] = w[i] + a[k1 + m + 1] * x[p + 1];
            w[i] = w[i] + a[k1 + m + 2] * x[p + 2];
            w[i] = w[i] + a[k1 + m + 3] * x[p + 3];*//*

            a_vec = _mm256_loadu_pd(a + k1 + m);
            x_vec = _mm256_loadu_pd(x + p);

            acc_vec = _mm_add_pd(acc_vec, dot_product_avx(a_vec, x_vec));

            m += 4;
        }

        _mm_store_sd(&w[i], acc_vec);*/

        // Sum up at the end
        __m256d a_vec, x_vec;
        __m256d acc_vec = _mm256_setzero_pd();
        int32_t p;
        for (k = 0; k < K/4; k++) {
            p = ja[k1 + m];
/*            w[i] = w[i] + a[k1 + m] * x[p];
            w[i] = w[i] + a[k1 + m + 1] * x[p + 1];
            w[i] = w[i] + a[k1 + m + 2] * x[p + 2];
            w[i] = w[i] + a[k1 + m + 3] * x[p + 3];*/

            a_vec = _mm256_loadu_pd(a + k1 + m);
            x_vec = _mm256_loadu_pd(x + p);

            acc_vec = _mm256_add_pd(acc_vec, _mm256_mul_pd(a_vec, x_vec));

            m += 4;
        }

        // Sum up components of accumulator
        acc_vec = _mm256_hadd_pd(acc_vec, acc_vec);

        // Performance loss here is negligible
        w[i] = ((double*) &acc_vec)[0] + ((double*) &acc_vec)[2];

        // Remainder
        for (k = (K/4) * 4; k < K; k++) {
            w[i] = w[i] + a[k1 + k] * x[ja[k1 + k]];
        }

    }
    return;
}

void diagonal_pointer_csr_avx(int n, int nz_num, int ia[], int ja[], int ua[])
{
    int i;
    int j;
    int j1;
    int j2;
    int k;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        for (j = j1; j < j2; j++) {
            if (ja[j] == i) {
                ua[i] = j;
            }
        }

    }
    return;
}

/*
  [FastCode] Computes the incomplete LU factorization of a matrix in CSR-VI format

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.

    Input, double A[NZ_NUM], the matrix values.

    Input, int32_t UA[N], the index of the diagonal element of each row.

    Output, double L[NZ_NUM], the values of the ILU factorization of A.
*/
void ilu_csr_avx(int n, int nz_num, int ia[], int ja[], double a[], int ua[],
            double l[])
{
    int *iw;
    int i;
    int j;
    int jj;
    int jrow;
    int jw;
    int k;
    double tl;

    iw = (int *) malloc(n * sizeof(int));
/*
  Copy A.
*/
  /* [OLD] [BEGIN]
    for (k = 0; k < nz_num; k++) {
        l[k] = a[k];
    }
    [OLD] [END] */

    // Second optimization
    int m = 0;
    // TODO: Doesn't pay off

    __m256d a_vec;
    for (k = 0; k < nz_num/4; k++) {
/*        l[m] = a[m];
        l[m + 1] = a[m + 1];
        l[m + 2] = a[m + 2];
        l[m + 3] = a[m + 3];*/

        a_vec = _mm256_loadu_pd(a + m);

        _mm256_storeu_pd(l + m, a_vec);

        m += 4;
    }

    // Process remainder
    for (k = (nz_num/4) * 4; k < nz_num; k++) {
        l[k] = a[k];
    }

    for (i = 0; i < n; i++) {
/*
  IW points to the nonzero entries in row I.
*/
  		/* [OLD] [START]
        for (j = 0; j < n; j++) {
            iw[j] = -1;
        }
		   [OLD] [END] */

        // Second optimization
        // TODO: WARNING INTEGERS (Doesn't pay off)
        m = 0;
        for (j = 0; j < n/4; j++) {

            iw[m] = -1;
            iw[m + 1] = -1;
            iw[m + 2] = -1;
            iw[m + 3] = -1;

            m += 4;
        }

        // Process remainder
        for (j = (n/4) * 4; j < n; j++) {
            iw[j] = -1;
        }

        /* [OLD] [START]
        for (k = ia[i]; k <= ia[i + 1] - 1; k++) {
            iw[ja[k]] = k;
        }
		   [OLD] [END] */

        // Fourth optimization
        // TODO: Doesn't pay off
        m = 0;
        int K = ia[i + 1] - ia[i];
        int32_t ind = ja[ia[i]];
        for (k = 0; k < K/4; k++) {
/*
            iw[ja[ia[i] + m]] = ia[i] + m;
            iw[ja[ia[i] + m + 1]] = ia[i] + m + 1;
            iw[ja[ia[i] + m + 2]] = ia[i] + m + 2;
            iw[ja[ia[i] + m + 3]] = ia[i] + m + 3;*/

/*
            iw[ind + m] = ia[i] + m;
            iw[ind + m + 1] = ia[i] + m + 1;
            iw[ind + m + 2] = ia[i] + m + 2;
            iw[ind + m + 3] = ia[i] + m + 3;*/

            iw[ind + m] = ia[i] + m;
            iw[ind + m + 1] = ia[i] + m + 1;
            iw[ind + m + 2] = ia[i] + m + 2;
            iw[ind + m + 3] = ia[i] + m + 3;


            m += 4;
        }

        // Process remainder
        for (k = (K/4) * 4; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }
            tl = l[j] * l[ua[jrow]];
            l[j] = tl;

            // ____________________________________________________________
            // ____________________________________________________________

            /* [OLD] [START]
            for (jj = ua[jrow] + 1; jj <= ia[jrow + 1] - 1; jj++) {
                jw = iw[ja[jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[jj];
                }
            }

            [OLD] [END] */

            // TODO: Doesn't pay off
            // Third optimization (If clauses are not exclusive)
            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;

            int32_t b = ja[ua[jrow]];
            int32_t jw0, jw1, jw2, jw3;
            for (jj = 0; jj < K/4; jj++) {

/*                jw = iw[ja[ua[jrow] + m + 1]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 1];

                }

                jw = iw[ja[ua[jrow] + m + 2]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 2];

                }

                jw = iw[ja[ua[jrow] + m + 3]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 3];

                }

                jw = iw[ja[ua[jrow] +  m + 4]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 4];

                }*/


/*                jw = iw[ja[ua[jrow]] + m + 1];

                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 1];

                }

                jw = iw[ja[ua[jrow]] + m + 2];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 2];

                }

                jw = iw[ja[ua[jrow]] + m + 3];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 3];

                }

                jw = iw[ja[ua[jrow]] +  m + 4];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 4];

                }*/

                jw0 = iw[b + m] + 1;
//                jw1 = iw[b + m] + 2;
//                jw2 = iw[b + m] + 3;
//                jw3 = iw[b +  m] + 4;


/*
                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw1 != -1) {
                    l[jw1] = l[jw1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw2 != -1) {
                    l[jw2] = l[jw2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw3 != -1) {
                    l[jw3] = l[jw3] - tl * l[ua[jrow] + m + 4];

                }*/


/*                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0 + 1 != -1) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 + 2 != -1) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0 + 3 != -1) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }*/

/*

                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0  != -2) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 != -3) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0  != -4) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }*/
                int32_t z = ua[jrow] + m;
                __m256d l_vec, l_shift_vec;
                __m256d tl_vec = _mm256_set1_pd(tl);

                if (jw0 < -4 || jw0 > -1) {


/*
                    l[jw0] = l[jw0] - tl * l[z + 1];
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[z + 2];
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[z + 3];
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[z + 4];*/

                    l_vec = _mm256_loadu_pd(l + jw0);
                    l_shift_vec = _mm256_loadu_pd(l + z + 1);

                    l_vec = _mm256_sub_pd(l_vec, _mm256_mul_pd(tl_vec, l_shift_vec));

                    _mm256_storeu_pd(l + jw0, l_vec);

                } else {
                    if (jw0 != -1) {
                        l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                    }


                    if (jw0  != -2) {
                        l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                    }

                    if (jw0 != -3) {
                        l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                    }

                    if (jw0  != -4) {
                        l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                    }
                }

                m += 4;
            }

            // Process remainder
            for (jj = (K/4) * 4; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }

            // ____________________________________________________________
            // ____________________________________________________________


            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CSR_AVX - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CSR_AVX - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = \n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
    }

    /* [OLD] [START]
    for (k = 0; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }
       [OLD] [END] */

    // TODO: Doesn't pay off
    // Second optimization
    m = 0;
    for (k = 0; k < n/4; k++) {

        l[ua[m]] = 1.0 / l[ua[m]];
        l[ua[m + 1]] = 1.0 / l[ua[m + 1]];
        l[ua[m + 2]] = 1.0 / l[ua[m + 2]];
        l[ua[m + 3]] = 1.0 / l[ua[m + 3]];

        m += 4;
    }

    // Process remainder
    for (k = (n/4) * 4; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }


/*
  Free memory.
*/
    free(iw);

    return;
}


/* [FastCode] LUS_CR applies the incomplete LU preconditioner.
              The linear system M * Z = R is solved for Z.  M is the incomplete
              LU preconditioner matrix, and R is a vector supplied by the user.
              So essentially, we're solving L * U * Z = R.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.

    Input, double L[NZ_NUM], the matrix values.

    Input, int32_t UA[N], the index of the diagonal element of each row.

    Input, double R[N], the right hand side.

    Output, double Z[N], the solution of the system M * Z = R.
*/
void lus_csr_avx(int n, int nz_num, int ia[], int ja[], double l[], int ua[],
            double r[], double z[])
{
    int i;
    int j;
    double *w;

    w = (double *) malloc(n * sizeof(double));
/*
  Copy R in.
*/
  	/* [OLD] [START]
    for (i = 0; i < n; i++) {
        w[i] = r[i];
    }
       [OLD] [END] */

    // Second optimization
    int32_t m = 0;
    __m256d r_vec;
    for (i = 0; i < n/4; i++) {

        // Load four doubles from r
        r_vec = _mm256_loadu_pd(r + m);

        _mm256_storeu_pd(w + m, r_vec);
/*
        w[m] = r[m];
        w[m + 1] = r[m + 1];
        w[m + 2] = r[m + 2];
        w[m + 3] = r[m + 3];*/

        m += 4;
    }

    // Process remainder
    for (i = (n/4) * 4; i < n; i++) {
        w[i] = r[i];
    }


/*
  Solve L * w = w where L is unit lower triangular.
*/
  	/* [OLD] [START]
    for (i = 1; i < n; i++) {
        for (j = ia[i]; j < ua[i]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
        }
    }
       [OLD] [END] */

    // Unroll inner loop
    for (i = 1; i < n; i++) {

        /* [OLD] [START]
            for (j = ia[i]; j < ua[i]; j++) {
                w[i] = w[i] - l[j] * w[ja[j]];
            }
       [OLD] [END] */

        // First modification
//        for (j = 0; j < (ua[i] - ia[i]); j++) {
//            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
//        }

        int32_t K = ua[i] - ia[i];

        // First optimization
//        for (j = 0; j < K/4; j++) {
//            w[i] = w[i] - l[ia[i] + 4 * j] * w[ja[ia[i] + 4 * j]];
//            w[i] = w[i] - l[ia[i] + 4 * j + 1] * w[ja[ia[i] + 4 * j + 1]];
//            w[i] = w[i] - l[ia[i] + 4 * j + 2] * w[ja[ia[i] + 4 * j + 2]];
//            w[i] = w[i] - l[ia[i] + 4 * j + 3] * w[ja[ia[i] + 4 * j + 3]];
//        }

        // Process remainder
//        for (j = (K/4) * 4; j < K; j++) {
//            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
//
//        }

        // Second optimization
        m = 0;
        __m256d w_vec = _mm256_loadu_pd(w + i);
        __m256d l_vec;

        // TODO: Doesn't pay off
        for (j = 0; j < K/4; j++) {
            w[i] = w[i] - l[ia[i] + m] * w[ja[ia[i] + m]];
            w[i] = w[i] - l[ia[i] + m + 1] * w[ja[ia[i] + m + 1]];
            w[i] = w[i] - l[ia[i] + m + 2] * w[ja[ia[i] + m + 2]];
            w[i] = w[i] - l[ia[i] + m + 3] * w[ja[ia[i] + m + 3]];

            m += 4;
        }

        // Process remainder
        for (j = (K/4) * 4; j < K; j++) {
            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
        }

    }


/*


  Solve U * w = w, where U is upper triangular.
*/
    for (i = n - 1; 0 <= i; i--) {
    	/* [OLD] [START]
        for (j = ua[i] + 1; j < ia[i + 1]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
        }
        w[i] = w[i] / l[ua[i]];
        [OLD] [END] */

        int32_t K = ia[i + 1] - ua[i] - 1;
        // First modification
//        for (j = 0; j < K; j++) {
//            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
//        }

        // Optimization (steps similar to last optimizations in this method)

        m = 0;
        for (j = 0; j < K/4; j++) {
            w[i] = w[i] - l[ua[i] + m + 1] * w[ja[ua[i] + m + 1]];
            w[i] = w[i] - l[ua[i] + m + 2] * w[ja[ua[i] + m + 2]];
            w[i] = w[i] - l[ua[i] + m + 3] * w[ja[ua[i] + m + 3]];
            w[i] = w[i] - l[ua[i] + m + 4] * w[ja[ua[i] + m + 4]];

            m += 4;
        }

        // Process remainder
        for (j = (K/4) * 4; j < K; j++) {
            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
        }


        w[i] = w[i] / l[ua[i]];

    }
/*
  Copy Z out.
*/
  	/* [OLD] [START]
    for (i = 0; i < n; i++) {
        z[i] = w[i];
    }
       [OLD] [END] */

    // First optimization
    m = 0;
    for (i = 0; i < n/4; i++) {
/*        z[m] = w[m];
        z[m + 1] = w[m + 1];
        z[m + 2] = w[m + 2];
        z[m + 3] = w[m + 3];*/

        _mm256_storeu_pd(z + m, _mm256_loadu_pd(w + m));

        m += 4;
    }

    // Process remainder
    for (i = (n/4) * 4; i < n; i++) {
        z[i] = w[i];
    }


/*
  Free memory.
*/
    free(w);

    return;
}


void pmgmres_ilu_csr_avx(int n, int nz_num, int ia[], int ja[], double a[],
                    double x[], double rhs[], int itr_max, int mr, double tol_abs,
                    double tol_rel)
{
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int i;
    int itr;
    int itr_used;
    int j;
    int k;
    int k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;
    int *ua;
    double **v;
    int verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int *) malloc(n * sizeof(int));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));

    rearrange_csr_avx(n, nz_num, ia, ja, a);

    diagonal_pointer_csr_avx(n, nz_num, ia, ja, ua);

    ilu_csr_avx(n, nz_num, ia, ja, a, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_AVX\n");
        printf("  Number of unknowns = %d\n", n);
    }

    for (itr = 0; itr < itr_max; itr++) {
        ax_csr_avx(n, nz_num, ia, ja, a, x, r);


        // ____________________________________________________________
        // ____________________________________________________________

        /* [OLD] [START]
        for (i = 0; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }
		  [OLD] [END] */

        // Second optimization
        int32_t m = 0;
        __m256d r_vec, rhs_vec;
        for (i = 0; i < n/4; i++) {
/*            r[m] = rhs[m] - r[m];
            r[m + 1] = rhs[m + 1] - r[m + 1];
            r[m + 2] = rhs[m + 2] - r[m + 2];
            r[m + 3] = rhs[m + 3] - r[m + 3];*/

            r_vec = _mm256_loadu_pd(r + m);
            rhs_vec = _mm256_loadu_pd(rhs + m);

            r_vec = _mm256_sub_pd(rhs_vec, r_vec);
            _mm256_storeu_pd(r + m, r_vec);

            m += 4;
        }

        // Process remainder
        for (i = (n/4) * 4; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        // ____________________________________________________________
        // ____________________________________________________________

        lus_csr_avx(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_csr_avx(n, r, r));

        if (verbose) {
            printf("  ITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
        }

        // ____________________________________________________________
        // ____________________________________________________________

        /* [OLD] [START]

        for (i = 0; i < n; i++) {
            v[0][i] = r[i] / rho;
        }
		   [OLD] [END] */

        // Use last optimization technique
        m = 0;
        __m256d rho_vec = _mm256_set1_pd(rho);
        __m256d v_vec;
        for (i = 0; i < n/4; i++) {
/*            v[0][m] = r[m] / rho;
            v[0][m + 1] = r[m + 1] / rho;
            v[0][m + 2] = r[m + 2] / rho;
            v[0][m + 3] = r[m + 3] / rho;*/

            r_vec = _mm256_loadu_pd(r + m);
            r_vec = _mm256_div_pd(r_vec, rho_vec);
            _mm256_storeu_pd(&v[0][m], r_vec);


            m += 4;
        }

        // Process remainder
        for (i = (n/4) * 4; i < n; i++) {
            v[0][i] = r[i] / rho;
        }


        // ____________________________________________________________
        // ____________________________________________________________


        g[0] = rho;
        /* [OLD] [START]
        for (i = 1; i < mr + 1; i++) {
            g[i] = 0.0;
        }
           [OLD] [END] */

        m = 0;
        for (i = 0; i < mr/4; i++) {
/*            g[m + 1] = 0.0;
            g[m + 2] = 0.0;
            g[m + 3] = 0.0;
            g[m + 4] = 0.0;*/

            _mm256_storeu_pd(g + m + 1, _mm256_setzero_pd());

            m += 4;
        }

        // Process remainder
        for (i = (mr/4) * 4; i < mr; i++) {
            g[i + 1] = 0.0;
        }



        // ____________________________________________________________
        // ____________________________________________________________

        for (i = 0; i < mr + 1; i++) {
        	/* [OLD] [START]
            for (j = 0; j < mr; j++) {
                h[i][j] = 0.0;
            }
             [OLD] [END] */

            // Use last optimization technique
            m = 0;
            for (j = 0; j < mr/4; j++) {
/*                h[i][m] = 0.0;
                h[i][m + 1] = 0.0;
                h[i][m + 2] = 0.0;
                h[i][m + 3] = 0.0;*/

                _mm256_storeu_pd(&h[i][m], _mm256_setzero_pd());

                m += 4;
            }

            // Process remainder
            for (j = (mr/4) * 4; j < mr; j++) {
                h[i][j] = 0.0;
            }
        }

        // ____________________________________________________________
        // ____________________________________________________________
        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csr_avx(n, nz_num, ia, ja, a, v[k], v[k + 1]);

            lus_csr_avx(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_csr_avx(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_csr_avx(n, v[k + 1], v[j]);
                // ____________________________________________________________
                // ____________________________________________________________


                /* [OLD] [START]
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }
                    [OLD] [END] */

                // Use last optimization technique
                m = 0;
                __m256d factor_vec = _mm256_set1_pd(h[j][k]);
                __m256d v_vec_shift;
                for (i = 0; i < n/4; i++) {
/*                    v[k + 1][m] = v[k + 1][m] - h[j][k] * v[j][m];
                    v[k + 1][m + 1] = v[k + 1][m + 1] - h[j][k] * v[j][m + 1];
                    v[k + 1][m + 2] = v[k + 1][m + 2] - h[j][k] * v[j][m + 2];
                    v[k + 1][m + 3] = v[k + 1][m + 3] - h[j][k] * v[j][m + 3];*/

                    v_vec = _mm256_loadu_pd(&v[k+1][m]);
                    v_vec_shift = _mm256_loadu_pd(&v[j][m]);
                    v_vec = _mm256_sub_pd(v_vec, _mm256_mul_pd(factor_vec, v_vec_shift));
                    _mm256_storeu_pd(&v[k+1][m], v_vec);

                    m += 4;
                }

                // Process remainder
                for (i = (n/4) * 4; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }

                // ____________________________________________________________
                // ____________________________________________________________

            }
            h[k + 1][k] = sqrt(r8vec_dot_csr_avx(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {
                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_csr_avx(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;
                    // ____________________________________________________________
                    // ____________________________________________________________


                    /* [OLD] [START]
                    for (i = 0; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }
                        [OLD] [END] */

                    // Use last optimization technique
                    m = 0;
/*                    __m256d htmp_vec = _mm256_set1_pd(htmp);
                    __m256d v_vec_shift;*/
                    for (i = 0; i < n/4; i++) {
                        v[k + 1][m] = v[k + 1][m] - htmp * v[j][m];
                        v[k + 1][m + 1] = v[k + 1][m + 1] - htmp * v[j][m + 1];
                        v[k + 1][m + 2] = v[k + 1][m + 2] - htmp * v[j][m + 2];
                        v[k + 1][m + 3] = v[k + 1][m + 3] - htmp * v[j][m + 3];

/*                        v_vec = _mm256_loadu_pd(&v[k + 1][m]);
                        v_vec_shift = _mm256_loadu_pd(&v[j][m]);
                        v_vec_shift = _mm256_mul_pd(htmp_vec, v_vec_shift);

                        v_vec = _mm256_sub_pd(v_vec, v_vec_shift);
                        _mm256_store_pd(&v[k+1][m], v_vec);*/


                        m += 4;
                    }

                    // Process remainder
                    for (i = (n/4) * 4; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }


                    // ____________________________________________________________
                    // ____________________________________________________________

                }
                h[k + 1][k] = sqrt(r8vec_dot_csr_avx(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {
            	/* [OLD] [START]
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }
                    [OLD] [END] */

                // Use last optimization technique
                m = 0;
                //__m256d h_vec = _mm256_set1_pd(h[k + 1][k]);
                for (i = 0; i < n/4; i++) {
                    v[k + 1][m] = v[k + 1][m] / h[k + 1][k];
                    v[k + 1][m + 1] = v[k + 1][m + 1] / h[k + 1][k];
                    v[k + 1][m + 2] = v[k + 1][m + 2] / h[k + 1][k];
                    v[k + 1][m + 3] = v[k + 1][m + 3] / h[k + 1][k];
/*
                    v_vec = _mm256_loadu_pd(&v[k + 1][m]);
                    v_vec = _mm256_div_pd(v_vec, h_vec);
                    _mm256_storeu_pd(&v[k + 1][m], v_vec);*/

                    m += 4;
                }

                // Process remainder
                for (i = (n/4) * 4; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }

            }

            if (0 < k) {
            	// ____________________________________________________________
                // ____________________________________________________________

                /* [OLD] [START]
                for (i = 0; i < k + 2; i++) {
                    y[i] = h[i][k];
                }
                   [OLD] [END] */

                // Use last optimization technique
                m = 0;
                // TODO: No potential for vectorization (see same structure later in the code of this function)
                for (i = 0; i < (k + 2)/4; i++) {
                    y[m] = h[m][k];
                    y[m + 1] = h[m + 1][k];
                    y[m + 2] = h[m + 2][k];
                    y[m + 3] = h[m + 3][k];

                    m += 4;
                }

                // Process remainder
                for (i = ((k + 2)/4) * 4; i < k + 2; i++) {
                    y[i] = h[i][k];
                }


                // ____________________________________________________________
                // ____________________________________________________________

                /* [OLD] [START]
                for (j = 0; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }
                   [OLD] [END] */

                // Use last optimization technique
                // TODO: Not optimizable
                m = 0;
                for (j = 0; j < k/4; j++) {
                    mult_givens(c[m], s[m], m, y);
                    mult_givens(c[m + 1], s[m + 1], m + 1, y);
                    mult_givens(c[m + 2], s[m + 2], m + 2, y);
                    mult_givens(c[m + 3], s[m + 3], m + 3, y);

                    m += 4;
                }

                // Process remainder
                for (j = (k/4) * 4; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }


                // ____________________________________________________________
                // ____________________________________________________________

                /* [OLD] [START]
                for (i = 0; i < k + 2; i++) {
                    h[i][k] = y[i];
                }
                   [OLD] [END] */

                // Use last optimization technique
                // TODO: No potential for vectorization (see same structure later in the code of this function)
                m = 0;
                for (i = 0; i < (k + 2)/4; i++) {
                    h[m][k] = y[m];
                    h[m + 1][k] = y[m + 1];
                    h[m + 2][k] = y[m + 2];
                    h[m + 3][k] = y[m + 3];

                    m += 4;
                }

                // Process remainder
                for (i = ((k + 2)/4) * 4; i < k + 2; i++) {
                    h[i][k] = y[i];
                }

                // ____________________________________________________________
                // ____________________________________________________________
            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];
            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("  K   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];
        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];

            /* [OLD] [BEGIN]
            for (j = i + 1; j < k + 1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
            }
            y[i] = y[i] / h[i][i];
            [OLD] [END] */

            // Second optimization (Scalar replacement and evaluating sums)
            int32_t K = k - i;
            m = 0;
            double y_tmp = y[i];
            //__m256d h_vec, y_vec;
            //__m128d y_tmp_vec = _mm_set_sd(y[i]);
            // TODO: No potential for vectorization
            for (j = 0; j < K/4; j++) {
/*
                // Load four doubles from h
                h_vec = _mm256_loadu_pd(&h[i][i + m + 1]);

                // Load four doubles from y
                y_vec = _mm256_loadu_pd(y + i + m + 1);

                y_tmp_vec = _mm_add_pd(y_tmp_vec, dot_product(h_vec, y_vec));*/
                y_tmp -= h[i][i + m + 1] * y[i + m + 1];
                y_tmp -= h[i][i + m + 2] * y[i + m + 2];
                y_tmp -= h[i][i + m + 3] * y[i + m + 3];
                y_tmp -= h[i][i + m + 4] * y[i + m + 4];

                m += 4;
            }

            //_mm_store_sd(&y_tmp, y_tmp_vec);

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                y_tmp -= h[i][i + 1 + j] * y[i + 1 + j];
            }
/*
            // Store value
            y[i] = y_tmp;

            y[i] = y[i] / h[i][i]; */

            // Contract the two above lines
            y[i] = y_tmp / h[i][i];

        }
        for (i = 0; i < n; i++) {
        	/* [OLD] [START]
            for (j = 0; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
            }
              [OLD] [END] */

            // TODO: Doesn't pay off (check again)
            // Second optimization (Scalar replacement)
            m = 0;
            double x_tmp = x[i];
            __m128d x_tmp_vec = _mm_set_sd(x[i]);
            for (j = 0; j < (k + 1)/4; j++) {
                __m256d v_vec = _mm256_set_pd(v[m + 3][i], v[m + 2][i], v[m + 1][i], v[m][i]);

                __m256d y_vec = _mm256_loadu_pd(y + m);

                x_tmp_vec = _mm_add_pd(x_tmp_vec, dot_product_avx(v_vec, y_vec));

/*                x_tmp += v[m][i] * y[m];
                x_tmp += v[m + 1][i] * y[m + 1];
                x_tmp += v[m + 2][i] * y[m + 2];
                x_tmp += v[m + 3][i] * y[m + 3];*/

                m += 4;
            }

            _mm_store_sd(&x_tmp, x_tmp_vec);

            // Process remainder
            for (j = ((k + 1)/4) * 4; j < k + 1; j++) {
                x_tmp += v[j][i] * y[j];
            }

            // Store value
            x[i] = x_tmp;
        }
        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_AVX:\n");
        printf("  Iterations = %d\n", itr_used);
        printf("  Final residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);
    return;
}




