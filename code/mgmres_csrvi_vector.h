#ifndef CODE_MGMRES_CSRVI_VECTOR_H
#define CODE_MGMRES_CSRVI_VECTOR_H

/*** SSE ***/
void pmgmres_ilu_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                      double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                      double tol_rel);
void ax_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], double x[],
             double w[]);
void diagonal_pointer_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[] );
void ilu_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
              double l[]);
void lus_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
               double r[], double z[]);

#endif //CODE_MGMRES_CSRVI_VECTOR_H
