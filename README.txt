/-----------------------------------------------------------------------------\
|                      Preconditioned Restarted GMRES                         |
\-----------------------------------------------------------------------------/


All source code is located in the code/ folder. The mgmres.c file the baseline 
implementation of the Preconditioned Restarted GMRES algorithm. The 
mgmres_prb.c file contains the validation and runtime measurement functions.


How to compile mgmres_prb:
    Simply change into the code/ folder and run 'make'.

    In the Makefile line 13 you can set the compilation mode. Possible values
    are 'debug', 'testing', 'production' and 'panalyis'.
    Per default the mode is set to 'testing' where all optimizations flags are
    set and assertions are checked (unlike in 'production'). The 'panalysis'
    mode enables the performance analysis.


Possible options for correctness tests on band matrices:

    -test_band_csrvi_ilp_correctness    
            Tests whether ILP optimizations (loop unrolling, scalar 
            replacement, etc.) yield the correct results. It tests different 
            loop unrolling factors (no unrolling, unrolling by 4, 8, 16 and 32).

    -csr_sse_correctness
            Tests whether SSE optimizations using the CSR matrix storage format
            yield the correct results.

    -csrvi_sse_correctness
            Tests whether SSE optimizations using the CSR-VI matrix storage
            format yield the correct results.

    -csr_avx_correctness
            Tests whether AVX optimizations using the CSR matrix storage format
            yield the correct results.

    -csrvi_avx_correctness
            Tests whether AVX optimizations using the CSR-VI matrix storage
            format yield the correct results.


Possible options for runtime measurements:
 
    -test_band_time
            Evaluates the runtime of the baseline implementation and all
            optimizations (ILP optimizations with different loop unrolling
            factors, SSE (using CSR and CSR-VI matrix storage format) and AVX
            optimizations (using CSR and CSR-VI matrix storage format) and 
            calculates the speedup of each optimizations and marks the fasted
            optimization (green) and the slowest optimizations (red).

    -test_band_time_preserve_sparsity
            Evaluates the runtime of the baseline implementation and all ILP 
            optimizations on a sparse band matrix confirming a given sparsity
            level (set within the method) and calculates the speedup of each
            optimization.


Configuration of the used sorting algorithm:
    The baseline implementation of the GMRES algorithm used bubblesort as 
    sorting algorithm. We changed the code such that you can specify which
    algorithm should be used (in the baseline and in all optimized 
    implementations).
    In the main function in mgmres_prb.c you set the use_quicksort flag to 
    indicate whether quicksort should be used or bubblesort.

