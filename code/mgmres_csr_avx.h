//
// Created by lennart on 5/29/16.
//

// AVX
#include <immintrin.h>
#include <stdint.h>

#ifndef CODE_MGMRES_CSR_AVX_H
#define CODE_MGMRES_CSR_AVX_H
__m128d dot_product_avx(const __m256d x, const __m256d y);

double r8vec_dot_csr_avx(int n, double a[], double b[]);

void ax_csr_avx(int n, int nz_num, int ia[], int ja[], double a[], double x[],
                double w[]);

void diagonal_pointer_csr_avx(int n, int nz_num, int ia[], int ja[], int ua[]);

void ilu_csr_avx(int n, int nz_num, int ia[], int ja[], double a[], int ua[],
                 double l[]);

void lus_csr_avx(int n, int nz_num, int ia[], int ja[], double l[], int ua[],
                 double r[], double z[]);

void pmgmres_ilu_csr_avx(int n, int nz_num, int ia[], int ja[], double a[],
                         double x[], double rhs[], int itr_max, int mr, double tol_abs,
                         double tol_rel);



#endif //CODE_MGMRES_CSR_AVX_H
