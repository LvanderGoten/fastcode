import sys
import re
import statistics
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np


def print_statistics(single_obs):

    for obs in single_obs:
        print("OPT: {}, mean: {}, stddev: {}, median: {}, min: {}, max: {}".format(obs.label, obs.mean(), obs.stddev(),
                                                                                   obs.median(), obs.min(), obs.max()))


class PlotSetting:
    def __init__(self):
        self.opt_ids = []
        self.colors = []
        self.legend = []
        self.markers = []

        self.suptitle = ""
        self.title = ""
        self.xticks = []
        self.yticks = []
        self.xlim = []
        self.ylim = []
        self.unit = 1
        self.xlabel = ""
        self.fname = ""
        self.labels = []

    def set_opt_ids(self, opt_ids):
        self.opt_ids = opt_ids

    def set_colors(self, colors):
        self.colors = colors

    def set_legend(self, legend):
        self.legend = legend

    def set_markers(self, markers):
        self.markers = markers

    def set_suptitle(self, suptitle):
        self.suptitle = suptitle

    def set_title(self, title):
        self.title = title

    def set_xticks(self, xticks):
        self.xticks = xticks

    def set_yticks(self, yticks):
        self.yticks = yticks

    def set_xlim(self, xlim):
        self.xlim = xlim

    def set_ylim(self, ylim):
        self.ylim = ylim

    def set_unit(self, unit):
        self.unit = unit

    def set_xlabel(self, xlabel):
        self.xlabel = xlabel

    def set_fname(self, fname):
        self.fname = fname

    def set_labels(self, labels):
        self.labels = labels


def plot_optimization_observations(single_obs, plot_setting):

    # Number of different optimization levels (also includes data sets that are not plotted)
    n = len(single_obs)

    # Number of data sets to plot
    n_sets = len(plot_setting.opt_ids)

    # Define x axis
    xx = plot_setting.xticks

    fig = plt.figure()
    for k in range(n_sets):
        opt_id = plot_setting.opt_ids[k]

        # Find optimization level to plot
        for i in range(n):
            if single_obs[i].label == opt_id:

                # Plot
                yy = single_obs[i].reduce()
                plt.plot(xx, [y/plot_setting.unit for y in yy], c=plot_setting.colors[k],
                         marker=plot_setting.markers[k],
                         zorder=3)

    ax = fig.gca()
    ax.yaxis.grid(True, color='w', linewidth=3, linestyle='-', zorder=0)
    ax.set_axis_bgcolor('#e9eaeb')
    ax.tick_params(direction='out')
    ax.ticklabel_format(axis="y", style="sci", useOffset=False)

    ax.get_yaxis().get_major_formatter().set_useOffset(False)

    plt.text(0.0, 1.075, plot_setting.suptitle, fontsize=11, fontweight='bold', horizontalalignment='left', transform=ax.transAxes)
    plt.text(0.0, 1.025, plot_setting.title, fontsize=11, horizontalalignment='left', transform=ax.transAxes)
    plt.xticks(list(range(1, plot_setting.xlim[1])))
    plt.xlim(plot_setting.xlim)
    plt.ylim(plot_setting.ylim)
    plt.xlabel(plot_setting.xlabel, fontsize=15, fontweight='bold')

    if plot_setting.legend:
        plt.legend(plot_setting.opt_ids, loc='upper left')

    for label in plot_setting.labels:
        plt.text(label[0], label[1], label[3], rotation=label[2])

    plt.savefig(plot_setting.fname, bbox_inches='tight')


def plot_bar(speedup_set, plot_setting):
    n = len(speedup_set)

    n_sets = len(plot_setting.opt_ids)

    xx = list(range(1, len(plot_setting.opt_ids) + 1))
    yy = []
    fig = plt.figure()
    ax = fig.gca()

    for k in range(n_sets):
        opt_id = plot_setting.opt_ids[k]

        for i in range(n):
            if speedup_set[i][0] == opt_id:

                yy += [speedup_set[i][1]]

    plt.bar(xx, yy,  width=0.5, align='center', zorder=3)
    plt.text(0.0, 1.075, plot_setting.suptitle, fontsize=11, fontweight='bold', horizontalalignment='left', transform=ax.transAxes)
    plt.text(0.0, 1.025, plot_setting.title, fontsize=11, horizontalalignment='left', transform=ax.transAxes)
    ax.yaxis.grid(True, color='w', linewidth=1, linestyle='-', zorder=0)
    ax.set_axis_bgcolor('#e9eaeb')
    ax.set_xticks(xx)
    ax.tick_params(direction='out')
    ax.set_xticklabels(plot_setting.opt_ids, rotation=45, ha='right')

    plt.plot([0, xx[-1] + 1], [1, 1], '--', color='k', linewidth=1, zorder=4)
    plt.ylim(plot_setting.ylim)
    plt.savefig(plot_setting.fname, bbox_inches='tight')


def to_single_optimization_observations(timing_units):

    # Number of data points
    n = len(timing_units)

    # Number of different optimizations
    n_opts = len(timing_units[0].get_timing_keys())

    opts_obs = [SingleOptimizationObservation(label) for label in timing_units[0].get_timing_keys()]

    for timing_unit in timing_units:

        for i in range(n_opts):
            opts_obs[i].add_point(timing_unit.config, timing_unit.get_timing_value(opts_obs[i].label))

    return opts_obs


class Configuration:
    def __init__(self, runs, n, n_ld, n_ud):
        self.runs = runs
        self.n = n
        self.n_ld = n_ld
        self.n_ud = n_ud


class SingleOptimizationObservation:
    def __init__(self, label):
        self.label = label
        self.points = []

    def add_point(self, config, cycles):
        self.points += [[config, cycles]]

    def reduce(self):
        return [point[1] for point in self.points]

    def mean(self):
        return statistics.mean(self.reduce())

    def stddev(self):
        return statistics.stdev(self.reduce())

    def median(self):
        return statistics.median(self.reduce())

    def min(self):
        return min(self.reduce())

    def max(self):
        return max(self.reduce())


def to_timing_units(params, cycles):
    n = len(params)

    timing_units = []
    for i in range(n):
        timing_units += [TimingUnit(params[i], cycles[i])]

    return timing_units


class TimingUnit:
    def __init__(self, params, cycles):
        self.timing = {}

        params_vals = [float(param.split(' = ')[1]) for param in params]
        self.config = Configuration(*params_vals)

        for cycle in cycles:
            label, value = cycle.split(' => ')

            self.timing[label.lstrip().rstrip()] = float(value)

    def get_timing_value(self, key):
        return self.timing.get(key)

    def get_timing_keys(self):
        return self.timing.keys()


def main():

    # Match configuration
    match_config = r'AVG_Time\(.*\):'

    # Match clock cycles
    match_cycles = r'\[(.*)\]'

    # Match Name-Value pair
    match_pair = r'[a-zA-Z0-9\_\s\(\)\-]+\s=>?(?:\s(?!\d+:)[0-9\.]+)+'

    # Get stdin
    time_report_path = None
    if len(sys.argv) > 1:
        time_report_path = sys.argv[1]
        base_name = sys.argv[1].split(".")[0]
    else:
        print("No input file specified!")
        sys.exit(-1)

    with open(time_report_path) as time_report_handle:
        time_report = time_report_handle.read()

        # Retrieve all configurations
        configs = re.findall(match_config, time_report)

        # Retrieve all timings
        timings = re.findall(match_cycles, time_report)

        N = len(configs)

        # Match configuration parameters
        params = [re.findall(match_pair, config) for config in configs]

        # Match cycles for each configuration
        cycles = [re.findall(match_pair, timing) for timing in timings]
        speedup = cycles[-1]
        cycles = cycles[:-1]

        timing_units = to_timing_units(params, cycles)

        single_obs = to_single_optimization_observations(timing_units)

        print_statistics(single_obs)
        plot_setting = PlotSetting()

        # Abscissae (as specified by data)
        n_vals = [x[0].n for x in single_obs[0].points]
        n_log_ticks = np.log2(n_vals)

        # General settings
        plot_setting.set_markers(["o", "s", "^", "v", "h", "D", "x"])
        plot_setting.set_colors(['#000000',  'darkorange', '#888888', '#FF00FF', '#F80000', '#FFFFFF',
                                 'darkmagenta', 'darkcyan', 'darkorange', 'darkgreen', 'darkslategray'])

        # SCALAR
        plot_setting.set_opt_ids(['BASELINE (CSR)', 'UL(1) (CSR-VI)', 'UL(4) (CSR-VI)', 'UL(8) (CSR-VI)',
                                  'UL(16) (CSR-VI)', 'UL(32) (CSR-VI)'])

        # VECTOR
        plot_setting.set_opt_ids(['BASELINE (CSR)', 'UL(8) (CSR-VI)',
                                  'SSE (CSR-VI)', 'SSE (CSR)',
                                  'AVX (CSR-VI)', 'AVX (CSR)'])

        plot_setting.set_suptitle("GMRES (double precision) on Intel Core i7-6600U CPU @ 2.60GHz")
        plot_setting.set_title("Runtime [$10^9$ Cycles] vs. matrix dimension $2^k$")

        plot_setting.set_xticks(n_log_ticks)
        plot_setting.set_yticks([])

        plot_setting.set_xlim([0, 19])
        plot_setting.set_ylim([0, 3])
        plot_setting.set_xlabel("$k$")
        plot_setting.set_unit(1E9)
        plot_setting.set_fname(base_name + "_cycles.png")

        plot_optimization_observations(single_obs, plot_setting)

        # Speedup
        plot_setting.set_opt_ids(['UL(1) (CSR-VI)', 'UL(4) (CSR-VI)', 'UL(8) (CSR-VI)',
                                  'UL(16) (CSR-VI)', 'UL(32) (CSR-VI)',
                                  'SSE (CSR-VI)', 'SSE (CSR)',
                                  'AVX (CSR-VI)', 'AVX (CSR)'])
        plot_setting.set_fname(base_name + "_speedup.png")
        plot_setting.set_title(r"Speedup of different optimizations (with respect to $\mathrm{BASELINE (CSR)}$)")
        speedup = [(p[0].lstrip().rstrip(), float(p[1])) for p in [x.split(' => ') for x in speedup]]

        plot_bar(speedup, plot_setting)

    if len(sys.argv) > 2:
        flop_count_path = sys.argv[2]
    else:
        sys.exit(-1)

    # Performance plot
    # Flop counts
    with open(flop_count_path) as flop_count_handle:
        flop_count_report = flop_count_handle.read()

        regex_config = r'\((\s?[a-zA-Z]+\s=\s\d+,?)+\)'

        flop_counts = [float(line.split(' ')[-1]) for line in flop_count_report.splitlines()]

        # Change observations
        for single_ob in single_obs:
            for i in range(len(single_ob.points)):
                _, cycles = single_ob.points[i]

                single_ob.points[i][1] = flop_counts[i]/cycles

        plot_setting.set_title("Performance [Flops/Cycle] vs. matrix dimension $2^k$")
        plot_setting.set_fname(base_name + "_performance.png")
        plot_setting.set_opt_ids(['BASELINE (CSR)', 'UL(1) (CSR-VI)', 'UL(4) (CSR-VI)', 'UL(8) (CSR-VI)',
                                  'UL(16) (CSR-VI)', 'UL(32) (CSR-VI)'])
        plot_setting.set_xlim([0, 20])
        plot_setting.set_ylim([0, 0.12])
        plot_setting.set_unit(1.0)
        plot_setting.set_labels([(15.5, 0.057, 0, 'Baseline/UL(1)'),
                                 (15.5, 0.097, 0, 'UL(8)/UL(16)'),
                                 (15.5, 0.09, 0, 'UL(4)/UL(32)')])

        # DEBUG
        plot_setting.set_legend([])

        plot_optimization_observations(single_obs, plot_setting)

if __name__ == "__main__":
    main()
