#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdint.h>
#include <string.h>

#include "mgmres.h"
#include "mgmres_csrvi.h"

#ifdef PANALYSIS
uint64_t flops;
#endif


/*
 * [FastCode] Rearrange using QUICKSORT (runs much faster than original Bubblesort)
 */
// Temporary arrays used for sorting
int32_t *tmp_ja_csrvi;

int comp_csrvi_idx(const void *i, const void *j) {

    return ( tmp_ja_csrvi[*(int*)i] - tmp_ja_csrvi[*(int*)j] );
}

void rearrange_csrvi_qs(int n, int nz_num, int ia[], int ja[], int va[], double vala[])
{
    double dtemp;
    int i;
    int is;
    int itemp;
    int j;
    int j1;
    int j2;
    int k;

    int32_t *idx = (int32_t *) malloc(nz_num * sizeof(int32_t));

    // Allocate backup arrays
    int32_t *ja_backup = (int32_t *) malloc(nz_num * sizeof(int32_t));
    int32_t *va_backup = (int32_t *) malloc(nz_num * sizeof(int32_t));

    memcpy(ja_backup, ja, nz_num * sizeof(int32_t));
    memcpy(va_backup, va, nz_num * sizeof(int32_t));

    for (k = 0; k < nz_num; k++) {
        idx[k] = k;
    }

    tmp_ja_csrvi = ja;

    for (i = 0; i < n; i++) {
        j1 = ia[i];
        j2 = ia[i + 1];
        is = j2 - j1;

        qsort(idx + j1, is, sizeof(int32_t), comp_csrvi_idx);
    }

    int32_t curr_idx;
    for (k = 0; k < nz_num; k++) {
        curr_idx = idx[k];

        // Permute
        ja[k] = ja_backup[curr_idx];
        va[k] = va_backup[curr_idx];
    }

    free(ja_backup);
    free(va_backup);
    free(idx);

    return;
}

// CSR-VI TRANSFORMATION REQUIRED
/*
 * [FastCode] GMRES with Incomplete LU Preconditioner operating on CSR-VI matrix format
 */
/*
  Parameters:

    Input, int32_t N, the order of the linear system.

    Input, int32_t NZ_NUM, the number of nonzero matrix values.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double A[NZ_NUM], the matrix values.

    Input/output, double X[N]; on input, an approximation to
    the solution.  On output, an improved approximation.

    Input, double RHS[N], the right hand side of the linear system.

    Input, int32_t ITR_MAX, the maximum number of (outer) iterations to take.

    Input, int32_t MR, the maximum number of (inner) iterations to take.
    MR must be less than N.

    Input, double TOL_ABS, an absolute tolerance applied to the
    current residual.

    Input, double TOL_REL, a relative tolerance comparing the
    current residual to the initial residual.
*/
/* [START] [OLD]
void pmgmres_ilu_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double a[],
                    double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                    double tol_rel)
   [END] [OLD] */
void pmgmres_ilu_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                       double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                       double tol_rel)
{
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;/* [FastCode] R8VEC_DOT computes the dot product of a pair of R8VEC's.
 *
  Parameters:

    Input, int32_t N, the number of entries in the vectors.

    Input, double A1[N], A2[N], the two vectors to be considered.

    Output, double R8VEC_DOT, the dot product of the vectors.
*/
    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));

/* [START] [OLD]
    rearrange_cr(n, nz_num, ia, ja, a);
   [END] [OLD] */

    /* [START] [NEW] */
    rearrange_csrvi(n, nz_num, ia, ja, va, vala);
    /* [END] [NEW] */


/* [START] [OLD]
    diagonal_pointer_cr(n, nz_num, ia, ja, ua);
   [END] [OLD] */

    /* [START] [NEW] */
    diagonal_pointer_csrvi(n, nz_num, ia, ja, ua);
    /* [END] [NEW] */

/* [START] [OLD]
    ilu_cr(n, nz_num, ia, ja, a, ua, l);
   [END] [OLD] */


    /* [START] [NEW] */
    ilu_csrvi(n, nz_num, ia, ja, va, vala, ua, l);
    /* [END] [NEW] */


    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI\n");
        printf("  Number of unknowns = %d\n", n);
    }

    for (itr = 0; itr < itr_max; itr++) {
/* [START] [OLD]
        ax_cr(n, nz_num, ia, ja, a, x, r);
   [END] [OLD] */

        /* [START] [NEW] */
        ax_csrvi(n, nz_num, ia, ja, va, vala, x, r);
        /* [END] [NEW] */

        for (i = 0; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }
        #ifdef PANALYSIS
        flops += n;
        #endif

/* [START] [OLD]
        lus_cr(n, nz_num, ia, ja, l, ua, r, r);
   [END] [OLD] */


        /* [START] [NEW] */
        lus_csrvi(n, nz_num, ia, ja, l, ua, r, r);
        /* [END] [NEW] */

        rho = sqrt(r8vec_dot(n, r, r));

        if (verbose) {
            printf("  ITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
            #ifdef PANALYSIS
            flops += 1;
            #endif
        }

        for (i = 0; i < n; i++) {
            v[0][i] = r[i] / rho;
        }
        #ifdef PANALYSIS
        flops += n;
        #endif

        g[0] = rho;
        for (i = 1; i < mr + 1; i++) {
            g[i] = 0.0;
        }

        for (i = 0; i < mr + 1; i++) {
            for (j = 0; j < mr; j++) {
                h[i][j] = 0.0;
            }
        }

        for (k = 0; k < mr; k++) {
            k_copy = k;

/* [START] [OLD]
            ax_cr(n, nz_num, ia, ja, a, v[k], v[k + 1]);
   [END] [OLD] */


            /* [START] [NEW] */
            ax_csrvi(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);
            /* [END] [NEW] */

/* [START] [OLD]
            lus_cr(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);
[END] [OLD] */


            /* [START] [NEW] */
            lus_csrvi(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);
            /* [END] [NEW] */

            av = sqrt(r8vec_dot(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot(n, v[k + 1], v[j]);
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }
                #ifdef PANALYSIS
                flops += 2 * n;
                #endif
            }
            h[k + 1][k] = sqrt(r8vec_dot(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {
                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;
                    #ifdef PANALYSIS
                    flops += 1;
                    #endif
                    for (i = 0; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }
                    #ifdef PANALYSIS
                    flops += 2 * n;
                    #endif
                }
                h[k + 1][k] = sqrt(r8vec_dot(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }
                #ifdef PANALYSIS
                flops += n;
                #endif
            }

            if (0 < k) {
                for (i = 0; i < k + 2; i++) {
                    y[i] = h[i][k];
                }
                for (j = 0; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }
                for (i = 0; i < k + 2; i++) {
                    h[i][k] = y[i];
                }
            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];
            #ifdef PANALYSIS
            flops += 9;
            #endif
            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("  K   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];
        #ifdef PANALYSIS
        flops += 1;
        #endif
        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];
            for (j = i + 1; j < k + 1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
                #ifdef PANALYSIS
                flops += 2;
                #endif
            }
            y[i] = y[i] / h[i][i];
            #ifdef PANALYSIS
            flops += 1;
            #endif
        }
        for (i = 0; i < n; i++) {
            for (j = 0; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
                #ifdef PANALYSIS
                flops += 2;
                #endif
            }
        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI:\n");
        printf("  Iterations = %d\n", itr_used);
        printf("  Final residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}


/*
  [FastCode] Computes the incomplete LU factorization of a matrix in CSR-VI format

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double A[NZ_NUM], the matrix values.

    Input, int32_t UA[N], the index of the diagonal element of each row.

    Output, double L[NZ_NUM], the values of the ILU factorization of A.
*/
/* [START] [OLD]
void ilu_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double a[], int32_t ua[],
            double l[])
  [END] [OLD] */
void ilu_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
               double l[])
{
    int32_t *iw;
    int32_t i;
    int32_t j;
    int32_t jj;
    int32_t jrow;
    int32_t jw;
    int32_t k;
    double tl;

    iw = (int32_t *) malloc(n * sizeof(int32_t));
/*
  Copy A.
*/
    for (k = 0; k < nz_num; k++) {
        /* [START] [OLD]
        l[k] = a[k];
           [END] [OLD] */

        /* [START] [NEW] */
        l[k] = vala[va[k]];
        /* [END] [NEW] */
    }

    for (i = 0; i < n; i++) {
/*
  IW points to the nonzero entries in row I.
*/
        for (j = 0; j < n; j++) {
            iw[j] = -1;
        }

        for (k = ia[i]; k <= ia[i + 1] - 1; k++) {
            iw[ja[k]] = k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }
            tl = l[j] * l[ua[jrow]];
            #ifdef PANALYSIS
            flops += 1;
            #endif
            l[j] = tl;
            for (jj = ua[jrow] + 1; jj <= ia[jrow + 1] - 1; jj++) {
                jw = iw[ja[jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[jj];
                    #ifdef PANALYSIS
                    flops += 2;
                    #endif
                }
            }
            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CSR_VI - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CSR_VI - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = \n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
        #ifdef PANALYSIS
        flops += 1;
        #endif
    }

    for (k = 0; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }
    #ifdef PANALYSIS
    flops += n;
    #endif
/*
  Free memory.
*/
    free(iw);

    return;
}


/* [FastCode] LUS_CR applies the incomplete LU preconditioner.
              The linear system M * Z = R is solved for Z.  M is the incomplete
              LU preconditioner matrix, and R is a vector supplied by the user.
              So essentially, we're solving L * U * Z = R.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double L[NZ_NUM], the matrix values.

    Input, int32_t UA[N], the index of the diagonal element of each row.

    Input, double R[N], the right hand side.

    Output, double Z[N], the solution of the system M * Z = R.
*/
void lus_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
            double r[], double z[])
{
    int32_t i;
    int32_t j;
    double *w;

    w = (double *) malloc(n * sizeof(double));
/*
  Copy R in.
*/
    for (i = 0; i < n; i++) {
        w[i] = r[i];
    }
/*
  Solve L * w = w where L is unit lower triangular.
*/
    for (i = 1; i < n; i++) {
        for (j = ia[i]; j < ua[i]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
            #ifdef PANALYSIS
            flops += 2;
            #endif
        }
    }
/*
  Solve U * w = w, where U is upper triangular.
*/
    for (i = n - 1; 0 <= i; i--) {
        for (j = ua[i] + 1; j < ia[i + 1]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
            #ifdef PANALYSIS
            flops += 2;
            #endif
        }
        w[i] = w[i] / l[ua[i]];
        #ifdef PANALYSIS
        flops += 1;
        #endif
    }
/*
  Copy Z out.
*/
    for (i = 0; i < n; i++) {
        z[i] = w[i];
    }
/*
  Free memory.
*/
    free(w);

    return;
}


/*
  [FastCode] AX_CR computes A*x for a matrix stored in sparse compressed row form.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double A[NZ_NUM], the matrix values.

    Input, double X[N], the vector to be multiplied by A.

    Output, double W[N], the value of A*X.
*/
/* [START] [OLD]
 * void ax_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double a[], double x[],
           double w[])
   [END] [OLD]
 */
void ax_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], double x[],
           double w[])
{
    int32_t i;
    int32_t k;
    int32_t k1;
    int32_t k2;

    for (i = 0; i < n; i++) {
        w[i] = 0.0;
        k1 = ia[i];
        k2 = ia[i + 1];
        for (k = k1; k < k2; k++) {
            /* [START] [OLD]
            w[i] = w[i] + a[k] * x[ja[k]];
             [END] [OLD */

            /* [START] [NEW] */
            w[i] = w[i] + vala[va[k]] * x[ja[k]];
            /* [END] [NEW] */
            #ifdef PANALYSIS
            flops += 2;
            #endif

        }
    }
    return;
}


/*

    REARRANGE_CR sorts a sparse compressed row matrix.

  Discussion:

    This routine guarantees that the entries in the CR matrix
    are properly sorted.

    After the sorting, the entries of the matrix are rearranged in such
    a way that the entries of each column are listed in ascending order
    of their column values.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], the compressed row index vector.

    Input/output, int32_t JA[NZ_NUM], the column indices of the matrix values.
    On output, the order of the entries of JA may have changed because of
    the sorting.

    Input/output, double A[NZ_NUM], the matrix values.  On output, the
    order of the entries may have changed because of the sorting.
*/
/* [START] [OLD]
 * void rearrange_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double a[])
 * [END] [OLD]
 * */
void rearrange_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[])
{
    if (use_quicksort) {
        rearrange_csrvi_qs(n, nz_num, ia, ja, va, vala);
    } else {

        double dtemp;
        int32_t i;
        int32_t is;
        int32_t itemp;
        int32_t j;
        int32_t j1;
        int32_t j2;
        int32_t k;

        for (i = 0; i < n; i++) {
            j1 = ia[i];
            j2 = ia[i + 1];

            // Number of elements in current row
            is = j2 - j1;

            for (k = 1; k < is; k++) {
                for (j = j1; j < j2 - k; j++) {
                    if (ja[j + 1] < ja[j]) {

                        // Swap column indices
                        itemp = ja[j + 1];
                        ja[j + 1] = ja[j];
                        ja[j] = itemp;
    /*
     * [START] [OLD]
                        dtemp = a[j + 1];
                        a[j + 1] = a[j];
                        a[j] = dtemp;
       [END]  [OLD]
    */

                        // Swap values (Relink va)
                        /* [START] [NEW] */
                        dtemp = va[j + 1];
                        va[j + 1] = va[j];
                        va[j] = dtemp;
                        /* [END] [NEW] */
                    }
                }
            }
        }
    }
    return;
}

// NO CSR-VI TRANSFORMATION REQUIRED!
/*
  Purpose:

    DIAGONAL_POINTER_CR finds diagonal entries in a sparse compressed row matrix.

  Discussion:

    The matrix A is assumed to be stored in compressed row format.  Only
    the nonzero entries of A are stored.  The vector JA stores the
    column index of the nonzero value.  The nonzero values are sorted
    by row, and the compressed row vector IA then has the property that
    the entries in A and JA that correspond to row I occur in indices
    IA[I] through IA[I+1]-1.

    The array UA can be used to locate the diagonal elements of the matrix.

    It is assumed that every row of the matrix includes a diagonal element,
    and that the elements of each row have been ascending sorted.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.  On output,
    the order of the entries of JA may have changed because of the sorting.

    Output, int32_t UA[N], the index of the diagonal element of each row.
*/
void diagonal_pointer_csrvi(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[])
{
    int32_t i;
    int32_t j;
    int32_t j1;
    int32_t j2;
    int32_t k;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        for (j = j1; j < j2; j++) {
            if (ja[j] == i) {
                ua[i] = j;
            }
        }

    }
    return;
}

