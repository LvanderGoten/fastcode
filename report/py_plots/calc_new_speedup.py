def main():
    with open("/home/lennartv/ETH/FastCode/Project/report/data/final_run/final_run_sparsity_icc_gcc_speedup_raw.dat", "r") as fraw:
        lines = fraw.readlines()

        cont_speedup = []
        for i in range(len(lines)):
            line = lines[i].strip()

            values = line.split(',')
            baseline = float(values[0])
            line_speedup = []
            for j in range(1, len(values)):
                curr_val = float(values[j])

                line_speedup += [baseline/curr_val]

            cont_speedup += [line_speedup]

        tot_speedup = [0 for _ in cont_speedup[0]]
        for i in range(len(cont_speedup)):
            for j in range(len(cont_speedup[i])):
                tot_speedup[j] += cont_speedup[i][j]

        for speedup in tot_speedup:
            print(speedup)


if __name__ == "__main__":
    main()