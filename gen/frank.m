function A = frank(N)

% Define first sub diagonal
f_sub = [N-1:-1:1 0]';

% Define other diagonals
F = [f_sub];

for n=N:-1:1
    f = [zeros(1, N-n) n:-1:1]';
    F = [F f];
end

A = spdiags(F, -1:N-1, N, N);
end

