#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdint.h>

#include "x86intrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"
#include "xmmintrin.h"

#include "mgmres.h"
#include "mgmres_csr_vector.h"      // for r8vec_dot_sse() and r8vec_dot_avx()
#include "mgmres_csrvi_vector.h"
#include "mgmres_csrvi.h"

#ifdef PANALYSIS
uint64_t flops;
#endif


/*************************************************************************************************/
/*   SSE                                                                                         */
/*************************************************************************************************/

void pmgmres_ilu_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                       double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                       double tol_rel)
{
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;
    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));

    rearrange_csrvi(n, nz_num, ia, ja, va, vala);

    diagonal_pointer_csrvi_sse(n, nz_num, ia, ja, ua);

    ilu_csrvi_sse(n, nz_num, ia, ja, va, vala, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_SSE\n");
        printf("  Number of unknowns = %d\n", n);
    }

    for (itr = 0; itr < itr_max; itr++) {
        ax_csrvi_sse(n, nz_num, ia, ja, va, vala, x, r);

        /* OLD
        for (i = 0; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }
        */

        /* NEW */
        for (i = 0; (i+1) < n; i += 2) {
            _mm_store_pd(&r[i], _mm_sub_pd(_mm_loadu_pd(&rhs[i]), _mm_loadu_pd(&r[i])));
        }
        for ( ; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        #ifdef PANALYSIS
        flops += n;
        #endif

        lus_csrvi_sse(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_sse(n, r, r));

        if (verbose) {
            printf("  ITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
            #ifdef PANALYSIS
            flops += 1;
            #endif
        }

        /* OLD
        for (i = 0; i < n; i++) {
            v[0][i] = r[i] / rho;
        }
        */

        /* NEW */
        __m128d rho_vec = _mm_set1_pd(rho);
        for (i = 0; (i+1) < n; i += 2) {
            _mm_storeu_pd(&v[0][i], _mm_div_pd(_mm_loadu_pd(&r[i]), rho_vec));
        }
        for ( ; i < n; i++) {
            v[0][i] = r[i] / rho;
        }

        #ifdef PANALYSIS
        flops += n;
        #endif

        g[0] = rho;
        for (i = 1; i < mr + 1; i++) {
            g[i] = 0.0;
        }

        for (i = 0; i < mr + 1; i++) {
            for (j = 0; j < mr; j++) {
                h[i][j] = 0.0;
            }
        }

        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csrvi_sse(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);

            lus_csrvi_sse(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_sse(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_sse(n, v[k + 1], v[j]);

                /* OLD
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }
                */

                /* NEW */
                __m128d vki;
                __m128d hjk_mul_vji;
                for (i = 0; (i+1) < n; i += 2) {
                    vki = _mm_loadu_pd(&v[k+1][i]);
                    hjk_mul_vji = _mm_mul_pd(_mm_set_pd(h[j][k], h[j][k]), _mm_loadu_pd(&v[j][i]));
                    vki = _mm_sub_pd(vki, hjk_mul_vji);
                    _mm_storeu_pd(&v[k+1][i], vki);
                }
                for ( ; i < n; i++) {
                    v[k+1][i] = v[k+1][i] - h[j][k] * v[j][i];
                }
                
                #ifdef PANALYSIS
                flops += 2 * n;
                #endif
            }

            h[k + 1][k] = sqrt(r8vec_dot_sse(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {
                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_sse(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;
                    #ifdef PANALYSIS
                    flops += 1;
                    #endif

                    /* OLD
                    for (i = 0; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }
                    */

                    /* NEW */
                    __m128d htmps = _mm_set_pd(htmp, htmp);
                    __m128d htmp_mul_vji;
                    for (i = 0; (i+1) < n; i += 2) {
                        htmp_mul_vji = _mm_mul_pd(htmps, _mm_loadu_pd(&v[j][i]));
                        _mm_storeu_pd(&v[k+1][i], _mm_sub_pd(_mm_loadu_pd(&v[k+1][i]), htmp_mul_vji));
                    }
                    for ( ; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }

                    #ifdef PANALYSIS
                    flops += 2 * n;
                    #endif
                }
                h[k + 1][k] = sqrt(r8vec_dot_sse(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {
                /* OLD
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }
                */

                /* NEW */
                __m128d hkks = _mm_set_pd(h[k+1][k], h[k+1][k]);
                for (i = 0; (i+1) < n; i += 2) {
                    _mm_storeu_pd(&v[k+1][i], _mm_div_pd(_mm_loadu_pd(&v[k+1][i]), hkks));
                }
                for ( ; i < n; i++) {
                    v[k+1][i] = v[k+1][i] / h[k+1][k];
                }
                
                #ifdef PANALYSIS
                flops += n;
                #endif
            }

            if (0 < k) {
                for (i = 0; i < k + 2; i++) {
                    y[i] = h[i][k];
                }
                for (j = 0; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }
                for (i = 0; i < k + 2; i++) {
                    h[i][k] = y[i];
                }
            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];
            #ifdef PANALYSIS
            flops += 9;
            #endif
            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("  K   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];
        #ifdef PANALYSIS
        flops += 1;
        #endif

        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];

            /* OLD */
            for (j = i + 1; j < k + 1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
            }

            /* NEW */
            /*
            __m128d sum = _mm_setzero_pd();
            __m128d hij_mul_yj;
            for (j = i+1; (j+1) < k+1; j += 2) {
                hij_mul_yj = _mm_mul_pd(_mm_loadu_pd(&h[i][j]), _mm_loadu_pd(&y[j]));
                sum = _mm_add_pd(hij_mul_yj, sum);
            }
            _mm_store_sd(&y[i], _mm_sub_pd(_mm_set1_pd(y[i], y[i]), sum));
            for ( ; j < k+1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
            }
            */

            #ifdef PANALYSIS
            flops += 2 * (k+1) - (i+1);
            #endif

            y[i] = y[i] / h[i][i];
            #ifdef PANALYSIS
            flops += 1;
            #endif
        }
        for (i = 0; i < n; i++) {
            /* OLD */
            for (j = 0; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
            }

            /* NEW */
            /*
            __m128d sum = _mm_setzero_pd();
            __m128d vji_mul_yj;
            for (j = 0; (j+1) < k+1; j += 2) {
                vji_mul_yj = _mm_mul_pd(_mm_loadu_pd(&v[j][i]), _mm_loadu_pd(&y[j]));
                sum = _mm_add_pd(vji_mul_yj, sum);
            }
            _mm_store_sd(&x[i], _mm_add_pd(_mm_set_pd(x[i], x[i]), sum));
            for ( ; j < k+1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
            }
            */

            #ifdef PANALYSIS
            flops += 2 * (k+1);
            #endif
        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_SSE:\n");
        printf("  Iterations = %d\n", itr_used);
        printf("  Final residual = %e\n", rho);
    }

    // Free memory.
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}

void ilu_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
               double l[])
{
    int32_t *iw;
    int32_t i;
    int32_t j;
    int32_t jj;
    int32_t jrow;
    int32_t jw;
    int32_t k;
    double tl;

    iw = (int32_t *) malloc(n * sizeof(int32_t));

    // Copy A
    for (k = 0; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }

    int K;
    int m = 0;

    for (i = 0; i < n; i++) {
        // IW points to the nonzero entries in row I.
        for (j = 0; j < n; j++) {
            iw[j] = -1;
        }

        for (k = ia[i]; k <= ia[i + 1] - 1; k++) {
            iw[ja[k]] = k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }
            tl = l[j] * l[ua[jrow]];
            #ifdef PANALYSIS
            flops += 1;
            #endif
            l[j] = tl;

            /* OLD
            for (jj = ua[jrow] + 1; jj <= ia[jrow + 1] - 1; jj++) {
                jw = iw[ja[jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[jj];
                    #ifdef PANALYSIS
                    flops += 2;
                    #endif
                }
            }
            */

            /* NEW */
            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;
            int32_t z = iw[ja[ua[jrow]]];
            int32_t jw0;
            int32_t ut;
            __m128d tl_vec = _mm_set1_pd(tl);
            for (jj = 0; jj < K/2; jj++) {
                jw0 = z + m + 1;
                
                ut = ua[jrow] + m;

                __m128d l_vec, l_shift_vec;
                if (jw0 < -2 || jw0 > -1) {
                    l_vec = _mm_loadu_pd(l + jw0);
                    l_shift_vec = _mm_loadu_pd(l + ut + 1);

                    l_vec = _mm_sub_pd(l_vec, _mm_mul_pd(tl_vec, l_shift_vec));

                    _mm_storeu_pd(l + jw0, l_vec);
                } else {
                    if (jw0 != -1) {
                        l[jw0] = l[jw0] - tl * l[ut + 1];
                    }
                    if (jw0  != -2) {
                        l[jw0 + 1] = l[jw0 + 1] - tl * l[ut + 2];
                    }
                }

                m += 2;
            }
            // process remainder
            for (jj = (K/2) * 2; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];
                }
            }

            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CSR_VI - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CSR_VI - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = %d\n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
        #ifdef PANALYSIS
        flops += 1;
        #endif
    }

    for (k = 0; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }
    #ifdef PANALYSIS
    flops += n;
    #endif

    // Free memory
    free(iw);

    return;
}

void lus_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
            double r[], double z[])
{
    int32_t i;
    int32_t j;
    double *w;

    w = (double *) malloc(n * sizeof(double));

    // Copy R in

    /* OLD
    for (i = 0; i < n; i++) {
        w[i] = r[i];
    }
    */

    /* NEW */
    for (i = 0; (i+1) < n; i += 2) {
        _mm_storeu_pd(&w[i], _mm_loadu_pd(&r[i]));
    }
    for ( ; i < n; i++) {
        w[i] = r[i];
    }

    // Solve L * w = w where L is unit lower triangular.
    for (i = 1; i < n; i++) {
        for (j = ia[i]; j < ua[i]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
        }

        #ifdef PANALYSIS
        flops += 2 * (ua[i] - ia[i]);
        #endif
    }

    // Solve U * w = w, where U is upper triangular.
    for (i = n - 1; 0 <= i; i--) {
        for (j = ua[i] + 1; j < ia[i + 1]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
            #ifdef PANALYSIS
            flops += 2;
            #endif
        }
        w[i] = w[i] / l[ua[i]];
        #ifdef PANALYSIS
        flops += 1;
        #endif
    }

    // Copy Z out

    /* OLD
    for (i = 0; i < n; i++) {
        z[i] = w[i];
    }
    */

    /* NEW */
    for (i = 0; (i+1) < n; i += 2) {
        _mm_storeu_pd(&z[i], _mm_loadu_pd(&w[i]));
    }
    for ( ; i < n; i++) {
        z[i] = w[i];
    }

    // Free memory
    free(w);

    return;
}

void ax_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], double x[],
           double w[])
{
    int32_t i;
    int32_t k;
    int32_t k1, k2;

    for (i = 0; i < n; i++) {
        w[i] = 0.0;
        k1 = ia[i];
        k2 = ia[i + 1];
        for (k = k1; k < k2; k++) {
            w[i] = w[i] + vala[va[k]] * x[ja[k]];
        }
        #ifdef PANALYSIS
        flops += 2 * (k2 - k1);
        #endif
    }

    return;
}

void diagonal_pointer_csrvi_sse(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[])
{
    int32_t i, j;
    int32_t j1, j2;
    int32_t k;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        for (j = j1; j < j2; j++) {
            if (ja[j] == i) {
                ua[i] = j;
            }
        }
    }
}

