//
// Created by dejan on 5/27/16.
//
#include <stdint.h>

// AVX
#include <immintrin.h>

#ifndef CODE_MGMRES_CSRVI_ILP_VECTOR_H
#define CODE_MGMRES_CSRVI_ILP_VECTOR_H

__m128d dot_product(const __m256d x, const __m256d y);

void pmgmres_ilu_csrvi_ilp_vector(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                                  double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                                  double tol_rel);

void ilu_csrvi_ilp_vector(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
                          double l[]);

void lus_csrvi_ilp_vector(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                          double *r, double *z);

void ax_csrvi_ilp_vector(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, double *x,
                         double *w);

void rearrange_csrvi_ilp_vector(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[]);

void diagonal_pointer_csrvi_ilp_vector(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[]);

double r8vec_dot_vector(int n, double a[], double b[]);
#endif //CODE_MGMRES_CSRVI_ILP_VECTOR_H
