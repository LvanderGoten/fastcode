CC = gcc

# command line arguments
# mode
#   * debug:      debug mode, essentially -O0 -g
#   * testing:    all optimization flags
#                 assertions are checked
#   * production: all optimization flags
#                 assertions disabled
#	* panalysis:  performance analysis, enables
#				  counting of flops
# - usage: make mode=debug
mode ?= testing

# case:
# whether to measure performance or LLC misses
case ?= performance

# vectorization
vect ?= enabled


# processor architecture
architecture ?= 

# compiler flags
# mode
# debug mode
ifeq ($(mode), debug)
	CFLAGS = -w -c -O0 -g

# testing & production mode
# to generate annotated assembly code: -S -fsource-asm
else
	CFLAGS = -w -c -O0 -m64 -march=native
endif

# disable assertions for production
ifeq ($(mode), production)
	CFLAGS += -DNDEBUG
endif


# vectorization
# vectorization enabled: sepcify ISA extension
ifeq ($(vect), enabled)
	CFLAGS += -xSSE4.1 -msse3 -mavx
# vectorization disabled
else
	CFLAGS += -fno-tree-vectorize
endif


# GCC architecture flags
AFLAGS = -m64 -march=core-avx2 -mavx2 -fno-tree-vectorize -fno-strict-aliasing
# GCC debugging flags
DFLAGS = -g
CFLAGS = -O0 $(AFLAGS) $(DFLAGS)
LIBS   = -lm

OBJS   = mgmres.o validate.o perf.o mgmres_csrvi.o mgmres_csrvi_ilp.o mgmres_csr_vector.o mgmres_csrvi_vector.o mgmres_csr_avx.o mgmres_csrvi_avx.o

all: mgmres_prb

mgmres_prb: mgmres_prb.c $(OBJS)
	$(CC) $(CFLAGS) -o mgmres_prb mgmres_prb.c $(OBJS) $(LIBS)

mgmres.o: mgmres.h mgmres.c
	$(CC) -c $(CFLAGS) -o mgmres.o mgmres.c

validate.o: validate.h validate.c
	$(CC) -c $(CFLAGS) -o validate.o validate.c

perf.o: perf.h perf.c
	$(CC) -c $(CFLAGS) -o perf.o perf.c

mgmres_csrvi.o: mgmres_csrvi.h mgmres_csrvi.c
	$(CC) -c $(CFLAGS) -o mgmres_csrvi.o mgmres_csrvi.c

mgmres_csrvi_ilp.o: mgmres_csrvi_ilp.h mgmres_csrvi_ilp.c
	$(CC) -c $(CFLAGS) -o mgmres_csrvi_ilp.o mgmres_csrvi_ilp.c

mgmres_csr_vector.o: mgmres_csr_vector.h mgmres_csr_vector.c
	$(CC) -c $(CFLAGS) -o mgmres_csr_vector.o mgmres_csr_vector.c

mgmres_csrvi_vector.o: mgmres_csrvi_vector.h mgmres_csrvi_vector.c
	$(CC) -c $(CFLAGS) -o mgmres_csrvi_vector.o mgmres_csrvi_vector.c

mgmres_csrvi_avx.o: mgmres_csrvi_avx.h mgmres_csrvi_avx.c
	$(CC) -c $(CFLAGS) -o mgmres_csrvi_avx.o mgmres_csrvi_avx.c

mgmres_csr_avx.o: mgmres_csr_avx.h mgmres_csr_avx.c
	$(CC) -c $(CFLAGS) -o mgmres_csr_avx.o mgmres_csr_avx.c

clean:
	-rm $(OBJS)
	-rm mgmres_prb
