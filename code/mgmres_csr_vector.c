#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdint.h>
#include "perf.h"

#include "x86intrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"
#include "xmmintrin.h"
 
#include "mgmres.h"
#include "mgmres_csr_vector.h"

#ifdef PANALYSIS
uint64_t flops;
#endif


/*************************************************************************************************/
/*   SSE                                                                                         */
/*************************************************************************************************/

void ax_csr_sse(int n, int nz_num, int ia[], int ja[], double a[], double x[], double w[])
/*
    Purpose:
        AX_CR computes A*x for a matrix stored in sparse compressed row form.

    Parameters:
        Input, int N, the order of the system.
        Input, int NZ_NUM, the number of nonzeros.
        Input, int IA[N+1], JA[NZ_NUM], the row and column indices
            of the matrix values.  The row vector has been compressed.
        Input, double A[NZ_NUM], the matrix values.
        Input, double X[N], the vector to be multiplied by A.
        Output, double W[N], the value of A*X.
*/
{
    int i;
    int k;
    int k1;
    int k2;

    for (i = 0; i < n; i++) {
        w[i] = 0.0;
        k1 = ia[i];
        k2 = ia[i + 1];
        for (k = k1; k < k2; k++) {
            w[i] = w[i] + a[k] * x[ja[k]];
        }
        #ifdef PANALYSIS
        flops += 2 * (k2 - k1);
        #endif
    }
    return;
}


void ilu_csr_sse(int n, int nz_num, int ia[], int ja[], double a[], int ua[], double l[])
/*
  Purpose:
    ILU_CR computes the incomplete LU factorization of a matrix.

  Parameters:
    Input, int N, the order of the system.
    Input, int NZ_NUM, the number of nonzeros.
    Input, int IA[N+1], JA[NZ_NUM], the row and column indices
        of the matrix values.  The row vector has been compressed.
    Input, double A[NZ_NUM], the matrix values.
    Input, int UA[N], the index of the diagonal element of each row.
    Output, double L[NZ_NUM], the ILU factorization of A.
*/
{
    int *iw;
    int i;
    int j;
    int jj;
    int jrow;
    int jw;
    int k;
    double tl;

    iw = (int *) malloc(n * sizeof(int));

    // Copy A.

    /* OLD
    for (k = 0; k < nz_num; k++) {
        l[k] = a[k];
    }
    */

    /* NEW */
    for (k = 0; (k+1) < nz_num; k += 2) {
        _mm_storeu_pd(&l[k], _mm_loadu_pd(&a[k]));
    }
    for ( ; k < nz_num; k++) {
        l[k] = a[k];
    }

    int K;
    int m = 0;

    for (i = 0; i < n; i++) {
        // IW points to the nonzero entries in row I.
        for (j = 0; j < n; j++) {
            iw[j] = -1;
        }

        for (k = ia[i]; k <= ia[i + 1] - 1; k++) {
            iw[ja[k]] = k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }
            tl = l[j] * l[ua[jrow]];
            #ifdef PANALYSIS
            flops += 1;
            #endif
            l[j] = tl;

            /* OLD
            for (jj = ua[jrow] + 1; jj <= ia[jrow + 1] - 1; jj++) {
                jw = iw[ja[jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[jj];
                    #ifdef PANALYSIS
                    flops += 2;
                    #endif
                }
            }
            */
        
            /* NEW */
            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;
            int32_t b = ja[ua[jrow]];
            int32_t jw0;
            for (jj = 0; jj < K/2; jj++) {
                jw0 = iw[b + m] + 1;

                int32_t z = ua[jrow] + m;
                __m128d l_vec, l_shift_vec;
                __m128d tl_vec = _mm_set1_pd(tl);
                if (jw0 < -2 || jw0 > -1) {
                    l_vec = _mm_loadu_pd(l + jw0);
                    l_shift_vec = _mm_loadu_pd(l + z + 1);
                    
                    l_vec = _mm_sub_pd(l_vec, _mm_mul_pd(tl_vec, l_shift_vec));        
                    
                    _mm_storeu_pd(l + jw0, l_vec);
                } else {
                    if (jw0 != -1) {
                        l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];
                    }
                    if (jw0  != -2) {
                        l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];
                    }
                }

                m += 2;
            }
            // process remainder
            for (jj = (K/2) * 2; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];
                }
            }

            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CR - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CR - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = \n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
        #ifdef PANALYSIS
        flops += 1;
        #endif
    }

    for (k = 0; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }
    #ifdef PANALYSIS
    flops += n;
    #endif

    // Free memory.
    free(iw);

    return;
}

void lus_csr_sse(int n, int nz_num, int ia[], int ja[], double l[], int ua[],
                    double r[], double z[])
/*
  Purpose:
    LUS_CR applies the incomplete LU preconditioner.

  Parameters:
    Input, int N, the order of the system.
    Input, int NZ_NUM, the number of nonzeros.
    Input, int IA[N+1], JA[NZ_NUM], the row and column indices
        of the matrix values.  The row vector has been compressed.
    Input, double L[NZ_NUM], the matrix values.
    Input, int UA[N], the index of the diagonal element of each row.
    Input, double R[N], the right hand side.
    Output, double Z[N], the solution of the system M * Z = R.
*/
{
    int i;
    int j;
    double *w;

    w = (double *) malloc(n * sizeof(double));

    // Copy R in.

    /* OLD
    for (i = 0; i < n; i++) {
        w[i] = r[i];
    }
    */

    /* NEW */
    for (i = 0; (i+1) < n; i += 2) {
        _mm_storeu_pd(&w[i], _mm_loadu_pd(&r[i]));
    }
    for ( ; i < n; i++) {
        w[i] = r[i];
    }

    // Solve L * w = w where L is unit lower triangular.
    for (i = 1; i < n; i++) {
        for (j = ia[i]; j < ua[i]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
            #ifdef PANALYSIS
            flops += 2;
            #endif
        }
    }

    // Solve U * w = w, where U is upper triangular.
    for (i = n - 1; 0 <= i; i--) {
        for (j = ua[i] + 1; j < ia[i + 1]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
            #ifdef PANALYSIS
            flops += 2;
            #endif
        }
        w[i] = w[i] / l[ua[i]];
        #ifdef PANALYSIS
        flops += 1;
        #endif
    }

    // Copy Z out.

    /* OLD
    for (i = 0; i < n; i++) {
        z[i] = w[i];
    }
    */

    /* NEW */
    for (i = 0; (i+1) < n; i += 2) {
        _mm_storeu_pd(&z[i], _mm_loadu_pd(&w[i]));
    }
    for ( ; i < n; i++) {
        z[i] = w[i];
    }

    // Free memory.
    free(w);

    return;
}

void pmgmres_ilu_csr_sse(int n, int nz_num, int ia[], int ja[], double a[],
                            double x[], double rhs[], int itr_max, int mr, double tol_abs, double tol_rel)
/*
  Purpose:
    PMGMRES_ILU_CR applies the preconditioned restarted GMRES algorithm.

  Parameters:
    Input, int N, the order of the linear system.
    Input, int NZ_NUM, the number of nonzero matrix values.
    Input, int IA[N+1], JA[NZ_NUM], the row and column indices
        of the matrix values.  The row vector has been compressed.
    Input, double A[NZ_NUM], the matrix values.
    Input/output, double X[N]; on input, an approximation to
        the solution.  On output, an improved approximation.
    Input, double RHS[N], the right hand side of the linear system.
    Input, int ITR_MAX, the maximum number of (outer) iterations to take.
    Input, int MR, the maximum number of (inner) iterations to take.
        MR must be less than N.
    Input, double TOL_ABS, an absolute tolerance applied to the
        current residual.
    Input, double TOL_REL, a relative tolerance comparing the
        current residual to the initial residual.
*/
{
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int i;
    int itr;
    int itr_used;
    int j;
    int k;
    int k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;
    int *ua;
    double **v;
    int verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int *) malloc(n * sizeof(int));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));

    rearrange_csr_sse(n, nz_num, ia, ja, a);

    diagonal_pointer_csr_sse(n, nz_num, ia, ja, ua);

    ilu_csr_sse(n, nz_num, ia, ja, a, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_SSE\n");
        printf("  Number of unknowns = %d\n", n);
    }

    for (itr = 0; itr < itr_max; itr++) {
        ax_csr_sse(n, nz_num, ia, ja, a, x, r);

        /* OLD
        for (i = 0; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }
        */

        /* NEW */
        for (i = 0; (i+1) < n; i += 2) {
            _mm_storeu_pd(&r[i], _mm_sub_pd(_mm_loadu_pd(&rhs[i]), _mm_loadu_pd(&r[i])));
        }
        for ( ; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        #ifdef PANALYSIS
        flops += n;
        #endif

        lus_csr_sse(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_sse(n, r, r));

        if (verbose) {
            printf("  ITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
            #ifdef PANALYSIS
            flops += 1;
            #endif
        }

        /* OLD
        for (i = 0; i < n; i++) {
            v[0][i] = r[i] / rho;
        }
        */
        
        /* NEW */
        __m128d rho_vec = _mm_set1_pd(rho);
        for (i = 0; (i+1) < n; i += 2) {
            _mm_storeu_pd(&v[0][i], _mm_div_pd(_mm_loadu_pd(&r[i]), rho_vec));
        }
        for ( ; i < n; i++) {
            v[0][i] = r[i] / rho;
        }

        #ifdef PANALYSIS
        flops += n;
        #endif

        g[0] = rho;
        for (i = 1; i < mr + 1; i++) {
            g[i] = 0.0;
        }

        for (i = 0; i < mr + 1; i++) {
            for (j = 0; j < mr; j++) {
                h[i][j] = 0.0;
            }
        }

        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csr_sse(n, nz_num, ia, ja, a, v[k], v[k + 1]);

            lus_csr_sse(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_sse(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_sse(n, v[k + 1], v[j]);

                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }

                #ifdef PANALYSIS
                flops += 2 * n;
                #endif
            }
            h[k + 1][k] = sqrt(r8vec_dot_sse(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {
                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_sse(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;
                    #ifdef PANALYSIS
                    flops += 1;
                    #endif
                    for (i = 0; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }
                    #ifdef PANALYSIS
                    flops += 2 * n;
                    #endif
                }
                h[k + 1][k] = sqrt(r8vec_dot_sse(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }
                #ifdef PANALYSIS
                flops += n;
                #endif
            }

            if (0 < k) {
                for (i = 0; i < k + 2; i++) {
                    y[i] = h[i][k];
                }
                for (j = 0; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }
                for (i = 0; i < k + 2; i++) {
                    h[i][k] = y[i];
                }
            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];
            #ifdef PANALYSIS
            flops += 9;
            #endif
            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("  K   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];
        #ifdef PANALYSIS
        flops += 1;
        #endif
        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];
            for (j = i + 1; j < k + 1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
                #ifdef PANALYSIS
                flops += 2;
                #endif
            }
            y[i] = y[i] / h[i][i];
            #ifdef PANALYSIS
            flops += 1;
            #endif
        }
        for (i = 0; i < n; i++) {
            for (j = 0; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
                #ifdef PANALYSIS
                flops += 2;
                #endif
            }
        }
        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_SSE:\n");
        printf("  Iterations = %d\n", itr_used);
        printf("  Final residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);
    #ifdef PANALYSIS
    printf("Flops counted: %lu\n", flops);
    #endif
    return;
}

double r8vec_dot_sse(int n, double a1[], double a2[])
/*
  Purpose:
    R8VEC_DOT computes the dot product of a pair of R8VEC's.

  Parameters:
    Input, int N, the number of entries in the vectors.
    Input, double A1[N], A2[N], the two vectors to be considered.
    Output, double R8VEC_DOT, the dot product of the vectors.
*/
{
    int i;
    double value = 0.0;

    /* OLD */
    for (i = 0; i < n; i++) {
        value = value + a1[i] * a2[i];
    }

    /* NEW */
    /*
    __m128d sum = _mm_setzero_pd();
    for (i = 0; (i+1) < n; i += 2) {
        sum = _mm_add_pd(sum, _mm_mul_pd(_mm_loadu_pd(&a1[i]), _mm_loadu_pd(&a2[i])));
    }
    sum = _mm_hadd_pd(sum, sum);
    _mm_store_sd(&value, sum);
    for( ; i < n; i++) {
        value = value + a1[i] * a2[i];
    }
    */

    #ifdef PANALYSIS
    flops += 2 * n;
    #endif

    return value;
}

void diagonal_pointer_csr_sse(int n, int nz_num, int ia[], int ja[], int ua[])
 /*
    Purpose:
        DIAGONAL_POINTER_CR finds diagonal entries in a sparse compressed row matrix.

    Parameters:
        Input, int N, the order of the system.
        Input, int NZ_NUM, the number of nonzeros.
        Input, int IA[N+1], JA[NZ_NUM], the row and column indices
            of the matrix values.  The row vector has been compressed.  On output,
            the order of the entries of JA may have changed because of the sorting.
        Output, int UA[N], the index of the diagonal element of each row.
*/
{
    int i;
    int j;
    int j1;
    int j2;
    int k;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        for (j = j1; j < j2; j++) {
            if (ja[j] == i) {
                ua[i] = j;
            }
        }

    }
    return;
}

void rearrange_csr_sse(int n, int nz_num, int ia[], int ja[], double a[])
/*
  Purpose:
    REARRANGE_CR sorts a sparse compressed row matrix.

  Parameters:
    Input, int N, the order of the system.
    Input, int NZ_NUM, the number of nonzeros.
    Input, int IA[N+1], the compressed row index vector.
    Input/output, int JA[NZ_NUM], the column indices of the matrix values.
    On output, the order of the entries of JA may have changed because of
        the sorting.
    Input/output, double A[NZ_NUM], the matrix values.  On output, the
        order of the entries may have changed because of the sorting.
*/
{
    if (use_quicksort) {
        rearrange_csr_qs(n, nz_num, ia, ja, a);
    } else {

        double dtemp;
        int i;
        int is;
        int itemp;
        int j;
        int j1;
        int j2;
        int k;

        for (i = 0; i < n; i++) {
            j1 = ia[i];
            j2 = ia[i + 1];
            is = j2 - j1;

            for (k = 1; k < is; k++) {
                for (j = j1; j < j2 - k; j++) {
                    if (ja[j + 1] < ja[j]) {
                        itemp = ja[j + 1];
                        ja[j + 1] = ja[j];
                        ja[j] = itemp;

                        dtemp = a[j + 1];
                        a[j + 1] = a[j];
                        a[j] = dtemp;
                    }
                }
            }
        }
    }
    return;
}

