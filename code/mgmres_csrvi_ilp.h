//
// Created by lennart on 5/23/16.
//
#include <stdint.h>

#ifndef CODE_MGMRES_CSRVI_ILP_H
#define CODE_MGMRES_CSRVI_ILP_H

// Dot product
double r8vec_dot_unroll(int n, double a[], double b[]);
double r8vec_dot_unroll_4(int n, double a[], double b[]);
double r8vec_dot_unroll_8(int n, double a[], double b[]);
double r8vec_dot_unroll_16(int n, double a[], double b[]);
double r8vec_dot_unroll_32(int n, double a[], double b[]);

// Mat-Vec product
void ax_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int *ia, int *ja, int *va, double *vala, double *x,
                         double *w);
void ax_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int *ia, int *ja, int *va, double *vala, double *x,
                         double *w);
void ax_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int *ia, int *ja, int *va, double *vala, double *x,
                         double *w);
void ax_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int *ia, int *ja, int *va, double *vala, double *x,
                         double *w);
void ax_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int *ia, int *ja, int *va, double *vala, double *x,
                            double *w);

// Find diagonal elements
void diagonal_pointer_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *ua);
void diagonal_pointer_csrvi_ilp_unroll_4 ( int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[] );
void diagonal_pointer_csrvi_ilp_unroll_8 ( int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[] );
void diagonal_pointer_csrvi_ilp_unroll_16 ( int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[] );
void diagonal_pointer_csrvi_ilp_unroll_32 ( int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[] );

// Incomplete LU decomposition
void ilu_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, int32_t *ua,
                          double *l);
void ilu_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, int32_t *ua,
                          double *l);
void ilu_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, int32_t *ua,
                          double *l);
void ilu_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, int32_t *ua,
                          double *l);
void ilu_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, int32_t *ua,
                          double *l);

// Incomplete LU application
void lus_csrvi_ilp_unroll (int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                           double *r, double *z);
void lus_csrvi_ilp_unroll_4 (int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
                    double r[], double z[]);
void lus_csrvi_ilp_unroll_8 (int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
                    double r[], double z[]);
void lus_csrvi_ilp_unroll_16 (int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
                    double r[], double z[]);
void lus_csrvi_ilp_unroll_32 (int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], double l[], int32_t ua[],
                    double r[], double z[]);

void pmgmres_ilu_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala,
                                  double *x, double *rhs, int32_t itr_max, int32_t mr, double tol_abs,
                                  double tol_rel);
void pmgmres_ilu_csrvi_ilp(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala,
                                  double *x, double *rhs, int32_t itr_max, int32_t mr, double tol_abs,
                                  double tol_rel);
void pmgmres_ilu_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala,
                                  double *x, double *rhs, int32_t itr_max, int32_t mr, double tol_abs,
                                  double tol_rel);

// Sorting CSR-VI
void rearrange_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala);
void rearrange_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala);
void rearrange_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala);
void rearrange_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala);
void rearrange_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala);


// Set loop unroll factors
void set_unroll_factors(const int32_t pmgmres_factor, const int32_t ax_factor, const int32_t ilu_factor, const int32_t lus_factor, const int32_t diag_factor, const int32_t rearrange_factor, const int32_t r8vec_factor);

#endif //CODE_MGMRES_CSRVI_ILP_H
