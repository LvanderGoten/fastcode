#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mgmres.h"
#include "mgmres_csrvi.h"

/* [FastCode] Unroll factors */
int32_t pmgmres_unroll_factor = 1;
int32_t ax_unroll_factor = 1;
int32_t ilu_unroll_factor = 1;
int32_t lus_unroll_factor = 1;
int32_t diag_unroll_factor = 1;
int32_t rearrange_unroll_factor = 1;
int32_t r8vec_unroll_factor = 1;


#ifdef PANALYSIS
uint64_t flops;
#endif

/*
 * [FastCode] Set the unroll factor for each function in this C file
 */
void set_unroll_factors(const int32_t pmgmres_factor, const int32_t ax_factor, const int32_t ilu_factor, const int32_t lus_factor, const int32_t diag_factor, const int32_t rearrange_factor, const int32_t r8vec_factor) {

    pmgmres_unroll_factor = pmgmres_factor;
    ax_unroll_factor = ax_factor;
    ilu_unroll_factor = ilu_factor;
    lus_unroll_factor = lus_factor;
    diag_unroll_factor = diag_factor;
    rearrange_unroll_factor = rearrange_factor;
    r8vec_unroll_factor = r8vec_factor;

}



double r8vec_dot_unroll_4(int n, double a[], double b[])
{
    int i;
    double value;

    value = 0.0;
    int32_t m = 0;

    double v0 = 0.0;
    double v1 = 0.0;
    double v2 = 0.0;
    double v3 = 0.0;

    for (i = 0; i < n/4; i++) {
        v0 += a[m] * b[m];
        v1 += a[m + 1] * b[m + 1];
        v2 += a[m + 2] * b[m + 2];
        v3 += a[m + 3] * b[m + 3];

        m += 4;
    }

    double a0, a1;
    a0 = v0 + v1;
    a1 = v2 + v3;
    value = a0 + a1;


    // Process remainder
    for (i = (n/4) * 4; i < n; i++) {
        value = value + a[i] * b[i];
    }

    return value;
}


double r8vec_dot_unroll_8(int n, double a[], double b[])
{
    int i;
    double value;

    value = 0.0;
    int32_t m = 0;

    double v0 = 0.0, v1 = 0.0, v2 = 0.0, v3 = 0.0;
    double v4 = 0.0, v5 = 0.0, v6 = 0.0, v7 = 0.0;

    for (i = 0; i < n/8; i++) {
        v0 += a[m] * b[m];
        v1 += a[m + 1] * b[m + 1];
        v2 += a[m + 2] * b[m + 2];
        v3 += a[m + 3] * b[m + 3];
        v4 += a[m + 4] * b[m + 4];
        v5 += a[m + 5] * b[m + 5];
        v6 += a[m + 6] * b[m + 6];
        v7 += a[m + 7] * b[m + 7];

        m += 8;
    }

    double a0, a1, a2, a3;
    a0 = v0 + v1;
    a1 = v2 + v3;
    a2 = v4 + v5;
    a3 = v6 + v7;

    double b0, b1;
    b0 = a0 + a1;
    b1 = a2 + a3;

    value = b0 + b1;

    // Process remainder
    for (i = (n/8) * 8; i < n; i++) {
        value = value + a[i] * b[i];
    }

    return value;
}


double r8vec_dot_unroll_16(int n, double a[], double b[])
{
    int i;
    double value;

    value = 0.0;
    int32_t m = 0;

    double v0 = 0.0, v1 = 0.0, v2 = 0.0, v3 = 0.0;
    double v4 = 0.0, v5 = 0.0, v6 = 0.0, v7 = 0.0;
    double v8 = 0.0, v9 = 0.0, v10 = 0.0, v11 = 0.0;
    double v12 = 0.0, v13 = 0.0, v14 = 0.0, v15 = 0.0;

    for (i = 0; i < n/16; i++) {
        v0 += a[m] * b[m];
        v1 += a[m + 1] * b[m + 1];
        v2 += a[m + 2] * b[m + 2];
        v3 += a[m + 3] * b[m + 3];
        v4 += a[m + 4] * b[m + 4];
        v5 += a[m + 5] * b[m + 5];
        v6 += a[m + 6] * b[m + 6];
        v7 += a[m + 7] * b[m + 7];

        v8 += a[m + 8] * b[m + 8];
        v9 += a[m + 9] * b[m + 9];
        v10 += a[m + 10] * b[m + 10];
        v11 += a[m + 11] * b[m + 11];
        v12 += a[m + 12] * b[m + 12];
        v13 += a[m + 13] * b[m + 13];
        v14 += a[m + 14] * b[m + 14];
        v15 += a[m + 15] * b[m + 15];

        m += 16;
    }

    double a0, a1, a2, a3, a4, a5, a6, a7;
    a0 = v0 + v1;
    a1 = v2 + v3;
    a2 = v4 + v5;
    a3 = v6 + v7;
    a4 = v8 + v9;
    a5 = v10 + v11;
    a6 = v12 + v13;
    a7 = v14 + v15;

    double b0, b1, b2, b3;
    b0 = a0 + a1;
    b1 = a2 + a3;
    b2 = a4 + a5;
    b3 = a6 + a7;

    double c0, c1;
    c0 = b0 + b1;
    c1 = b2 + b3;

    value = c0 + c1;

    // Process remainder
    for (i = (n/16) * 16; i < n; i++) {
        value = value + a[i] * b[i];
    }

    return value;
}


double r8vec_dot_unroll_32(int n, double a[], double b[])
{
    int i;
    double value;

    value = 0.0;
    int32_t m = 0;

    double v0 = 0.0, v1 = 0.0, v2 = 0.0, v3 = 0.0;
    double v4 = 0.0, v5 = 0.0, v6 = 0.0, v7 = 0.0;
    double v8 = 0.0, v9 = 0.0, v10 = 0.0, v11 = 0.0;
    double v12 = 0.0, v13 = 0.0, v14 = 0.0, v15 = 0.0;
    double v16 = 0.0, v17 = 0.0, v18 = 0.0, v19 = 0.0;
    double v20 = 0.0, v21 = 0.0, v22 = 0.0, v23 = 0.0;
    double v24 = 0.0, v25 = 0.0, v26 = 0.0, v27 = 0.0;
    double v28 = 0.0, v29 = 0.0, v30 = 0.0, v31 = 0.0;

    for (i = 0; i < n/32; i++) {
        v0 += a[m] * b[m];
        v1 += a[m + 1] * b[m + 1];
        v2 += a[m + 2] * b[m + 2];
        v3 += a[m + 3] * b[m + 3];
        v4 += a[m + 4] * b[m + 4];
        v5 += a[m + 5] * b[m + 5];
        v6 += a[m + 6] * b[m + 6];
        v7 += a[m + 7] * b[m + 7];

        v8 += a[m + 8] * b[m + 8];
        v9 += a[m + 9] * b[m + 9];
        v10 += a[m + 10] * b[m + 10];
        v11 += a[m + 11] * b[m + 11];
        v12 += a[m + 12] * b[m + 12];
        v13 += a[m + 13] * b[m + 13];
        v14 += a[m + 14] * b[m + 14];
        v15 += a[m + 15] * b[m + 15];

        v16 += a[m + 16] * b[m + 16];
        v17 += a[m + 17] * b[m + 17];
        v18 += a[m + 18] * b[m + 18];
        v19 += a[m + 19] * b[m + 19];
        v20 += a[m + 20] * b[m + 20];
        v21 += a[m + 21] * b[m + 21];
        v22 += a[m + 22] * b[m + 22];
        v23 += a[m + 23] * b[m + 23];

        v24 += a[m + 24] * b[m + 24];
        v25 += a[m + 25] * b[m + 25];
        v26 += a[m + 26] * b[m + 26];
        v27 += a[m + 27] * b[m + 27];
        v28 += a[m + 28] * b[m + 28];
        v29 += a[m + 29] * b[m + 29];
        v30 += a[m + 30] * b[m + 30];
        v31 += a[m + 31] * b[m + 31];

        m += 32;
    }

    double a0, a1, a2, a3, a4, a5, a6, a7;
    double a8, a9, a10, a11, a12, a13, a14, a15;
    a0 = v0 + v1;
    a1 = v2 + v3;
    a2 = v4 + v5;
    a3 = v6 + v7;
    a4 = v8 + v9;
    a5 = v10 + v11;
    a6 = v12 + v13;
    a7 = v14 + v15;

    a8 = v16 + v17;
    a9 = v18 + v19;
    a10 = v20 + v21;
    a11 = v22 + v23;
    a12 = v24 + v25;
    a13 = v26 + v27;
    a14 = v28 + v29;
    a15 = v30 + v31;

    double b0, b1, b2, b3;
    double b4, b5, b6, b7;
    b0 = a0 + a1;
    b1 = a2 + a3;
    b2 = a4 + a5;
    b3 = a6 + a7;

    b4 = a8 + a9;
    b5 = a10 + a11;
    b6 = a12 + a13;
    b7 = a14 + a15;

    double c0, c1, c2, c3;
    c0 = b0 + b1;
    c1 = b2 + b3;
    c2 = b4 + b5;
    c3 = b6 + b7;

    double d0, d1;
    d0 = c0 + c1;
    d1 = c2 + c3;

    value = d0 + d1 ;

    // Process remainder
    for (i = (n/32) * 32; i < n; i++) {
        value = value + a[i] * b[i];
    }

    return value;
}


double r8vec_dot_unroll(int n, double a[], double b[])
/******************************************************************************/
/*
  Purpose:

    R8VEC_DOT computes the dot product of a pair of R8VEC's.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    26 July 2007

  Author:

    John Burkardt

  Parameters:

    Input, int N, the number of entries in the vectors.

    Input, double A1[N], A2[N], the two vectors to be considered.

    Output, double R8VEC_DOT, the dot product of the vectors.
*/
{
    switch (r8vec_unroll_factor) {
        case 1:
            return r8vec_dot(n, a, b);
            break;
        case 4:
            return r8vec_dot_unroll_4(n, a, b);
            break;
        case 8:
            return r8vec_dot_unroll_8(n, a, b);
            break;
        case 16:
            return r8vec_dot_unroll_16(n, a, b);
            break;
        case 32:
            return r8vec_dot_unroll_32(n, a, b);
            break;
        default:
            return r8vec_dot(n, a, b);
            break;
    }
}



// CSR-VI TRANSFORMATION REQUIRED
/*
 * [FastCode] GMRES with Incomplete LU Preconditioner operating on CSR-VI matrix format
 */
/*
  Parameters:

    Input, int32_t N, the order of the linear system.

    Input, int32_t NZ_NUM, the number of nonzero matrix values.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double A[NZ_NUM], the matrix values.

    Input/output, double X[N]; on input, an approximation to
    the solution.  On output, an improved approximation.

    Input, double RHS[N], the right hand side of the linear system.

    Input, int32_t ITR_MAX, the maximum number of (outer) iterations to take.

    Input, int32_t MR, the maximum number of (inner) iterations to take.
    MR must be less than N.

    Input, double TOL_ABS, an absolute tolerance applied to the
    current residual.

    Input, double TOL_REL, a relative tolerance comparing the
    current residual to the initial residual.
*/
void pmgmres_ilu_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala,
                                  double *x, double *rhs, int32_t itr_max, int32_t mr, double tol_abs,
                                  double tol_rel) {


    switch (pmgmres_unroll_factor) {
        case 1:
            pmgmres_ilu_csrvi_ilp(n, nz_num, ia, ja, va, vala, x, rhs, itr_max, mr, tol_abs, tol_rel);
            break;
        case 4:
            pmgmres_ilu_csrvi_ilp_unroll_4(n, nz_num, ia, ja, va, vala, x, rhs, itr_max, mr, tol_abs, tol_rel);
            break;
        case 8:
            pmgmres_ilu_csrvi_ilp_unroll_8(n, nz_num, ia, ja, va, vala, x, rhs, itr_max, mr, tol_abs, tol_rel);
            break;
        case 16:
            pmgmres_ilu_csrvi_ilp_unroll_16(n, nz_num, ia, ja, va, vala, x, rhs, itr_max, mr, tol_abs, tol_rel);
            break;
        case 32:
            pmgmres_ilu_csrvi_ilp_unroll_32(n, nz_num, ia, ja, va, vala, x, rhs, itr_max, mr, tol_abs, tol_rel);
            break;
        default:
            pmgmres_ilu_csrvi_ilp(n, nz_num, ia, ja, va, vala, x, rhs, itr_max, mr, tol_abs, tol_rel);
            break;
    }


}

/*
 * [FastCode] PGMRES without ILP optimizations (but possibly uses ILP optimized auxiliary functions)
 *
 * DON'T REMOVE!
 */
void pmgmres_ilu_csrvi_ilp(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                           double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                           double tol_rel) {
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;

    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));

    rearrange_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala);

    diagonal_pointer_csrvi_ilp_unroll(n, nz_num, ia, ja, ua);

    ilu_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP\n");
        printf("\tNumber of unknowns = %d\n", n);
        printf("\tAX_LOOP_UNROLL_FACTOR = %d\n", ax_unroll_factor);
        printf("\tILU_LOOP_UNROLL_FACTOR = %d\n", ilu_unroll_factor);
        printf("\tLUS_LOOP_UNROLL_FACTOR = %d\n", lus_unroll_factor);
        printf("\tREARRANGE_LOOP_UNROLL_FACTOR = %d\n", rearrange_unroll_factor);
        printf("\tDIAG_LOOP_UNROLL_FACTOR = %d\n", diag_unroll_factor);
        printf("\n\n");
    }

    for (itr = 0; itr < itr_max; itr++) {

        ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, x, r);

        for (i = 0; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_unroll(n, r, r));

        if (verbose) {
            printf("\tITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
        }

        for (i = 0; i < n; i++) {
            v[0][i] = r[i] / rho;
        }

        g[0] = rho;
        for (i = 1; i < mr + 1; i++) {
            g[i] = 0.0;
        }

        for (i = 0; i < mr + 1; i++) {
            for (j = 0; j < mr; j++) {
                h[i][j] = 0.0;
            }
        }

        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);

            lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_unroll(n, v[k + 1], v[j]);
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }
            }
            h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {

                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_unroll(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;

                    for (i = 0; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }
                }
                h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }
            }

            if (0 < k) {
                for (i = 0; i < k + 2; i++) {
                    y[i] = h[i][k];
                }
                for (j = 0; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }
                for (i = 0; i < k + 2; i++) {
                    h[i][k] = y[i];
                }
            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];

            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("\tK   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];
        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];
            for (j = i + 1; j < k + 1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
            }
            y[i] = y[i] / h[i][i];
        }
        for (i = 0; i < n; i++) {
            for (j = 0; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
            }
        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP:\n");
        printf("\tIterations = %d\n", itr_used);
        printf("\tFinal residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}

void pmgmres_ilu_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                                    double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                                    double tol_rel) {
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;

    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));


    rearrange_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala);

    diagonal_pointer_csrvi_ilp_unroll(n, nz_num, ia, ja, ua);

    ilu_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP\n");
        printf("\tNumber of unknowns = %d\n", n);
        printf("\tAX_LOOP_UNROLL_FACTOR = %d\n", ax_unroll_factor);
        printf("\tILU_LOOP_UNROLL_FACTOR = %d\n", ilu_unroll_factor);
        printf("\tLUS_LOOP_UNROLL_FACTOR = %d\n", lus_unroll_factor);
        printf("\tREARRANGE_LOOP_UNROLL_FACTOR = %d\n", rearrange_unroll_factor);
        printf("\tDIAG_LOOP_UNROLL_FACTOR = %d\n", diag_unroll_factor);
        printf("\n\n");
    }

    for (itr = 0; itr < itr_max; itr++) {

        ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, x, r);

        // ____________________________________________________________
        // ____________________________________________________________

        /* [OLD] [START]
        for (i = 0; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }
          [OLD] [END] */

/*
        // First optimization
        for (i = 0; i < n/4; i++) {
            r[4 * i] = rhs[4 * i] - r[4 * i];
            r[4 * i + 1] = rhs[4 * i + 1] - r[4 * i + 1];
            r[4 * i + 2] = rhs[4 * i + 2] - r[4 * i + 2];
            r[4 * i + 3] = rhs[4 * i + 3] - r[4 * i + 3];
        }

        // Process remainder
        for (i = (n/4) * 4; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }*/

        // Second optimization
        int32_t m = 0;
        for (i = 0; i < n/4; i++) {
            r[m] = rhs[m] - r[m];
            r[m + 1] = rhs[m + 1] - r[m + 1];
            r[m + 2] = rhs[m + 2] - r[m + 2];
            r[m + 3] = rhs[m + 3] - r[m + 3];

            m += 4;
        }

        // Process remainder
        for (i = (n/4) * 4; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }


        // ____________________________________________________________
        // ____________________________________________________________
        lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_unroll(n, r, r));

        if (verbose) {
            printf("\tITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
        }

        // ____________________________________________________________
        // ____________________________________________________________

        /* [OLD] [START]
        for (i = 0; i < n; i++) {
            v[0][i] = r[i] / rho;
        }
          [OLD] [END] */

        // Use last optimization technique
        m = 0;
        for (i = 0; i < n/4; i++) {
            v[0][m] = r[m] / rho;
            v[0][m + 1] = r[m + 1] / rho;
            v[0][m + 2] = r[m + 2] / rho;
            v[0][m + 3] = r[m + 3] / rho;

            m += 4;
        }

        // Process remainder
        for (i = (n/4) * 4; i < n; i++) {
            v[0][i] = r[i] / rho;
        }


        // ____________________________________________________________
        // ____________________________________________________________


        g[0] = rho;
        /* [OLD] [START]
        for (i = 1; i < mr + 1; i++) {
            g[i] = 0.0;
        }
           [OLD] [END] */

/*
        // First modification
        for (i = 0; i < mr; i++) {
            g[i + 1] = 0.0;
        }*/

        m = 0;
        for (i = 0; i < mr/4; i++) {
            g[m + 1] = 0.0;
            g[m + 2] = 0.0;
            g[m + 3] = 0.0;
            g[m + 4] = 0.0;

            m += 4;
        }

        // Process remainder
        for (i = (mr/4) * 4; i < mr; i++) {
            g[i + 1] = 0.0;
        }



        // ____________________________________________________________
        // ____________________________________________________________


        for (i = 0; i < mr + 1; i++) {

            /* [OLD] [START]
            for (j = 0; j < mr; j++) {
                h[i][j] = 0.0;
            }
             [OLD] [END] */

            // Use last optimization technique
            m = 0;
            for (j = 0; j < mr/4; j++) {
                h[i][m] = 0.0;
                h[i][m + 1] = 0.0;
                h[i][m + 2] = 0.0;
                h[i][m + 3] = 0.0;

                m += 4;
            }

            // Process remainder
            for (j = (mr/4) * 4; j < mr; j++) {
                h[i][j] = 0.0;
            }

        }

        // ____________________________________________________________
        // ____________________________________________________________
        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);

            lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_unroll(n, v[k + 1], v[j]);
                // ____________________________________________________________
                // ____________________________________________________________


                /* [OLD] [START]
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }
                    [OLD] [END] */

                // Use last optimization technique
                m = 0;
                for (i = 0; i < n/4; i++) {
                    v[k + 1][m] = v[k + 1][m] - h[j][k] * v[j][m];
                    v[k + 1][m + 1] = v[k + 1][m + 1] - h[j][k] * v[j][m + 1];
                    v[k + 1][m + 2] = v[k + 1][m + 2] - h[j][k] * v[j][m + 2];
                    v[k + 1][m + 3] = v[k + 1][m + 3] - h[j][k] * v[j][m + 3];

                    m += 4;
                }

                // Process remainder
                for (i = (n/4) * 4; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }

                // ____________________________________________________________
                // ____________________________________________________________
            }
            h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {

                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_unroll(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;

                    // ____________________________________________________________
                    // ____________________________________________________________


                    /* [OLD] [START]
                    for (i = 0; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }
                        [OLD] [END] */

                    // Use last optimization technique
                    m = 0;
                    for (i = 0; i < n/4; i++) {
                        v[k + 1][m] = v[k + 1][m] - htmp * v[j][m];
                        v[k + 1][m + 1] = v[k + 1][m + 1] - htmp * v[j][m + 1];
                        v[k + 1][m + 2] = v[k + 1][m + 2] - htmp * v[j][m + 2];
                        v[k + 1][m + 3] = v[k + 1][m + 3] - htmp * v[j][m + 3];

                        m += 4;
                    }

                    // Process remainder
                    for (i = (n/4) * 4; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }


                    // ____________________________________________________________
                    // ____________________________________________________________
                }
                h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {

                /* [OLD] [START]
                for (i = 0; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }
                    [OLD] [END] */

                // Use last optimization technique
                m = 0;
                for (i = 0; i < n/4; i++) {
                    v[k + 1][m] = v[k + 1][m] / h[k + 1][k];
                    v[k + 1][m + 1] = v[k + 1][m + 1] / h[k + 1][k];
                    v[k + 1][m + 2] = v[k + 1][m + 2] / h[k + 1][k];
                    v[k + 1][m + 3] = v[k + 1][m + 3] / h[k + 1][k];

                    m += 4;
                }

                // Process remainder
                for (i = (n/4) * 4; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }

            }

            if (0 < k) {
                // ____________________________________________________________
                // ____________________________________________________________

                /* [OLD] [START]
                for (i = 0; i < k + 2; i++) {
                    y[i] = h[i][k];
                }
                   [OLD] [END] */

                // Use last optimization technique
                m = 0;
                for (i = 0; i < (k + 2)/4; i++) {
                    y[m] = h[m][k];
                    y[m + 1] = h[m + 1][k];
                    y[m + 2] = h[m + 2][k];
                    y[m + 3] = h[m + 3][k];

                    m += 4;
                }

                // Process remainder
                for (i = ((k + 2)/4) * 4; i < k + 2; i++) {
                    y[i] = h[i][k];
                }


                // ____________________________________________________________
                // ____________________________________________________________

                /* [OLD] [START]
                for (j = 0; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }
                   [OLD] [END] */

                // Use last optimization technique
                m = 0;
                for (j = 0; j < k/4; j++) {
                    mult_givens(c[m], s[m], m, y);
                    mult_givens(c[m + 1], s[m + 1], m + 1, y);
                    mult_givens(c[m + 2], s[m + 2], m + 2, y);
                    mult_givens(c[m + 3], s[m + 3], m + 3, y);

                    m += 4;
                }

                // Process remainder
                for (j = (k/4) * 4; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }


                // ____________________________________________________________
                // ____________________________________________________________


                /* [OLD] [START]
                for (i = 0; i < k + 2; i++) {
                    h[i][k] = y[i];
                }
                   [OLD] [END] */

                // Use last optimization technique
                m = 0;
                for (i = 0; i < (k + 2)/4; i++) {
                    h[m][k] = y[m];
                    h[m + 1][k] = y[m + 1];
                    h[m + 2][k] = y[m + 2];
                    h[m + 3][k] = y[m + 3];

                    m += 4;
                }

                // Process remainder
                for (i = ((k + 2)/4) * 4; i < k + 2; i++) {
                    h[i][k] = y[i];
                }

                // ____________________________________________________________
                // ____________________________________________________________
            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];

            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("\tK   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];


        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];


            /* [OLD] [BEGIN]
            for (j = i + 1; j < k + 1; j++) {
                y[i] = y[i] - h[i][j] * y[j];
            }
              [OLD] [END] */

/*
            // First modification
            int32_t K = k - i;
            for (j = 0; j < K; j++) {
                y[i] = y[i] - h[i][i + 1 + j] * y[i + 1 + j];
            }*/
/*
            // First optimization
            int32_t K = k - i;
            m = 0;
            for (j = 0; j < K/4; j++) {
                y[i] = y[i] - h[i][i + 1 + m] * y[i + 1 + m];
                y[i] = y[i] - h[i][i + 1 + m + 1] * y[i + 1 + m + 1];
                y[i] = y[i] - h[i][i + 1 + m + 2] * y[i + 1 + m + 2];
                y[i] = y[i] - h[i][i + 1 + m + 3] * y[i + 1 + m + 3];

                m += 4;
            }

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                y[i] = y[i] - h[i][i + 1 + j] * y[i + 1 + j];
            }*/

            // Second optimization (Scalar replacement and evaluating sums)
            int32_t K = k - i;
            m = 0;
            double y_tmp = y[i];
            for (j = 0; j < K/4; j++) {

                y_tmp -= h[i][i + m + 1] * y[i + m + 1];
                y_tmp -= h[i][i + m + 2] * y[i + m + 2];
                y_tmp -= h[i][i + m + 3] * y[i + m + 3];
                y_tmp -= h[i][i + m + 4] * y[i + m + 4];

                m += 4;
            }

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                y_tmp -= h[i][i + 1 + j] * y[i + 1 + j];
            }
/*
            // Store value
            y[i] = y_tmp;

            y[i] = y[i] / h[i][i]; */

            // Contract the two above lines
            y[i] = y_tmp / h[i][i];
        }
        for (i = 0; i < n; i++) {

            /* [OLD] [START]
            for (j = 0; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
            }
              [OLD] [END] */

/*
            // First optimization
            m = 0;
            for (j = 0; j < (k + 1)/4; j++) {
                x[i] = x[i] + v[m][i] * y[m];
                x[i] = x[i] + v[m + 1][i] * y[m + 1];
                x[i] = x[i] + v[m + 2][i] * y[m + 2];
                x[i] = x[i] + v[m + 3][i] * y[m + 3];

                m += 4;
            }

            // Process remainder
            for (j = ((k + 1)/4) * 4; j < k + 1; j++) {
                x[i] = x[i] + v[j][i] * y[j];
            }*/

            // TODO: Maybe we find a better access pattern for this
            // Second optimization (Scalar replacement)
            m = 0;
            double x_tmp = x[i];
            for (j = 0; j < (k + 1)/4; j++) {
                x_tmp += v[m][i] * y[m];
                x_tmp += v[m + 1][i] * y[m + 1];
                x_tmp += v[m + 2][i] * y[m + 2];
                x_tmp += v[m + 3][i] * y[m + 3];

                m += 4;
            }

            // Process remainder
            for (j = ((k + 1)/4) * 4; j < k + 1; j++) {
                x_tmp += v[j][i] * y[j];
            }

            // Store value
            x[i] = x_tmp;

        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP:\n");
        printf("\tIterations = %d\n", itr_used);
        printf("\tFinal residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}


void pmgmres_ilu_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                                    double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                                    double tol_rel) {
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;

    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));


    rearrange_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala);

    diagonal_pointer_csrvi_ilp_unroll(n, nz_num, ia, ja, ua);

    ilu_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP\n");
        printf("\tNumber of unknowns = %d\n", n);
        printf("\tAX_LOOP_UNROLL_FACTOR = %d\n", ax_unroll_factor);
        printf("\tILU_LOOP_UNROLL_FACTOR = %d\n", ilu_unroll_factor);
        printf("\tLUS_LOOP_UNROLL_FACTOR = %d\n", lus_unroll_factor);
        printf("\tREARRANGE_LOOP_UNROLL_FACTOR = %d\n", rearrange_unroll_factor);
        printf("\tDIAG_LOOP_UNROLL_FACTOR = %d\n", diag_unroll_factor);
        printf("\n\n");
    }

    for (itr = 0; itr < itr_max; itr++) {

        ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, x, r);

        int32_t m = 0;
        for (i = 0; i < n/8; i++) {
            r[m] = rhs[m] - r[m];
            r[m + 1] = rhs[m + 1] - r[m + 1];
            r[m + 2] = rhs[m + 2] - r[m + 2];
            r[m + 3] = rhs[m + 3] - r[m + 3];
            r[m + 4] = rhs[m + 4] - r[m + 4];
            r[m + 5] = rhs[m + 5] - r[m + 5];
            r[m + 6] = rhs[m + 6] - r[m + 6];
            r[m + 7] = rhs[m + 7] - r[m + 7];

            m += 8;
        }

        // Process remainder
        for (i = (n/8) * 8; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_unroll(n, r, r));

        if (verbose) {
            printf("\tITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
        }

        m = 0;
        for (i = 0; i < n/8; i++) {
            v[0][m] = r[m] / rho;
            v[0][m + 1] = r[m + 1] / rho;
            v[0][m + 2] = r[m + 2] / rho;
            v[0][m + 3] = r[m + 3] / rho;
            v[0][m + 4] = r[m + 4] / rho;
            v[0][m + 5] = r[m + 5] / rho;
            v[0][m + 6] = r[m + 6] / rho;
            v[0][m + 7] = r[m + 7] / rho;

            m += 8;
        }

        // Process remainder
        for (i = (n/8) * 8; i < n; i++) {
            v[0][i] = r[i] / rho;
        }


        // ____________________________________________________________
        // ____________________________________________________________


        g[0] = rho;

        m = 0;
        for (i = 0; i < mr/8; i++) {
            g[m + 1] = 0.0;
            g[m + 2] = 0.0;
            g[m + 3] = 0.0;
            g[m + 4] = 0.0;
            g[m + 5] = 0.0;
            g[m + 6] = 0.0;
            g[m + 7] = 0.0;
            g[m + 8] = 0.0;

            m += 8;
        }

        // Process remainder
        for (i = (mr/8) * 8; i < mr; i++) {
            g[i + 1] = 0.0;
        }



        // ____________________________________________________________
        // ____________________________________________________________


        for (i = 0; i < mr + 1; i++) {

            m = 0;
            for (j = 0; j < mr/8; j++) {
                h[i][m] = 0.0;
                h[i][m + 1] = 0.0;
                h[i][m + 2] = 0.0;
                h[i][m + 3] = 0.0;
                h[i][m + 4] = 0.0;
                h[i][m + 5] = 0.0;
                h[i][m + 6] = 0.0;
                h[i][m + 7] = 0.0;

                m += 8;
            }

            // Process remainder
            for (j = (mr/8) * 8; j < mr; j++) {
                h[i][j] = 0.0;
            }

        }

        // ____________________________________________________________
        // ____________________________________________________________
        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);

            lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_unroll(n, v[k + 1], v[j]);

                m = 0;
                for (i = 0; i < n/8; i++) {
                    v[k + 1][m] = v[k + 1][m] - h[j][k] * v[j][m];
                    v[k + 1][m + 1] = v[k + 1][m + 1] - h[j][k] * v[j][m + 1];
                    v[k + 1][m + 2] = v[k + 1][m + 2] - h[j][k] * v[j][m + 2];
                    v[k + 1][m + 3] = v[k + 1][m + 3] - h[j][k] * v[j][m + 3];
                    v[k + 1][m + 4] = v[k + 1][m + 4] - h[j][k] * v[j][m + 4];
                    v[k + 1][m + 5] = v[k + 1][m + 5] - h[j][k] * v[j][m + 5];
                    v[k + 1][m + 6] = v[k + 1][m + 6] - h[j][k] * v[j][m + 6];
                    v[k + 1][m + 7] = v[k + 1][m + 7] - h[j][k] * v[j][m + 7];

                    m += 8;
                }

                // Process remainder
                for (i = (n/8) * 8; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }

                // ____________________________________________________________
                // ____________________________________________________________
            }
            h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {

                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_unroll(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;

                    m = 0;
                    for (i = 0; i < n/8; i++) {
                        v[k + 1][m] = v[k + 1][m] - htmp * v[j][m];
                        v[k + 1][m + 1] = v[k + 1][m + 1] - htmp * v[j][m + 1];
                        v[k + 1][m + 2] = v[k + 1][m + 2] - htmp * v[j][m + 2];
                        v[k + 1][m + 3] = v[k + 1][m + 3] - htmp * v[j][m + 3];
                        v[k + 1][m + 4] = v[k + 1][m + 4] - htmp * v[j][m + 4];
                        v[k + 1][m + 5] = v[k + 1][m + 5] - htmp * v[j][m + 5];
                        v[k + 1][m + 6] = v[k + 1][m + 6] - htmp * v[j][m + 6];
                        v[k + 1][m + 7] = v[k + 1][m + 7] - htmp * v[j][m + 7];

                        m += 8;
                    }

                    // Process remainder
                    for (i = (n/8) * 8; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }

                }
                h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {

                m = 0;
                for (i = 0; i < n/8; i++) {
                    v[k + 1][m] = v[k + 1][m] / h[k + 1][k];
                    v[k + 1][m + 1] = v[k + 1][m + 1] / h[k + 1][k];
                    v[k + 1][m + 2] = v[k + 1][m + 2] / h[k + 1][k];
                    v[k + 1][m + 3] = v[k + 1][m + 3] / h[k + 1][k];
                    v[k + 1][m + 4] = v[k + 1][m + 4] / h[k + 1][k];
                    v[k + 1][m + 5] = v[k + 1][m + 5] / h[k + 1][k];
                    v[k + 1][m + 6] = v[k + 1][m + 6] / h[k + 1][k];
                    v[k + 1][m + 7] = v[k + 1][m + 7] / h[k + 1][k];

                    m += 8;
                }

                // Process remainder
                for (i = (n/8) * 8; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }

            }

            if (0 < k) {

                m = 0;
                for (i = 0; i < (k + 2)/8; i++) {
                    y[m] = h[m][k];
                    y[m + 1] = h[m + 1][k];
                    y[m + 2] = h[m + 2][k];
                    y[m + 3] = h[m + 3][k];
                    y[m + 4] = h[m + 4][k];
                    y[m + 5] = h[m + 5][k];
                    y[m + 6] = h[m + 6][k];
                    y[m + 7] = h[m + 7][k];

                    m += 8;
                }

                // Process remainder
                for (i = ((k + 2)/8) * 8; i < k + 2; i++) {
                    y[i] = h[i][k];
                }

                m = 0;
                for (j = 0; j < k/8; j++) {
                    mult_givens(c[m], s[m], m, y);
                    mult_givens(c[m + 1], s[m + 1], m + 1, y);
                    mult_givens(c[m + 2], s[m + 2], m + 2, y);
                    mult_givens(c[m + 3], s[m + 3], m + 3, y);
                    mult_givens(c[m + 4], s[m + 4], m + 4, y);
                    mult_givens(c[m + 5], s[m + 5], m + 5, y);
                    mult_givens(c[m + 6], s[m + 6], m + 6, y);
                    mult_givens(c[m + 7], s[m + 7], m + 7, y);

                    m += 8;
                }

                // Process remainder
                for (j = (k/8) * 8; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }

                m = 0;
                for (i = 0; i < (k + 2)/8; i++) {
                    h[m][k] = y[m];
                    h[m + 1][k] = y[m + 1];
                    h[m + 2][k] = y[m + 2];
                    h[m + 3][k] = y[m + 3];
                    h[m + 4][k] = y[m + 4];
                    h[m + 5][k] = y[m + 5];
                    h[m + 6][k] = y[m + 6];
                    h[m + 7][k] = y[m + 7];

                    m += 8;
                }

                // Process remainder
                for (i = ((k + 2)/8) * 8; i < k + 2; i++) {
                    h[i][k] = y[i];
                }

            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];

            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("\tK   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];


        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];

            int32_t K = k - i;
            m = 0;
            double y_tmp = y[i];
            for (j = 0; j < K/8; j++) {

                y_tmp -= h[i][i + m + 1] * y[i + m + 1];
                y_tmp -= h[i][i + m + 2] * y[i + m + 2];
                y_tmp -= h[i][i + m + 3] * y[i + m + 3];
                y_tmp -= h[i][i + m + 4] * y[i + m + 4];
                y_tmp -= h[i][i + m + 5] * y[i + m + 5];
                y_tmp -= h[i][i + m + 6] * y[i + m + 6];
                y_tmp -= h[i][i + m + 7] * y[i + m + 7];
                y_tmp -= h[i][i + m + 8] * y[i + m + 8];

                m += 8;
            }

            // Process remainder
            for (j = (K/8) * 8; j < K; j++) {
                y_tmp -= h[i][i + 1 + j] * y[i + 1 + j];
            }

            y[i] = y_tmp / h[i][i];
        }
        for (i = 0; i < n; i++) {

            m = 0;
            double x_tmp = x[i];
            for (j = 0; j < (k + 1)/8; j++) {
                x_tmp += v[m][i] * y[m];
                x_tmp += v[m + 1][i] * y[m + 1];
                x_tmp += v[m + 2][i] * y[m + 2];
                x_tmp += v[m + 3][i] * y[m + 3];
                x_tmp += v[m + 4][i] * y[m + 4];
                x_tmp += v[m + 5][i] * y[m + 5];
                x_tmp += v[m + 6][i] * y[m + 6];
                x_tmp += v[m + 7][i] * y[m + 7];

                m += 8;
            }

            // Process remainder
            for (j = ((k + 1)/8) * 8; j < k + 1; j++) {
                x_tmp += v[j][i] * y[j];
            }

            // Store value
            x[i] = x_tmp;

        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP:\n");
        printf("\tIterations = %d\n", itr_used);
        printf("\tFinal residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}


void pmgmres_ilu_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                                    double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                                    double tol_rel) {
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;

    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));


    rearrange_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala);

    diagonal_pointer_csrvi_ilp_unroll(n, nz_num, ia, ja, ua);

    ilu_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP\n");
        printf("\tNumber of unknowns = %d\n", n);
        printf("\tAX_LOOP_UNROLL_FACTOR = %d\n", ax_unroll_factor);
        printf("\tILU_LOOP_UNROLL_FACTOR = %d\n", ilu_unroll_factor);
        printf("\tLUS_LOOP_UNROLL_FACTOR = %d\n", lus_unroll_factor);
        printf("\tREARRANGE_LOOP_UNROLL_FACTOR = %d\n", rearrange_unroll_factor);
        printf("\tDIAG_LOOP_UNROLL_FACTOR = %d\n", diag_unroll_factor);
        printf("\n\n");
    }

    for (itr = 0; itr < itr_max; itr++) {

        ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, x, r);

        int32_t m = 0;
        for (i = 0; i < n/16; i++) {
            r[m] = rhs[m] - r[m];
            r[m + 1] = rhs[m + 1] - r[m + 1];
            r[m + 2] = rhs[m + 2] - r[m + 2];
            r[m + 3] = rhs[m + 3] - r[m + 3];
            r[m + 4] = rhs[m + 4] - r[m + 4];
            r[m + 5] = rhs[m + 5] - r[m + 5];
            r[m + 6] = rhs[m + 6] - r[m + 6];
            r[m + 7] = rhs[m + 7] - r[m + 7];
            r[m + 8] = rhs[m + 8] - r[m + 8];
            r[m + 9] = rhs[m + 9] - r[m + 9];
            r[m + 10] = rhs[m + 10] - r[m + 10];
            r[m + 11] = rhs[m + 11] - r[m + 11];
            r[m + 12] = rhs[m + 12] - r[m + 12];
            r[m + 13] = rhs[m + 13] - r[m + 13];
            r[m + 14] = rhs[m + 14] - r[m + 14];
            r[m + 15] = rhs[m + 15] - r[m + 15];

            m += 16;
        }

        // Process remainder
        for (i = (n/16) * 16; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_unroll(n, r, r));

        if (verbose) {
            printf("\tITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
        }

        m = 0;
        for (i = 0; i < n/16; i++) {
            v[0][m] = r[m] / rho;
            v[0][m + 1] = r[m + 1] / rho;
            v[0][m + 2] = r[m + 2] / rho;
            v[0][m + 3] = r[m + 3] / rho;
            v[0][m + 4] = r[m + 4] / rho;
            v[0][m + 5] = r[m + 5] / rho;
            v[0][m + 6] = r[m + 6] / rho;
            v[0][m + 7] = r[m + 7] / rho;
            v[0][m + 8] = r[m + 8] / rho;
            v[0][m + 9] = r[m + 9] / rho;
            v[0][m + 10] = r[m + 10] / rho;
            v[0][m + 11] = r[m + 11] / rho;
            v[0][m + 12] = r[m + 12] / rho;
            v[0][m + 13] = r[m + 13] / rho;
            v[0][m + 14] = r[m + 14] / rho;
            v[0][m + 15] = r[m + 15] / rho;

            m += 16;
        }

        // Process remainder
        for (i = (n/16) * 16; i < n; i++) {
            v[0][i] = r[i] / rho;
        }


        // ____________________________________________________________
        // ____________________________________________________________


        g[0] = rho;

        m = 0;
        for (i = 0; i < mr/16; i++) {
            g[m + 1] = 0.0;
            g[m + 2] = 0.0;
            g[m + 3] = 0.0;
            g[m + 4] = 0.0;
            g[m + 5] = 0.0;
            g[m + 6] = 0.0;
            g[m + 7] = 0.0;
            g[m + 8] = 0.0;
            g[m + 9] = 0.0;
            g[m + 10] = 0.0;
            g[m + 11] = 0.0;
            g[m + 12] = 0.0;
            g[m + 13] = 0.0;
            g[m + 14] = 0.0;
            g[m + 15] = 0.0;
            g[m + 16] = 0.0;

            m += 16;
        }

        // Process remainder
        for (i = (mr/16) * 16; i < mr; i++) {
            g[i + 1] = 0.0;
        }



        // ____________________________________________________________
        // ____________________________________________________________


        for (i = 0; i < mr + 1; i++) {

            m = 0;
            for (j = 0; j < mr/16; j++) {
                h[i][m] = 0.0;
                h[i][m + 1] = 0.0;
                h[i][m + 2] = 0.0;
                h[i][m + 3] = 0.0;
                h[i][m + 4] = 0.0;
                h[i][m + 5] = 0.0;
                h[i][m + 6] = 0.0;
                h[i][m + 7] = 0.0;
                h[i][m + 8] = 0.0;
                h[i][m + 9] = 0.0;
                h[i][m + 10] = 0.0;
                h[i][m + 11] = 0.0;
                h[i][m + 12] = 0.0;
                h[i][m + 13] = 0.0;
                h[i][m + 14] = 0.0;
                h[i][m + 15] = 0.0;

                m += 16;
            }

            // Process remainder
            for (j = (mr/16) * 16; j < mr; j++) {
                h[i][j] = 0.0;
            }

        }

        // ____________________________________________________________
        // ____________________________________________________________
        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);

            lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_unroll(n, v[k + 1], v[j]);

                m = 0;
                for (i = 0; i < n/16; i++) {
                    v[k + 1][m] = v[k + 1][m] - h[j][k] * v[j][m];
                    v[k + 1][m + 1] = v[k + 1][m + 1] - h[j][k] * v[j][m + 1];
                    v[k + 1][m + 2] = v[k + 1][m + 2] - h[j][k] * v[j][m + 2];
                    v[k + 1][m + 3] = v[k + 1][m + 3] - h[j][k] * v[j][m + 3];
                    v[k + 1][m + 4] = v[k + 1][m + 4] - h[j][k] * v[j][m + 4];
                    v[k + 1][m + 5] = v[k + 1][m + 5] - h[j][k] * v[j][m + 5];
                    v[k + 1][m + 6] = v[k + 1][m + 6] - h[j][k] * v[j][m + 6];
                    v[k + 1][m + 7] = v[k + 1][m + 7] - h[j][k] * v[j][m + 7];
                    v[k + 1][m + 8] = v[k + 1][m + 8] - h[j][k] * v[j][m + 8];
                    v[k + 1][m + 9] = v[k + 1][m + 9] - h[j][k] * v[j][m + 9];
                    v[k + 1][m + 10] = v[k + 1][m + 10] - h[j][k] * v[j][m + 10];
                    v[k + 1][m + 11] = v[k + 1][m + 11] - h[j][k] * v[j][m + 11];
                    v[k + 1][m + 12] = v[k + 1][m + 12] - h[j][k] * v[j][m + 12];
                    v[k + 1][m + 13] = v[k + 1][m + 13] - h[j][k] * v[j][m + 13];
                    v[k + 1][m + 14] = v[k + 1][m + 14] - h[j][k] * v[j][m + 14];
                    v[k + 1][m + 15] = v[k + 1][m + 15] - h[j][k] * v[j][m + 15];

                    m += 16;
                }

                // Process remainder
                for (i = (n/16) * 16; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }

                // ____________________________________________________________
                // ____________________________________________________________
            }
            h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {

                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_unroll(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;

                    m = 0;
                    for (i = 0; i < n/16; i++) {
                        v[k + 1][m] = v[k + 1][m] - htmp * v[j][m];
                        v[k + 1][m + 1] = v[k + 1][m + 1] - htmp * v[j][m + 1];
                        v[k + 1][m + 2] = v[k + 1][m + 2] - htmp * v[j][m + 2];
                        v[k + 1][m + 3] = v[k + 1][m + 3] - htmp * v[j][m + 3];
                        v[k + 1][m + 4] = v[k + 1][m + 4] - htmp * v[j][m + 4];
                        v[k + 1][m + 5] = v[k + 1][m + 5] - htmp * v[j][m + 5];
                        v[k + 1][m + 6] = v[k + 1][m + 6] - htmp * v[j][m + 6];
                        v[k + 1][m + 7] = v[k + 1][m + 7] - htmp * v[j][m + 7];
                        v[k + 1][m + 8] = v[k + 1][m + 8] - htmp * v[j][m + 8];
                        v[k + 1][m + 9] = v[k + 1][m + 9] - htmp * v[j][m + 9];
                        v[k + 1][m + 10] = v[k + 1][m + 10] - htmp * v[j][m + 10];
                        v[k + 1][m + 11] = v[k + 1][m + 11] - htmp * v[j][m + 11];
                        v[k + 1][m + 12] = v[k + 1][m + 12] - htmp * v[j][m + 12];
                        v[k + 1][m + 13] = v[k + 1][m + 13] - htmp * v[j][m + 13];
                        v[k + 1][m + 14] = v[k + 1][m + 14] - htmp * v[j][m + 14];
                        v[k + 1][m + 15] = v[k + 1][m + 15] - htmp * v[j][m + 15];

                        m += 16;
                    }

                    // Process remainder
                    for (i = (n/16) * 16; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }

                }
                h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {

                m = 0;
                for (i = 0; i < n/16; i++) {
                    v[k + 1][m] = v[k + 1][m] / h[k + 1][k];
                    v[k + 1][m + 1] = v[k + 1][m + 1] / h[k + 1][k];
                    v[k + 1][m + 2] = v[k + 1][m + 2] / h[k + 1][k];
                    v[k + 1][m + 3] = v[k + 1][m + 3] / h[k + 1][k];
                    v[k + 1][m + 4] = v[k + 1][m + 4] / h[k + 1][k];
                    v[k + 1][m + 5] = v[k + 1][m + 5] / h[k + 1][k];
                    v[k + 1][m + 6] = v[k + 1][m + 6] / h[k + 1][k];
                    v[k + 1][m + 7] = v[k + 1][m + 7] / h[k + 1][k];
                    v[k + 1][m + 8] = v[k + 1][m + 8] / h[k + 1][k];
                    v[k + 1][m + 9] = v[k + 1][m + 9] / h[k + 1][k];
                    v[k + 1][m + 10] = v[k + 1][m + 10] / h[k + 1][k];
                    v[k + 1][m + 11] = v[k + 1][m + 11] / h[k + 1][k];
                    v[k + 1][m + 12] = v[k + 1][m + 12] / h[k + 1][k];
                    v[k + 1][m + 13] = v[k + 1][m + 13] / h[k + 1][k];
                    v[k + 1][m + 14] = v[k + 1][m + 14] / h[k + 1][k];
                    v[k + 1][m + 15] = v[k + 1][m + 15] / h[k + 1][k];

                    m += 16;
                }

                // Process remainder
                for (i = (n/16) * 16; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }

            }

            if (0 < k) {

                m = 0;
                for (i = 0; i < (k + 2)/16; i++) {
                    y[m] = h[m][k];
                    y[m + 1] = h[m + 1][k];
                    y[m + 2] = h[m + 2][k];
                    y[m + 3] = h[m + 3][k];
                    y[m + 4] = h[m + 4][k];
                    y[m + 5] = h[m + 5][k];
                    y[m + 6] = h[m + 6][k];
                    y[m + 7] = h[m + 7][k];
                    y[m + 8] = h[m + 8][k];
                    y[m + 9] = h[m + 9][k];
                    y[m + 10] = h[m + 10][k];
                    y[m + 11] = h[m + 11][k];
                    y[m + 12] = h[m + 12][k];
                    y[m + 13] = h[m + 13][k];
                    y[m + 14] = h[m + 14][k];
                    y[m + 15] = h[m + 15][k];

                    m += 16;
                }

                // Process remainder
                for (i = ((k + 2)/16) * 16; i < k + 2; i++) {
                    y[i] = h[i][k];
                }

                m = 0;
                for (j = 0; j < k/16; j++) {
                    mult_givens(c[m], s[m], m, y);
                    mult_givens(c[m + 1], s[m + 1], m + 1, y);
                    mult_givens(c[m + 2], s[m + 2], m + 2, y);
                    mult_givens(c[m + 3], s[m + 3], m + 3, y);
                    mult_givens(c[m + 4], s[m + 4], m + 4, y);
                    mult_givens(c[m + 5], s[m + 5], m + 5, y);
                    mult_givens(c[m + 6], s[m + 6], m + 6, y);
                    mult_givens(c[m + 7], s[m + 7], m + 7, y);
                    mult_givens(c[m + 8], s[m + 8], m + 8, y);
                    mult_givens(c[m + 9], s[m + 9], m + 9, y);
                    mult_givens(c[m + 10], s[m + 10], m + 10, y);
                    mult_givens(c[m + 11], s[m + 11], m + 11, y);
                    mult_givens(c[m + 12], s[m + 12], m + 12, y);
                    mult_givens(c[m + 13], s[m + 13], m + 13, y);
                    mult_givens(c[m + 14], s[m + 14], m + 14, y);
                    mult_givens(c[m + 15], s[m + 15], m + 15, y);

                    m += 16;
                }

                // Process remainder
                for (j = (k/16) * 16; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }

                m = 0;
                for (i = 0; i < (k + 2)/16; i++) {
                    h[m][k] = y[m];
                    h[m + 1][k] = y[m + 1];
                    h[m + 2][k] = y[m + 2];
                    h[m + 3][k] = y[m + 3];
                    h[m + 4][k] = y[m + 4];
                    h[m + 5][k] = y[m + 5];
                    h[m + 6][k] = y[m + 6];
                    h[m + 7][k] = y[m + 7];
                    h[m + 8][k] = y[m + 8];
                    h[m + 9][k] = y[m + 9];
                    h[m + 10][k] = y[m + 10];
                    h[m + 11][k] = y[m + 11];
                    h[m + 12][k] = y[m + 12];
                    h[m + 13][k] = y[m + 13];
                    h[m + 14][k] = y[m + 14];
                    h[m + 15][k] = y[m + 15];

                    m += 16;
                }

                // Process remainder
                for (i = ((k + 2)/16) * 16; i < k + 2; i++) {
                    h[i][k] = y[i];
                }

            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];

            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("\tK   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];


        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];

            int32_t K = k - i;
            m = 0;
            double y_tmp = y[i];
            for (j = 0; j < K/16; j++) {

                y_tmp -= h[i][i + m + 1] * y[i + m + 1];
                y_tmp -= h[i][i + m + 2] * y[i + m + 2];
                y_tmp -= h[i][i + m + 3] * y[i + m + 3];
                y_tmp -= h[i][i + m + 4] * y[i + m + 4];
                y_tmp -= h[i][i + m + 5] * y[i + m + 5];
                y_tmp -= h[i][i + m + 6] * y[i + m + 6];
                y_tmp -= h[i][i + m + 7] * y[i + m + 7];
                y_tmp -= h[i][i + m + 8] * y[i + m + 8];
                y_tmp -= h[i][i + m + 9] * y[i + m + 9];
                y_tmp -= h[i][i + m + 10] * y[i + m + 10];
                y_tmp -= h[i][i + m + 11] * y[i + m + 11];
                y_tmp -= h[i][i + m + 12] * y[i + m + 12];
                y_tmp -= h[i][i + m + 13] * y[i + m + 13];
                y_tmp -= h[i][i + m + 14] * y[i + m + 14];
                y_tmp -= h[i][i + m + 15] * y[i + m + 15];
                y_tmp -= h[i][i + m + 16] * y[i + m + 16];

                m += 16;
            }

            // Process remainder
            for (j = (K/16) * 16; j < K; j++) {
                y_tmp -= h[i][i + 1 + j] * y[i + 1 + j];
            }

            y[i] = y_tmp / h[i][i];
        }
        for (i = 0; i < n; i++) {

            m = 0;
            double x_tmp = x[i];
            for (j = 0; j < (k + 1)/16; j++) {

                x_tmp += v[m][i] * y[m];
                x_tmp += v[m + 1][i] * y[m + 1];
                x_tmp += v[m + 2][i] * y[m + 2];
                x_tmp += v[m + 3][i] * y[m + 3];
                x_tmp += v[m + 4][i] * y[m + 4];
                x_tmp += v[m + 5][i] * y[m + 5];
                x_tmp += v[m + 6][i] * y[m + 6];
                x_tmp += v[m + 7][i] * y[m + 7];
                x_tmp += v[m + 8][i] * y[m + 8];
                x_tmp += v[m + 9][i] * y[m + 9];
                x_tmp += v[m + 10][i] * y[m + 10];
                x_tmp += v[m + 11][i] * y[m + 11];
                x_tmp += v[m + 12][i] * y[m + 12];
                x_tmp += v[m + 13][i] * y[m + 13];
                x_tmp += v[m + 14][i] * y[m + 14];
                x_tmp += v[m + 15][i] * y[m + 15];

                m += 16;
            }

            // Process remainder
            for (j = ((k + 1)/16) * 16; j < k + 1; j++) {
                x_tmp += v[j][i] * y[j];
            }

            // Store value
            x[i] = x_tmp;

        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP:\n");
        printf("\tIterations = %d\n", itr_used);
        printf("\tFinal residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}


void pmgmres_ilu_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[],
                                     double x[], double rhs[], int32_t itr_max, int32_t mr, double tol_abs,
                                     double tol_rel) {
    double av;
    double *c;
    double delta = 1.0e-03;
    double *g;
    double **h;
    double htmp;
    int32_t i;
    int32_t itr;
    int32_t itr_used;
    int32_t j;
    int32_t k;
    int32_t k_copy;
    double *l;
    double mu;
    double *r;
    double rho;
    double rho_tol;
    double *s;

    int32_t *ua;
    double **v;
    int32_t verbose = 1;
    double *y;

    itr_used = 0;

    c = (double *) malloc((mr + 1) * sizeof(double));
    g = (double *) malloc((mr + 1) * sizeof(double));
    h = dmatrix(0, mr, 0, mr - 1);
    l = (double *) malloc((ia[n] + 1) * sizeof(double));
    r = (double *) malloc(n * sizeof(double));
    s = (double *) malloc((mr + 1) * sizeof(double));
    ua = (int32_t *) malloc(n * sizeof(int32_t));
    v = dmatrix(0, mr, 0, n - 1);
    y = (double *) malloc((mr + 1) * sizeof(double));


    rearrange_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala);

    diagonal_pointer_csrvi_ilp_unroll(n, nz_num, ia, ja, ua);

    ilu_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, ua, l);

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP\n");
        printf("\tNumber of unknowns = %d\n", n);
        printf("\tAX_LOOP_UNROLL_FACTOR = %d\n", ax_unroll_factor);
        printf("\tILU_LOOP_UNROLL_FACTOR = %d\n", ilu_unroll_factor);
        printf("\tLUS_LOOP_UNROLL_FACTOR = %d\n", lus_unroll_factor);
        printf("\tREARRANGE_LOOP_UNROLL_FACTOR = %d\n", rearrange_unroll_factor);
        printf("\tDIAG_LOOP_UNROLL_FACTOR = %d\n", diag_unroll_factor);
        printf("\n\n");
    }

    for (itr = 0; itr < itr_max; itr++) {

        ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, x, r);

        int32_t m = 0;
        for (i = 0; i < n/32; i++) {
            r[m] = rhs[m] - r[m];
            r[m + 1] = rhs[m + 1] - r[m + 1];
            r[m + 2] = rhs[m + 2] - r[m + 2];
            r[m + 3] = rhs[m + 3] - r[m + 3];
            r[m + 4] = rhs[m + 4] - r[m + 4];
            r[m + 5] = rhs[m + 5] - r[m + 5];
            r[m + 6] = rhs[m + 6] - r[m + 6];
            r[m + 7] = rhs[m + 7] - r[m + 7];
            r[m + 8] = rhs[m + 8] - r[m + 8];
            r[m + 9] = rhs[m + 9] - r[m + 9];
            r[m + 10] = rhs[m + 10] - r[m + 10];
            r[m + 11] = rhs[m + 11] - r[m + 11];
            r[m + 12] = rhs[m + 12] - r[m + 12];
            r[m + 13] = rhs[m + 13] - r[m + 13];
            r[m + 14] = rhs[m + 14] - r[m + 14];
            r[m + 15] = rhs[m + 15] - r[m + 15];
            r[m + 16] = rhs[m + 16] - r[m + 16];
            r[m + 17] = rhs[m + 17] - r[m + 17];
            r[m + 18] = rhs[m + 18] - r[m + 18];
            r[m + 19] = rhs[m + 19] - r[m + 19];
            r[m + 20] = rhs[m + 20] - r[m + 20];
            r[m + 21] = rhs[m + 21] - r[m + 21];
            r[m + 22] = rhs[m + 22] - r[m + 22];
            r[m + 23] = rhs[m + 23] - r[m + 23];
            r[m + 24] = rhs[m + 24] - r[m + 24];
            r[m + 25] = rhs[m + 25] - r[m + 25];
            r[m + 26] = rhs[m + 26] - r[m + 26];
            r[m + 27] = rhs[m + 27] - r[m + 27];
            r[m + 28] = rhs[m + 28] - r[m + 28];
            r[m + 29] = rhs[m + 29] - r[m + 29];
            r[m + 30] = rhs[m + 30] - r[m + 30];
            r[m + 31] = rhs[m + 31] - r[m + 31];

            m += 32;
        }

        // Process remainder
        for (i = (n/32) * 32; i < n; i++) {
            r[i] = rhs[i] - r[i];
        }

        lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, r, r);

        rho = sqrt(r8vec_dot_unroll(n, r, r));

        if (verbose) {
            printf("\tITR = %d  Residual = %e\n", itr, rho);
        }

        if (itr == 0) {
            rho_tol = rho * tol_rel;
        }

        m = 0;
        for (i = 0; i < n/32; i++) {
            v[0][m] = r[m] / rho;
            v[0][m + 1] = r[m + 1] / rho;
            v[0][m + 2] = r[m + 2] / rho;
            v[0][m + 3] = r[m + 3] / rho;
            v[0][m + 4] = r[m + 4] / rho;
            v[0][m + 5] = r[m + 5] / rho;
            v[0][m + 6] = r[m + 6] / rho;
            v[0][m + 7] = r[m + 7] / rho;
            v[0][m + 8] = r[m + 8] / rho;
            v[0][m + 9] = r[m + 9] / rho;
            v[0][m + 10] = r[m + 10] / rho;
            v[0][m + 11] = r[m + 11] / rho;
            v[0][m + 12] = r[m + 12] / rho;
            v[0][m + 13] = r[m + 13] / rho;
            v[0][m + 14] = r[m + 14] / rho;
            v[0][m + 15] = r[m + 15] / rho;
            v[0][m + 16] = r[m + 16] / rho;
            v[0][m + 17] = r[m + 17] / rho;
            v[0][m + 18] = r[m + 18] / rho;
            v[0][m + 19] = r[m + 19] / rho;
            v[0][m + 20] = r[m + 20] / rho;
            v[0][m + 21] = r[m + 21] / rho;
            v[0][m + 22] = r[m + 22] / rho;
            v[0][m + 23] = r[m + 23] / rho;
            v[0][m + 24] = r[m + 24] / rho;
            v[0][m + 25] = r[m + 25] / rho;
            v[0][m + 26] = r[m + 26] / rho;
            v[0][m + 27] = r[m + 27] / rho;
            v[0][m + 28] = r[m + 28] / rho;
            v[0][m + 29] = r[m + 29] / rho;
            v[0][m + 30] = r[m + 30] / rho;
            v[0][m + 31] = r[m + 31] / rho;

            m += 32;
        }

        // Process remainder
        for (i = (n/32) * 32; i < n; i++) {
            v[0][i] = r[i] / rho;
        }


        // ____________________________________________________________
        // ____________________________________________________________


        g[0] = rho;

        m = 0;
        for (i = 0; i < mr/32; i++) {
            g[m + 1] = 0.0;
            g[m + 2] = 0.0;
            g[m + 3] = 0.0;
            g[m + 4] = 0.0;
            g[m + 5] = 0.0;
            g[m + 6] = 0.0;
            g[m + 7] = 0.0;
            g[m + 8] = 0.0;
            g[m + 9] = 0.0;
            g[m + 10] = 0.0;
            g[m + 11] = 0.0;
            g[m + 12] = 0.0;
            g[m + 13] = 0.0;
            g[m + 14] = 0.0;
            g[m + 15] = 0.0;
            g[m + 16] = 0.0;
            g[m + 17] = 0.0;
            g[m + 18] = 0.0;
            g[m + 19] = 0.0;
            g[m + 20] = 0.0;
            g[m + 21] = 0.0;
            g[m + 22] = 0.0;
            g[m + 23] = 0.0;
            g[m + 24] = 0.0;
            g[m + 25] = 0.0;
            g[m + 26] = 0.0;
            g[m + 27] = 0.0;
            g[m + 28] = 0.0;
            g[m + 29] = 0.0;
            g[m + 30] = 0.0;
            g[m + 31] = 0.0;
            g[m + 32] = 0.0;

            m += 32;
        }

        // Process remainder
        for (i = (mr/32) * 32; i < mr; i++) {
            g[i + 1] = 0.0;
        }



        // ____________________________________________________________
        // ____________________________________________________________


        for (i = 0; i < mr + 1; i++) {

            m = 0;
            for (j = 0; j < mr/32; j++) {
                h[i][m] = 0.0;
                h[i][m + 1] = 0.0;
                h[i][m + 2] = 0.0;
                h[i][m + 3] = 0.0;
                h[i][m + 4] = 0.0;
                h[i][m + 5] = 0.0;
                h[i][m + 6] = 0.0;
                h[i][m + 7] = 0.0;
                h[i][m + 8] = 0.0;
                h[i][m + 9] = 0.0;
                h[i][m + 10] = 0.0;
                h[i][m + 11] = 0.0;
                h[i][m + 12] = 0.0;
                h[i][m + 13] = 0.0;
                h[i][m + 14] = 0.0;
                h[i][m + 15] = 0.0;
                h[i][m + 16] = 0.0;
                h[i][m + 17] = 0.0;
                h[i][m + 18] = 0.0;
                h[i][m + 19] = 0.0;
                h[i][m + 20] = 0.0;
                h[i][m + 21] = 0.0;
                h[i][m + 22] = 0.0;
                h[i][m + 23] = 0.0;
                h[i][m + 24] = 0.0;
                h[i][m + 25] = 0.0;
                h[i][m + 26] = 0.0;
                h[i][m + 27] = 0.0;
                h[i][m + 28] = 0.0;
                h[i][m + 29] = 0.0;
                h[i][m + 30] = 0.0;
                h[i][m + 31] = 0.0;

                m += 32;
            }

            // Process remainder
            for (j = (mr/32) * 32; j < mr; j++) {
                h[i][j] = 0.0;
            }

        }

        // ____________________________________________________________
        // ____________________________________________________________
        for (k = 0; k < mr; k++) {
            k_copy = k;

            ax_csrvi_ilp_unroll(n, nz_num, ia, ja, va, vala, v[k], v[k + 1]);

            lus_csrvi_ilp_unroll(n, nz_num, ia, ja, l, ua, v[k + 1], v[k + 1]);

            av = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            for (j = 0; j <= k; j++) {
                h[j][k] = r8vec_dot_unroll(n, v[k + 1], v[j]);

                m = 0;
                for (i = 0; i < n/32; i++) {
                    v[k + 1][m] = v[k + 1][m] - h[j][k] * v[j][m];
                    v[k + 1][m + 1] = v[k + 1][m + 1] - h[j][k] * v[j][m + 1];
                    v[k + 1][m + 2] = v[k + 1][m + 2] - h[j][k] * v[j][m + 2];
                    v[k + 1][m + 3] = v[k + 1][m + 3] - h[j][k] * v[j][m + 3];
                    v[k + 1][m + 4] = v[k + 1][m + 4] - h[j][k] * v[j][m + 4];
                    v[k + 1][m + 5] = v[k + 1][m + 5] - h[j][k] * v[j][m + 5];
                    v[k + 1][m + 6] = v[k + 1][m + 6] - h[j][k] * v[j][m + 6];
                    v[k + 1][m + 7] = v[k + 1][m + 7] - h[j][k] * v[j][m + 7];
                    v[k + 1][m + 8] = v[k + 1][m + 8] - h[j][k] * v[j][m + 8];
                    v[k + 1][m + 9] = v[k + 1][m + 9] - h[j][k] * v[j][m + 9];
                    v[k + 1][m + 10] = v[k + 1][m + 10] - h[j][k] * v[j][m + 10];
                    v[k + 1][m + 11] = v[k + 1][m + 11] - h[j][k] * v[j][m + 11];
                    v[k + 1][m + 12] = v[k + 1][m + 12] - h[j][k] * v[j][m + 12];
                    v[k + 1][m + 13] = v[k + 1][m + 13] - h[j][k] * v[j][m + 13];
                    v[k + 1][m + 14] = v[k + 1][m + 14] - h[j][k] * v[j][m + 14];
                    v[k + 1][m + 15] = v[k + 1][m + 15] - h[j][k] * v[j][m + 15];
                    v[k + 1][m + 16] = v[k + 1][m + 16] - h[j][k] * v[j][m + 16];
                    v[k + 1][m + 17] = v[k + 1][m + 17] - h[j][k] * v[j][m + 17];
                    v[k + 1][m + 18] = v[k + 1][m + 18] - h[j][k] * v[j][m + 18];
                    v[k + 1][m + 19] = v[k + 1][m + 19] - h[j][k] * v[j][m + 19];
                    v[k + 1][m + 20] = v[k + 1][m + 20] - h[j][k] * v[j][m + 20];
                    v[k + 1][m + 21] = v[k + 1][m + 21] - h[j][k] * v[j][m + 21];
                    v[k + 1][m + 22] = v[k + 1][m + 22] - h[j][k] * v[j][m + 22];
                    v[k + 1][m + 23] = v[k + 1][m + 23] - h[j][k] * v[j][m + 23];
                    v[k + 1][m + 24] = v[k + 1][m + 24] - h[j][k] * v[j][m + 24];
                    v[k + 1][m + 25] = v[k + 1][m + 25] - h[j][k] * v[j][m + 25];
                    v[k + 1][m + 26] = v[k + 1][m + 26] - h[j][k] * v[j][m + 26];
                    v[k + 1][m + 27] = v[k + 1][m + 27] - h[j][k] * v[j][m + 27];
                    v[k + 1][m + 28] = v[k + 1][m + 28] - h[j][k] * v[j][m + 28];
                    v[k + 1][m + 29] = v[k + 1][m + 29] - h[j][k] * v[j][m + 29];
                    v[k + 1][m + 30] = v[k + 1][m + 30] - h[j][k] * v[j][m + 30];
                    v[k + 1][m + 31] = v[k + 1][m + 31] - h[j][k] * v[j][m + 31];

                    m += 32;
                }

                // Process remainder
                for (i = (n/32) * 32; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] - h[j][k] * v[j][i];
                }

                // ____________________________________________________________
                // ____________________________________________________________
            }
            h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));

            if ((av + delta * h[k + 1][k]) == av) {

                for (j = 0; j < k + 1; j++) {
                    htmp = r8vec_dot_unroll(n, v[k + 1], v[j]);
                    h[j][k] = h[j][k] + htmp;

                    m = 0;
                    for (i = 0; i < n/32; i++) {
                        v[k + 1][m] = v[k + 1][m] - htmp * v[j][m];
                        v[k + 1][m + 1] = v[k + 1][m + 1] - htmp * v[j][m + 1];
                        v[k + 1][m + 2] = v[k + 1][m + 2] - htmp * v[j][m + 2];
                        v[k + 1][m + 3] = v[k + 1][m + 3] - htmp * v[j][m + 3];
                        v[k + 1][m + 4] = v[k + 1][m + 4] - htmp * v[j][m + 4];
                        v[k + 1][m + 5] = v[k + 1][m + 5] - htmp * v[j][m + 5];
                        v[k + 1][m + 6] = v[k + 1][m + 6] - htmp * v[j][m + 6];
                        v[k + 1][m + 7] = v[k + 1][m + 7] - htmp * v[j][m + 7];
                        v[k + 1][m + 8] = v[k + 1][m + 8] - htmp * v[j][m + 8];
                        v[k + 1][m + 9] = v[k + 1][m + 9] - htmp * v[j][m + 9];
                        v[k + 1][m + 10] = v[k + 1][m + 10] - htmp * v[j][m + 10];
                        v[k + 1][m + 11] = v[k + 1][m + 11] - htmp * v[j][m + 11];
                        v[k + 1][m + 12] = v[k + 1][m + 12] - htmp * v[j][m + 12];
                        v[k + 1][m + 13] = v[k + 1][m + 13] - htmp * v[j][m + 13];
                        v[k + 1][m + 14] = v[k + 1][m + 14] - htmp * v[j][m + 14];
                        v[k + 1][m + 15] = v[k + 1][m + 15] - htmp * v[j][m + 15];
                        v[k + 1][m + 16] = v[k + 1][m + 16] - htmp * v[j][m + 16];
                        v[k + 1][m + 17] = v[k + 1][m + 17] - htmp * v[j][m + 17];
                        v[k + 1][m + 18] = v[k + 1][m + 18] - htmp * v[j][m + 18];
                        v[k + 1][m + 19] = v[k + 1][m + 19] - htmp * v[j][m + 19];
                        v[k + 1][m + 20] = v[k + 1][m + 20] - htmp * v[j][m + 20];
                        v[k + 1][m + 21] = v[k + 1][m + 21] - htmp * v[j][m + 21];
                        v[k + 1][m + 22] = v[k + 1][m + 22] - htmp * v[j][m + 22];
                        v[k + 1][m + 23] = v[k + 1][m + 23] - htmp * v[j][m + 23];
                        v[k + 1][m + 24] = v[k + 1][m + 24] - htmp * v[j][m + 24];
                        v[k + 1][m + 25] = v[k + 1][m + 25] - htmp * v[j][m + 25];
                        v[k + 1][m + 26] = v[k + 1][m + 26] - htmp * v[j][m + 26];
                        v[k + 1][m + 27] = v[k + 1][m + 27] - htmp * v[j][m + 27];
                        v[k + 1][m + 28] = v[k + 1][m + 28] - htmp * v[j][m + 28];
                        v[k + 1][m + 29] = v[k + 1][m + 29] - htmp * v[j][m + 29];
                        v[k + 1][m + 30] = v[k + 1][m + 30] - htmp * v[j][m + 30];
                        v[k + 1][m + 31] = v[k + 1][m + 31] - htmp * v[j][m + 31];

                        m += 32;
                    }

                    // Process remainder
                    for (i = (n/32) * 32; i < n; i++) {
                        v[k + 1][i] = v[k + 1][i] - htmp * v[j][i];
                    }

                }
                h[k + 1][k] = sqrt(r8vec_dot_unroll(n, v[k + 1], v[k + 1]));
            }

            if (h[k + 1][k] != 0.0) {

                m = 0;
                for (i = 0; i < n/32; i++) {
                    v[k + 1][m] = v[k + 1][m] / h[k + 1][k];
                    v[k + 1][m + 1] = v[k + 1][m + 1] / h[k + 1][k];
                    v[k + 1][m + 2] = v[k + 1][m + 2] / h[k + 1][k];
                    v[k + 1][m + 3] = v[k + 1][m + 3] / h[k + 1][k];
                    v[k + 1][m + 4] = v[k + 1][m + 4] / h[k + 1][k];
                    v[k + 1][m + 5] = v[k + 1][m + 5] / h[k + 1][k];
                    v[k + 1][m + 6] = v[k + 1][m + 6] / h[k + 1][k];
                    v[k + 1][m + 7] = v[k + 1][m + 7] / h[k + 1][k];
                    v[k + 1][m + 8] = v[k + 1][m + 8] / h[k + 1][k];
                    v[k + 1][m + 9] = v[k + 1][m + 9] / h[k + 1][k];
                    v[k + 1][m + 10] = v[k + 1][m + 10] / h[k + 1][k];
                    v[k + 1][m + 11] = v[k + 1][m + 11] / h[k + 1][k];
                    v[k + 1][m + 12] = v[k + 1][m + 12] / h[k + 1][k];
                    v[k + 1][m + 13] = v[k + 1][m + 13] / h[k + 1][k];
                    v[k + 1][m + 14] = v[k + 1][m + 14] / h[k + 1][k];
                    v[k + 1][m + 15] = v[k + 1][m + 15] / h[k + 1][k];
                    v[k + 1][m + 16] = v[k + 1][m + 16] / h[k + 1][k];
                    v[k + 1][m + 17] = v[k + 1][m + 17] / h[k + 1][k];
                    v[k + 1][m + 18] = v[k + 1][m + 18] / h[k + 1][k];
                    v[k + 1][m + 19] = v[k + 1][m + 19] / h[k + 1][k];
                    v[k + 1][m + 20] = v[k + 1][m + 20] / h[k + 1][k];
                    v[k + 1][m + 21] = v[k + 1][m + 21] / h[k + 1][k];
                    v[k + 1][m + 22] = v[k + 1][m + 22] / h[k + 1][k];
                    v[k + 1][m + 23] = v[k + 1][m + 23] / h[k + 1][k];
                    v[k + 1][m + 24] = v[k + 1][m + 24] / h[k + 1][k];
                    v[k + 1][m + 25] = v[k + 1][m + 25] / h[k + 1][k];
                    v[k + 1][m + 26] = v[k + 1][m + 26] / h[k + 1][k];
                    v[k + 1][m + 27] = v[k + 1][m + 27] / h[k + 1][k];
                    v[k + 1][m + 28] = v[k + 1][m + 28] / h[k + 1][k];
                    v[k + 1][m + 29] = v[k + 1][m + 29] / h[k + 1][k];
                    v[k + 1][m + 30] = v[k + 1][m + 30] / h[k + 1][k];
                    v[k + 1][m + 31] = v[k + 1][m + 31] / h[k + 1][k];

                    m += 32;
                }

                // Process remainder
                for (i = (n/32) * 32; i < n; i++) {
                    v[k + 1][i] = v[k + 1][i] / h[k + 1][k];
                }

            }

            if (0 < k) {

                m = 0;
                for (i = 0; i < (k + 2)/32; i++) {
                    y[m] = h[m][k];
                    y[m + 1] = h[m + 1][k];
                    y[m + 2] = h[m + 2][k];
                    y[m + 3] = h[m + 3][k];
                    y[m + 4] = h[m + 4][k];
                    y[m + 5] = h[m + 5][k];
                    y[m + 6] = h[m + 6][k];
                    y[m + 7] = h[m + 7][k];
                    y[m + 8] = h[m + 8][k];
                    y[m + 9] = h[m + 9][k];
                    y[m + 10] = h[m + 10][k];
                    y[m + 11] = h[m + 11][k];
                    y[m + 12] = h[m + 12][k];
                    y[m + 13] = h[m + 13][k];
                    y[m + 14] = h[m + 14][k];
                    y[m + 15] = h[m + 15][k];
                    y[m + 16] = h[m + 16][k];
                    y[m + 17] = h[m + 17][k];
                    y[m + 18] = h[m + 18][k];
                    y[m + 19] = h[m + 19][k];
                    y[m + 20] = h[m + 20][k];
                    y[m + 21] = h[m + 21][k];
                    y[m + 22] = h[m + 22][k];
                    y[m + 23] = h[m + 23][k];
                    y[m + 24] = h[m + 24][k];
                    y[m + 25] = h[m + 25][k];
                    y[m + 26] = h[m + 26][k];
                    y[m + 27] = h[m + 27][k];
                    y[m + 28] = h[m + 28][k];
                    y[m + 29] = h[m + 29][k];
                    y[m + 30] = h[m + 30][k];
                    y[m + 31] = h[m + 31][k];

                    m += 32;
                }

                // Process remainder
                for (i = ((k + 2)/32) * 32; i < k + 2; i++) {
                    y[i] = h[i][k];
                }

                m = 0;
                for (j = 0; j < k/32; j++) {
                    mult_givens(c[m], s[m], m, y);
                    mult_givens(c[m + 1], s[m + 1], m + 1, y);
                    mult_givens(c[m + 2], s[m + 2], m + 2, y);
                    mult_givens(c[m + 3], s[m + 3], m + 3, y);
                    mult_givens(c[m + 4], s[m + 4], m + 4, y);
                    mult_givens(c[m + 5], s[m + 5], m + 5, y);
                    mult_givens(c[m + 6], s[m + 6], m + 6, y);
                    mult_givens(c[m + 7], s[m + 7], m + 7, y);
                    mult_givens(c[m + 8], s[m + 8], m + 8, y);
                    mult_givens(c[m + 9], s[m + 9], m + 9, y);
                    mult_givens(c[m + 10], s[m + 10], m + 10, y);
                    mult_givens(c[m + 11], s[m + 11], m + 11, y);
                    mult_givens(c[m + 12], s[m + 12], m + 12, y);
                    mult_givens(c[m + 13], s[m + 13], m + 13, y);
                    mult_givens(c[m + 14], s[m + 14], m + 14, y);
                    mult_givens(c[m + 15], s[m + 15], m + 15, y);
                    mult_givens(c[m + 16], s[m + 16], m + 16, y);
                    mult_givens(c[m + 17], s[m + 17], m + 17, y);
                    mult_givens(c[m + 18], s[m + 18], m + 18, y);
                    mult_givens(c[m + 19], s[m + 19], m + 19, y);
                    mult_givens(c[m + 20], s[m + 20], m + 20, y);
                    mult_givens(c[m + 21], s[m + 21], m + 21, y);
                    mult_givens(c[m + 22], s[m + 22], m + 22, y);
                    mult_givens(c[m + 23], s[m + 23], m + 23, y);
                    mult_givens(c[m + 24], s[m + 24], m + 24, y);
                    mult_givens(c[m + 25], s[m + 25], m + 25, y);
                    mult_givens(c[m + 26], s[m + 26], m + 26, y);
                    mult_givens(c[m + 27], s[m + 27], m + 27, y);
                    mult_givens(c[m + 28], s[m + 28], m + 28, y);
                    mult_givens(c[m + 29], s[m + 29], m + 29, y);
                    mult_givens(c[m + 30], s[m + 30], m + 30, y);
                    mult_givens(c[m + 31], s[m + 31], m + 31, y);

                    m += 32;
                }

                // Process remainder
                for (j = (k/32) * 32; j < k; j++) {
                    mult_givens(c[j], s[j], j, y);
                }

                m = 0;
                for (i = 0; i < (k + 2)/32; i++) {

                    h[m][k] = y[m];
                    h[m + 1][k] = y[m + 1];
                    h[m + 2][k] = y[m + 2];
                    h[m + 3][k] = y[m + 3];
                    h[m + 4][k] = y[m + 4];
                    h[m + 5][k] = y[m + 5];
                    h[m + 6][k] = y[m + 6];
                    h[m + 7][k] = y[m + 7];
                    h[m + 8][k] = y[m + 8];
                    h[m + 9][k] = y[m + 9];
                    h[m + 10][k] = y[m + 10];
                    h[m + 11][k] = y[m + 11];
                    h[m + 12][k] = y[m + 12];
                    h[m + 13][k] = y[m + 13];
                    h[m + 14][k] = y[m + 14];
                    h[m + 15][k] = y[m + 15];
                    h[m + 16][k] = y[m + 16];
                    h[m + 17][k] = y[m + 17];
                    h[m + 18][k] = y[m + 18];
                    h[m + 19][k] = y[m + 19];
                    h[m + 20][k] = y[m + 20];
                    h[m + 21][k] = y[m + 21];
                    h[m + 22][k] = y[m + 22];
                    h[m + 23][k] = y[m + 23];
                    h[m + 24][k] = y[m + 24];
                    h[m + 25][k] = y[m + 25];
                    h[m + 26][k] = y[m + 26];
                    h[m + 27][k] = y[m + 27];
                    h[m + 28][k] = y[m + 28];
                    h[m + 29][k] = y[m + 29];
                    h[m + 30][k] = y[m + 30];
                    h[m + 31][k] = y[m + 31];

                    m += 32;
                }

                // Process remainder
                for (i = ((k + 2)/32) * 32; i < k + 2; i++) {
                    h[i][k] = y[i];
                }

            }
            mu = sqrt(h[k][k] * h[k][k] + h[k + 1][k] * h[k + 1][k]);

            c[k] = h[k][k] / mu;
            s[k] = -h[k + 1][k] / mu;
            h[k][k] = c[k] * h[k][k] - s[k] * h[k + 1][k];

            h[k + 1][k] = 0.0;
            mult_givens(c[k], s[k], k, g);

            rho = fabs(g[k + 1]);

            itr_used = itr_used + 1;

            if (verbose) {
                printf("\tK   = %d  Residual = %e\n", k, rho);
            }

            if (rho <= rho_tol && rho <= tol_abs) {
                break;
            }
        }

        k = k_copy;

        y[k] = g[k] / h[k][k];


        for (i = k - 1; 0 <= i; i--) {
            y[i] = g[i];

            int32_t K = k - i;
            m = 0;
            double y_tmp = y[i];
            for (j = 0; j < K/32; j++) {

                y_tmp -= h[i][i + m + 1] * y[i + m + 1];
                y_tmp -= h[i][i + m + 2] * y[i + m + 2];
                y_tmp -= h[i][i + m + 3] * y[i + m + 3];
                y_tmp -= h[i][i + m + 4] * y[i + m + 4];
                y_tmp -= h[i][i + m + 5] * y[i + m + 5];
                y_tmp -= h[i][i + m + 6] * y[i + m + 6];
                y_tmp -= h[i][i + m + 7] * y[i + m + 7];
                y_tmp -= h[i][i + m + 8] * y[i + m + 8];
                y_tmp -= h[i][i + m + 9] * y[i + m + 9];
                y_tmp -= h[i][i + m + 10] * y[i + m + 10];
                y_tmp -= h[i][i + m + 11] * y[i + m + 11];
                y_tmp -= h[i][i + m + 12] * y[i + m + 12];
                y_tmp -= h[i][i + m + 13] * y[i + m + 13];
                y_tmp -= h[i][i + m + 14] * y[i + m + 14];
                y_tmp -= h[i][i + m + 15] * y[i + m + 15];
                y_tmp -= h[i][i + m + 16] * y[i + m + 16];
                y_tmp -= h[i][i + m + 17] * y[i + m + 17];
                y_tmp -= h[i][i + m + 18] * y[i + m + 18];
                y_tmp -= h[i][i + m + 19] * y[i + m + 19];
                y_tmp -= h[i][i + m + 20] * y[i + m + 20];
                y_tmp -= h[i][i + m + 21] * y[i + m + 21];
                y_tmp -= h[i][i + m + 22] * y[i + m + 22];
                y_tmp -= h[i][i + m + 23] * y[i + m + 23];
                y_tmp -= h[i][i + m + 24] * y[i + m + 24];
                y_tmp -= h[i][i + m + 25] * y[i + m + 25];
                y_tmp -= h[i][i + m + 26] * y[i + m + 26];
                y_tmp -= h[i][i + m + 27] * y[i + m + 27];
                y_tmp -= h[i][i + m + 28] * y[i + m + 28];
                y_tmp -= h[i][i + m + 29] * y[i + m + 29];
                y_tmp -= h[i][i + m + 30] * y[i + m + 30];
                y_tmp -= h[i][i + m + 31] * y[i + m + 31];
                y_tmp -= h[i][i + m + 32] * y[i + m + 32];

                m += 32;
            }

            // Process remainder
            for (j = (K/32) * 32; j < K; j++) {
                y_tmp -= h[i][i + 1 + j] * y[i + 1 + j];
            }

            y[i] = y_tmp / h[i][i];
        }
        for (i = 0; i < n; i++) {

            m = 0;
            double x_tmp = x[i];
            for (j = 0; j < (k + 1)/32; j++) {

                x_tmp += v[m][i] * y[m];
                x_tmp += v[m + 1][i] * y[m + 1];
                x_tmp += v[m + 2][i] * y[m + 2];
                x_tmp += v[m + 3][i] * y[m + 3];
                x_tmp += v[m + 4][i] * y[m + 4];
                x_tmp += v[m + 5][i] * y[m + 5];
                x_tmp += v[m + 6][i] * y[m + 6];
                x_tmp += v[m + 7][i] * y[m + 7];
                x_tmp += v[m + 8][i] * y[m + 8];
                x_tmp += v[m + 9][i] * y[m + 9];
                x_tmp += v[m + 10][i] * y[m + 10];
                x_tmp += v[m + 11][i] * y[m + 11];
                x_tmp += v[m + 12][i] * y[m + 12];
                x_tmp += v[m + 13][i] * y[m + 13];
                x_tmp += v[m + 14][i] * y[m + 14];
                x_tmp += v[m + 15][i] * y[m + 15];
                x_tmp += v[m + 16][i] * y[m + 16];
                x_tmp += v[m + 17][i] * y[m + 17];
                x_tmp += v[m + 18][i] * y[m + 18];
                x_tmp += v[m + 19][i] * y[m + 19];
                x_tmp += v[m + 20][i] * y[m + 20];
                x_tmp += v[m + 21][i] * y[m + 21];
                x_tmp += v[m + 22][i] * y[m + 22];
                x_tmp += v[m + 23][i] * y[m + 23];
                x_tmp += v[m + 24][i] * y[m + 24];
                x_tmp += v[m + 25][i] * y[m + 25];
                x_tmp += v[m + 26][i] * y[m + 26];
                x_tmp += v[m + 27][i] * y[m + 27];
                x_tmp += v[m + 28][i] * y[m + 28];
                x_tmp += v[m + 29][i] * y[m + 29];
                x_tmp += v[m + 30][i] * y[m + 30];
                x_tmp += v[m + 31][i] * y[m + 31];

                m += 32;
            }

            // Process remainder
            for (j = ((k + 1)/32) * 32; j < k + 1; j++) {
                x_tmp += v[j][i] * y[j];
            }

            // Store value
            x[i] = x_tmp;

        }

        if (rho <= rho_tol && rho <= tol_abs) {
            break;
        }
    }

    if (verbose) {
        printf("\n");
        printf("PMGMRES_ILU_CSR_VI_ILP:\n");
        printf("\tIterations = %d\n", itr_used);
        printf("\tFinal residual = %e\n", rho);
    }
/*
  Free memory.
*/
    free(c);
    free(g);
    free_dmatrix(h, 0, mr, 0, mr - 1);
    free(l);
    free(r);
    free(s);
    free(ua);
    free_dmatrix(v, 0, mr, 0, n - 1);
    free(y);

    return;
}


/*
  [FastCode] Computes the incomplete LU factorization of a matrix in CSR-VI format

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double A[NZ_NUM], the matrix values.

    Input, int32_t UA[N], the index of the diagonal element of each row.

    Output, double L[NZ_NUM], the values of the ILU factorization of A.
*/
void ilu_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, int32_t *ua,
                          double *l) {

    switch(ilu_unroll_factor) {
        case 1:
            ilu_csrvi(n, nz_num, ia, ja, va, vala, ua, l);
            break;
        case 4:
            ilu_csrvi_ilp_unroll_4(n, nz_num, ia, ja, va, vala, ua, l);
            break;
        case 8:
            ilu_csrvi_ilp_unroll_8(n, nz_num, ia, ja, va, vala, ua, l);
            break;
        case 16:
            ilu_csrvi_ilp_unroll_16(n, nz_num, ia, ja, va, vala, ua, l);
            break;
        case 32:
            ilu_csrvi_ilp_unroll_32(n, nz_num, ia, ja, va, vala, ua, l);
            break;
        default:
            ilu_csrvi(n, nz_num, ia, ja, va, vala, ua, l);
            break;
    }

}



/*
 * [FastCode] Incomplete LU decomposition (unroll factor: 4)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'ilu_csrvi_ilp_unroll'
 */
void ilu_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
                       double l[]) {
    int32_t *iw;
    int32_t i;
    int32_t j;
    int32_t jj;
    int32_t jrow;
    int32_t jw;
    int32_t k;
    double tl;

    iw = (int32_t *) malloc(n * sizeof(int32_t));
/*
  Copy A.
*/

    /* [OLD] [BEGIN]
    for (k = 0; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }
       [OLD] [END] */

/*    // First optimization
    for (k = 0; k < nz_num/4; k++) {
        l[4 * k] = vala[va[4 * k]];
        l[4 * k + 1] = vala[va[4 * k + 1]];
        l[4 * k + 2] = vala[va[4 * k + 2]];
        l[4 * k + 3] = vala[va[4 * k + 3]];
    }

    // Process remainder
    for (k = (nz_num/4) * 4; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }*/

    // Second optimization
    int m = 0;
    for (k = 0; k < nz_num/4; k++) {
        l[m] = vala[va[m]];
        l[m + 1] = vala[va[m + 1]];
        l[m + 2] = vala[va[m + 2]];
        l[m + 3] = vala[va[m + 3]];

        m += 4;
    }

    // Process remainder
    for (k = (nz_num/4) * 4; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }

    for (i = 0; i < n; i++) {
/*
  IW points to the nonzero entries in row I.
*/
        /* [OLD] [START]
        for (j = 0; j < n; j++) {
            iw[j] = -1;
        }
           [OLD] [END] */

/*      First optimization
        for (j = 0; j < n/4; j++) {
            iw[4 * j] = -1;
            iw[4 * j + 1] = -1;
            iw[4 * j + 2] = -1;
            iw[4 * j + 3] = -1;
        }

        // Process remainder
        for (j = (n/4) * 4; j < n; j++) {
            iw[j] = -1;
        }*/

        // Second optimization
        m = 0;
        for (j = 0; j < n/4; j++) {
            iw[m] = -1;
            iw[m + 1] = -1;
            iw[m + 2] = -1;
            iw[m + 3] = -1;

            m += 4;
        }

        // Process remainder
        for (j = (n/4) * 4; j < n; j++) {
            iw[j] = -1;
        }

        // ____________________________________________________________
        // ____________________________________________________________


        /* [OLD] [START]
        for (k = ia[i]; k <= ia[i + 1] - 1; k++) {
            iw[ja[k]] = k;
        }
           [OLD] [END] */

/*
        // First modification
        for (k = ia[i]; k < ia[i + 1]; k++) {
            iw[ja[k]] = k;
        }*/

/*
        // First optimization
        int32_t ia_tmp = ia[i + 1];
        for (k = ia[i]; k < ia_tmp; k++) {
            iw[ja[k]] = k;
        }*/

/*
        // Second optimization
        int K = ia[i + 1] - ia[i];
        for (k = 0; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }*/

/*
        // Third optimization
        int K = ia[i + 1] - ia[i];
        for (k = 0; k < K/4; k++) {
            iw[ja[ia[i] + 4 * k]] = ia[i] + 4 * k;
            iw[ja[ia[i] + 4 * k + 1]] = ia[i] + 4 * k + 1;
            iw[ja[ia[i] + 4 * k + 2]] = ia[i] + 4 * k + 2;
            iw[ja[ia[i] + 4 * k + 3]] = ia[i] + 4 * k + 3;
        }

        // Process remainder
        for (k = (K/4) * 4; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }*/


        // Fourth optimization
        m = 0;
        int K = ia[i + 1] - ia[i];
        for (k = 0; k < K/4; k++) {
            iw[ja[ia[i] + m]] = ia[i] + m;
            iw[ja[ia[i] + m + 1]] = ia[i] + m + 1;
            iw[ja[ia[i] + m + 2]] = ia[i] + m + 2;
            iw[ja[ia[i] + m + 3]] = ia[i] + m + 3;

            m += 4;
        }

        // Process remainder
        for (k = (K/4) * 4; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }

        // ____________________________________________________________
        // ____________________________________________________________

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }

            tl = l[j] * l[ua[jrow]];
            l[j] = tl;

            // ____________________________________________________________
            // ____________________________________________________________

            /* [OLD] [START]
                for (jj = ua[jrow] + 1; jj <= ia[jrow + 1] - 1; jj++) {
                    jw = iw[ja[jj]];
                    if (jw != -1) {
                        l[jw] = l[jw] - tl * l[jj];

                    }
                }
           [OLD] [END] */

/*
            // First modification
            for (jj = ua[jrow] + 1; jj < ia[jrow + 1]; jj++) {
                jw = iw[ja[jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[jj];

                }
            }*/

/*
            // Second modification
            K = ia[jrow + 1] - ua[jrow] - 1;
            for (jj = 0; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }*/

/*
            // First optimization
            K = ia[jrow + 1] - ua[jrow] - 1;
            for (jj = 0; jj < K/4; jj++) {

                jw = iw[ja[ua[jrow] + 1 + 4 * jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + 4 * jj];

                }

                jw = iw[ja[ua[jrow] + 1 + 4 * jj + 1]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + 4 * jj + 1];

                }

                jw = iw[ja[ua[jrow] + 1 + 4 * jj + 2]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + 4 * jj + 2];

                }

                jw = iw[ja[ua[jrow] + 1 + 4 * jj + 3]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + 4 * jj + 3];

                }
            }

            // Process remainder
            for (jj = (K/4) * 4; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }*/

/*
            // Second optimization
            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;
            for (jj = 0; jj < K/4; jj++) {

                jw = iw[ja[ua[jrow] + 1 + m]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + m];

                }

                jw = iw[ja[ua[jrow] + 1 + m + 1]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + m + 1];

                }

                jw = iw[ja[ua[jrow] + 1 + m + 2]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + m + 2];

                }

                jw = iw[ja[ua[jrow] + 1 + m + 3]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + m + 3];

                }

                m += 4;
            }

            // Process remainder
            for (jj = (K/4) * 4; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }*/


            // Third optimization (If clauses are not exclusive)
            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;
            int32_t jw0;
            int32_t z = iw[ja[ua[jrow]]];
            for (jj = 0; jj < K/4; jj++) {

/*                jw = iw[ja[ua[jrow] + m + 1]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 1];

                }

                jw = iw[ja[ua[jrow] + m + 2]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 2];

                }

                jw = iw[ja[ua[jrow] + m + 3]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 3];

                }

                jw = iw[ja[ua[jrow] +  m + 4]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + m + 4];

                }*/


/*
                if (jw != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0 + 1 != -1) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 + 2 != -1) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0 + 3 != -1) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }*/

                jw0 = z + m + 1;
                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0 != -2) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 != -3) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0 != -4) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }

                m += 4;
            }

            // Process remainder
            for (jj = (K/4) * 4; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }


            // ____________________________________________________________
            // ____________________________________________________________

            j = j + 1;
            } while (j <= ia[i + 1] - 1);

            ua[i] = j;

            if (jrow != i) {
                printf("\n");
                printf("ILU_CSR_VI_ILP - Fatal error!\n");
                printf("  JROW != I\n");
                printf("  JROW = %d\n", jrow);
                printf("  I    = %d\n", i);
                exit(1);
            }

            if (l[j] == 0.0) {
                fprintf(stderr, "\n");
                fprintf(stderr, "ILU_CSR_VI_ILP - Fatal error!\n");
                fprintf(stderr, "  Zero pivot on step I = \n", i);
                fprintf(stderr, "  L[%d] = 0.0\n", j);
                exit(1);
            }

            l[j] = 1.0 / l[j];
        }

        /* [OLD] [START]
        for (k = 0; k < n; k++) {
            l[ua[k]] = 1.0 / l[ua[k]];
        }
           [OLD] [END] */

/*  First optimization
    for (k = 0; k < n/4; k++) {
        l[ua[4 * k]] = 1.0 / l[ua[4 * k]];
        l[ua[4 * k + 1]] = 1.0 / l[ua[4 * k + 1]];
        l[ua[4 * k + 2]] = 1.0 / l[ua[4 * k + 2]];
        l[ua[4 * k + 3]] = 1.0 / l[ua[4 * k + 3]];
    }

    // Process remainder
    for (k = (n/4) * 4; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }*/


    // Second optimization
    m = 0;
    for (k = 0; k < n/4; k++) {
        l[ua[m]] = 1.0 / l[ua[m]];
        l[ua[m + 1]] = 1.0 / l[ua[m + 1]];
        l[ua[m + 2]] = 1.0 / l[ua[m + 2]];
        l[ua[m + 3]] = 1.0 / l[ua[m + 3]];

        m += 4;
    }

    // Process remainder
    for (k = (n/4) * 4; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }


/*
  Free memory.
*/
    free(iw);

    return;
}




/*
 * [FastCode] Incomplete LU decomposition (unroll factor: 8)
 *
 * For documentation see 'ilu_csrvi_ilp_unroll'
 */
void ilu_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
                            double l[]) {
    int32_t *iw;
    int32_t i;
    int32_t j;
    int32_t jj;
    int32_t jrow;
    int32_t jw;
    int32_t k;
    double tl;

    iw = (int32_t *) malloc(n * sizeof(int32_t));
/*
  Copy A.
*/

    int m = 0;
    for (k = 0; k < nz_num/8; k++) {
        l[m] = vala[va[m]];
        l[m + 1] = vala[va[m + 1]];
        l[m + 2] = vala[va[m + 2]];
        l[m + 3] = vala[va[m + 3]];
        l[m + 4] = vala[va[m + 4]];
        l[m + 5] = vala[va[m + 5]];
        l[m + 6] = vala[va[m + 6]];
        l[m + 7] = vala[va[m + 7]];

        m += 8;
    }

    // Process remainder
    for (k = (nz_num/8) * 8; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }

    for (i = 0; i < n; i++) {
/*
  IW points to the nonzero entries in row I.
*/

        // Second optimization
        m = 0;
        for (j = 0; j < n/8; j++) {
            iw[m] = -1;
            iw[m + 1] = -1;
            iw[m + 2] = -1;
            iw[m + 3] = -1;
            iw[m + 4] = -1;
            iw[m + 5] = -1;
            iw[m + 6] = -1;
            iw[m + 7] = -1;

            m += 8;
        }

        // Process remainder
        for (j = (n/8) * 8; j < n; j++) {
            iw[j] = -1;
        }

        m = 0;
        int K = ia[i + 1] - ia[i];
        for (k = 0; k < K/8; k++) {
            iw[ja[ia[i] + m]] = ia[i] + m;
            iw[ja[ia[i] + m + 1]] = ia[i] + m + 1;
            iw[ja[ia[i] + m + 2]] = ia[i] + m + 2;
            iw[ja[ia[i] + m + 3]] = ia[i] + m + 3;
            iw[ja[ia[i] + m + 4]] = ia[i] + m + 4;
            iw[ja[ia[i] + m + 5]] = ia[i] + m + 5;
            iw[ja[ia[i] + m + 6]] = ia[i] + m + 6;
            iw[ja[ia[i] + m + 7]] = ia[i] + m + 7;

            m += 8;
        }

        // Process remainder
        for (k = (K/8) * 8; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }

            tl = l[j] * l[ua[jrow]];
            l[j] = tl;

            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;

            int32_t jw0;
            int32_t z = iw[ja[ua[jrow]]];
            for (jj = 0; jj < K/8; jj++) {
                jw0 = z + m + 1;

                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0 != -2) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 != -3) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0 != -4) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }


                if (jw0 != -5) {
                    l[jw0 + 4] = l[jw0 + 4] - tl * l[ua[jrow] + m + 5];

                }

                if (jw0 != -6) {
                    l[jw0 + 5] = l[jw0 + 5] - tl * l[ua[jrow] + m + 6];

                }

                if (jw0 != -7) {
                    l[jw0 + 6] = l[jw0 + 6] - tl * l[ua[jrow] + m + 7];

                }

                if (jw0 != -8) {
                    l[jw0 + 7] = l[jw0 + 7] - tl * l[ua[jrow] + m + 8];

                }

                m += 8;
            }

            // Process remainder
            for (jj = (K/8) * 8; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }

            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CSR_VI_ILP - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CSR_VI_ILP - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = \n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
    }

    m = 0;
    for (k = 0; k < n/8; k++) {
        l[ua[m]] = 1.0 / l[ua[m]];
        l[ua[m + 1]] = 1.0 / l[ua[m + 1]];
        l[ua[m + 2]] = 1.0 / l[ua[m + 2]];
        l[ua[m + 3]] = 1.0 / l[ua[m + 3]];
        l[ua[m + 4]] = 1.0 / l[ua[m + 4]];
        l[ua[m + 5]] = 1.0 / l[ua[m + 5]];
        l[ua[m + 6]] = 1.0 / l[ua[m + 6]];
        l[ua[m + 7]] = 1.0 / l[ua[m + 7]];

        m += 8;
    }

    // Process remainder
    for (k = (n/8) * 8; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }


/*
  Free memory.
*/
    free(iw);

    return;
}


/*
 * [FastCode] Incomplete LU decomposition (unroll factor: 16)
 *
 * For documentation see 'ilu_csrvi_ilp_unroll'
 */
void ilu_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
                            double l[]) {
    int32_t *iw;
    int32_t i;
    int32_t j;
    int32_t jj;
    int32_t jrow;
    int32_t jw;
    int32_t k;
    double tl;

    iw = (int32_t *) malloc(n * sizeof(int32_t));
/*
  Copy A.
*/

    int m = 0;
    for (k = 0; k < nz_num / 16; k++) {
        l[m] = vala[va[m]];
        l[m + 1] = vala[va[m + 1]];
        l[m + 2] = vala[va[m + 2]];
        l[m + 3] = vala[va[m + 3]];
        l[m + 4] = vala[va[m + 4]];
        l[m + 5] = vala[va[m + 5]];
        l[m + 6] = vala[va[m + 6]];
        l[m + 7] = vala[va[m + 7]];
        l[m + 8] = vala[va[m + 8]];
        l[m + 9] = vala[va[m + 9]];
        l[m + 10] = vala[va[m + 10]];
        l[m + 11] = vala[va[m + 11]];
        l[m + 12] = vala[va[m + 12]];
        l[m + 13] = vala[va[m + 13]];
        l[m + 14] = vala[va[m + 14]];
        l[m + 15] = vala[va[m + 15]];

        m += 16;
    }

    // Process remainder
    for (k = (nz_num / 16) * 16; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }

    for (i = 0; i < n; i++) {
/*
  IW points to the nonzero entries in row I.
*/

        // Second optimization
        m = 0;
        for (j = 0; j < n / 16; j++) {
            iw[m] = -1;
            iw[m + 1] = -1;
            iw[m + 2] = -1;
            iw[m + 3] = -1;
            iw[m + 4] = -1;
            iw[m + 5] = -1;
            iw[m + 6] = -1;
            iw[m + 7] = -1;
            iw[m + 8] = -1;
            iw[m + 9] = -1;
            iw[m + 10] = -1;
            iw[m + 11] = -1;
            iw[m + 12] = -1;
            iw[m + 13] = -1;
            iw[m + 14] = -1;
            iw[m + 15] = -1;

            m += 16;
        }

        // Process remainder
        for (j = (n / 16) * 16; j < n; j++) {
            iw[j] = -1;
        }

        m = 0;
        int K = ia[i + 1] - ia[i];
        for (k = 0; k < K / 16; k++) {
            iw[ja[ia[i] + m]] = ia[i] + m;
            iw[ja[ia[i] + m + 1]] = ia[i] + m + 1;
            iw[ja[ia[i] + m + 2]] = ia[i] + m + 2;
            iw[ja[ia[i] + m + 3]] = ia[i] + m + 3;
            iw[ja[ia[i] + m + 4]] = ia[i] + m + 4;
            iw[ja[ia[i] + m + 5]] = ia[i] + m + 5;
            iw[ja[ia[i] + m + 6]] = ia[i] + m + 6;
            iw[ja[ia[i] + m + 7]] = ia[i] + m + 7;
            iw[ja[ia[i] + m + 8]] = ia[i] + m + 8;
            iw[ja[ia[i] + m + 9]] = ia[i] + m + 9;
            iw[ja[ia[i] + m + 10]] = ia[i] + m + 10;
            iw[ja[ia[i] + m + 11]] = ia[i] + m + 11;
            iw[ja[ia[i] + m + 12]] = ia[i] + m + 12;
            iw[ja[ia[i] + m + 13]] = ia[i] + m + 13;
            iw[ja[ia[i] + m + 14]] = ia[i] + m + 14;
            iw[ja[ia[i] + m + 15]] = ia[i] + m + 15;

            m += 16;
        }

        // Process remainder
        for (k = (K / 16) * 16; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }

            tl = l[j] * l[ua[jrow]];
            l[j] = tl;

            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;
            int32_t jw0;
            int32_t z = iw[ja[ua[jrow]]];
            for (jj = 0; jj < K / 16; jj++) {

                jw0 = z + m + 1;

                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0 != -2) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 != -3) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0 != -4) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }


                if (jw0 != -5) {
                    l[jw0 + 4] = l[jw0 + 4] - tl * l[ua[jrow] + m + 5];

                }

                if (jw0 != -6) {
                    l[jw0 + 5] = l[jw0 + 5] - tl * l[ua[jrow] + m + 6];

                }

                if (jw0 != -7) {
                    l[jw0 + 6] = l[jw0 + 6] - tl * l[ua[jrow] + m + 7];

                }

                if (jw0 != -8) {
                    l[jw0 + 7] = l[jw0 + 7] - tl * l[ua[jrow] + m + 8];

                }

                if (jw0 != -9) {
                    l[jw0 + 8] = l[jw0 + 8] - tl * l[ua[jrow] + m + 9];

                }

                if (jw0 != -10) {
                    l[jw0 + 9] = l[jw0 + 9] - tl * l[ua[jrow] + m + 10];

                }

                if (jw0 != -11) {
                    l[jw0 + 10] = l[jw0 + 10] - tl * l[ua[jrow] + m + 11];

                }

                if (jw0 != -12) {
                    l[jw0 + 11] = l[jw0 + 11] - tl * l[ua[jrow] + m + 12];

                }


                if (jw0 != -13) {
                    l[jw0 + 12] = l[jw0 + 12] - tl * l[ua[jrow] + m + 13];

                }

                if (jw0 != -14) {
                    l[jw0 + 13] = l[jw0 + 13] - tl * l[ua[jrow] + m + 14];

                }

                if (jw0 != -15) {
                    l[jw0 + 14] = l[jw0 + 14] - tl * l[ua[jrow] + m + 15];

                }

                if (jw0 != -16) {
                    l[jw0 + 15] = l[jw0 + 15] - tl * l[ua[jrow] + m + 16];

                }

                m += 16;
            }

            // Process remainder
            for (jj = (K / 16) * 16; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }

            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CSR_VI_ILP - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CSR_VI_ILP - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = \n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
    }

    m = 0;
    for (k = 0; k < n / 16; k++) {
        l[ua[m]] = 1.0 / l[ua[m]];
        l[ua[m + 1]] = 1.0 / l[ua[m + 1]];
        l[ua[m + 2]] = 1.0 / l[ua[m + 2]];
        l[ua[m + 3]] = 1.0 / l[ua[m + 3]];
        l[ua[m + 4]] = 1.0 / l[ua[m + 4]];
        l[ua[m + 5]] = 1.0 / l[ua[m + 5]];
        l[ua[m + 6]] = 1.0 / l[ua[m + 6]];
        l[ua[m + 7]] = 1.0 / l[ua[m + 7]];
        l[ua[m + 8]] = 1.0 / l[ua[m + 8]];
        l[ua[m + 9]] = 1.0 / l[ua[m + 9]];
        l[ua[m + 10]] = 1.0 / l[ua[m + 10]];
        l[ua[m + 11]] = 1.0 / l[ua[m + 11]];
        l[ua[m + 12]] = 1.0 / l[ua[m + 12]];
        l[ua[m + 13]] = 1.0 / l[ua[m + 13]];
        l[ua[m + 14]] = 1.0 / l[ua[m + 14]];
        l[ua[m + 15]] = 1.0 / l[ua[m + 15]];

        m += 16;
    }

    // Process remainder
    for (k = (n / 16) * 16; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }


/*
  Free memory.
*/
    free(iw);

    return;
}


/*
 * [FastCode] Incomplete LU decomposition (unroll factor: 32)
 *
 * For documentation see 'ilu_csrvi_ilp_unroll'
 */
void ilu_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[], int32_t ua[],
                             double l[]) {
    int32_t *iw;
    int32_t i;
    int32_t j;
    int32_t jj;
    int32_t jrow;
    int32_t jw;
    int32_t k;
    double tl;

    iw = (int32_t *) malloc(n * sizeof(int32_t));
/*
  Copy A.
*/

    int m = 0;
    for (k = 0; k < nz_num / 32; k++) {
        l[m] = vala[va[m]];
        l[m + 1] = vala[va[m + 1]];
        l[m + 2] = vala[va[m + 2]];
        l[m + 3] = vala[va[m + 3]];
        l[m + 4] = vala[va[m + 4]];
        l[m + 5] = vala[va[m + 5]];
        l[m + 6] = vala[va[m + 6]];
        l[m + 7] = vala[va[m + 7]];
        l[m + 8] = vala[va[m + 8]];
        l[m + 9] = vala[va[m + 9]];
        l[m + 10] = vala[va[m + 10]];
        l[m + 11] = vala[va[m + 11]];
        l[m + 12] = vala[va[m + 12]];
        l[m + 13] = vala[va[m + 13]];
        l[m + 14] = vala[va[m + 14]];
        l[m + 15] = vala[va[m + 15]];
        l[m + 16] = vala[va[m + 16]];
        l[m + 17] = vala[va[m + 17]];
        l[m + 18] = vala[va[m + 18]];
        l[m + 19] = vala[va[m + 19]];
        l[m + 20] = vala[va[m + 20]];
        l[m + 21] = vala[va[m + 21]];
        l[m + 22] = vala[va[m + 22]];
        l[m + 23] = vala[va[m + 23]];
        l[m + 24] = vala[va[m + 24]];
        l[m + 25] = vala[va[m + 25]];
        l[m + 26] = vala[va[m + 26]];
        l[m + 27] = vala[va[m + 27]];
        l[m + 28] = vala[va[m + 28]];
        l[m + 29] = vala[va[m + 29]];
        l[m + 30] = vala[va[m + 30]];
        l[m + 31] = vala[va[m + 31]];

        m += 32;
    }

    // Process remainder
    for (k = (nz_num / 32) * 32; k < nz_num; k++) {
        l[k] = vala[va[k]];
    }

    for (i = 0; i < n; i++) {
/*
  IW points to the nonzero entries in row I.
*/

        // Second optimization
        m = 0;
        for (j = 0; j < n / 32; j++) {
            iw[m] = -1;
            iw[m + 1] = -1;
            iw[m + 2] = -1;
            iw[m + 3] = -1;
            iw[m + 4] = -1;
            iw[m + 5] = -1;
            iw[m + 6] = -1;
            iw[m + 7] = -1;
            iw[m + 8] = -1;
            iw[m + 9] = -1;
            iw[m + 10] = -1;
            iw[m + 11] = -1;
            iw[m + 12] = -1;
            iw[m + 13] = -1;
            iw[m + 14] = -1;
            iw[m + 15] = -1;
            iw[m + 16] = -1;
            iw[m + 17] = -1;
            iw[m + 18] = -1;
            iw[m + 19] = -1;
            iw[m + 20] = -1;
            iw[m + 21] = -1;
            iw[m + 22] = -1;
            iw[m + 23] = -1;
            iw[m + 24] = -1;
            iw[m + 25] = -1;
            iw[m + 26] = -1;
            iw[m + 27] = -1;
            iw[m + 28] = -1;
            iw[m + 29] = -1;
            iw[m + 30] = -1;
            iw[m + 31] = -1;

            m += 32;
        }

        // Process remainder
        for (j = (n / 32) * 32; j < n; j++) {
            iw[j] = -1;
        }

        m = 0;
        int K = ia[i + 1] - ia[i];
        for (k = 0; k < K / 32; k++) {
            iw[ja[ia[i] + m]] = ia[i] + m;
            iw[ja[ia[i] + m + 1]] = ia[i] + m + 1;
            iw[ja[ia[i] + m + 2]] = ia[i] + m + 2;
            iw[ja[ia[i] + m + 3]] = ia[i] + m + 3;
            iw[ja[ia[i] + m + 4]] = ia[i] + m + 4;
            iw[ja[ia[i] + m + 5]] = ia[i] + m + 5;
            iw[ja[ia[i] + m + 6]] = ia[i] + m + 6;
            iw[ja[ia[i] + m + 7]] = ia[i] + m + 7;
            iw[ja[ia[i] + m + 8]] = ia[i] + m + 8;
            iw[ja[ia[i] + m + 9]] = ia[i] + m + 9;
            iw[ja[ia[i] + m + 10]] = ia[i] + m + 10;
            iw[ja[ia[i] + m + 11]] = ia[i] + m + 11;
            iw[ja[ia[i] + m + 12]] = ia[i] + m + 12;
            iw[ja[ia[i] + m + 13]] = ia[i] + m + 13;
            iw[ja[ia[i] + m + 14]] = ia[i] + m + 14;
            iw[ja[ia[i] + m + 15]] = ia[i] + m + 15;
            iw[ja[ia[i] + m + 16]] = ia[i] + m + 16;
            iw[ja[ia[i] + m + 17]] = ia[i] + m + 17;
            iw[ja[ia[i] + m + 18]] = ia[i] + m + 18;
            iw[ja[ia[i] + m + 19]] = ia[i] + m + 19;
            iw[ja[ia[i] + m + 20]] = ia[i] + m + 20;
            iw[ja[ia[i] + m + 21]] = ia[i] + m + 21;
            iw[ja[ia[i] + m + 22]] = ia[i] + m + 22;
            iw[ja[ia[i] + m + 23]] = ia[i] + m + 23;
            iw[ja[ia[i] + m + 24]] = ia[i] + m + 24;
            iw[ja[ia[i] + m + 25]] = ia[i] + m + 25;
            iw[ja[ia[i] + m + 26]] = ia[i] + m + 26;
            iw[ja[ia[i] + m + 27]] = ia[i] + m + 27;
            iw[ja[ia[i] + m + 28]] = ia[i] + m + 28;
            iw[ja[ia[i] + m + 29]] = ia[i] + m + 29;
            iw[ja[ia[i] + m + 30]] = ia[i] + m + 30;
            iw[ja[ia[i] + m + 31]] = ia[i] + m + 31;

            m += 32;
        }

        // Process remainder
        for (k = (K / 32) * 32; k < K; k++) {
            iw[ja[ia[i] + k]] = ia[i] + k;
        }

        j = ia[i];
        do {
            jrow = ja[j];
            if (i <= jrow) {
                break;
            }

            tl = l[j] * l[ua[jrow]];
            l[j] = tl;

            m = 0;
            K = ia[jrow + 1] - ua[jrow] - 1;
            int32_t jw0;
            int32_t z = iw[ja[ua[jrow]]];
            for (jj = 0; jj < K / 32; jj++) {

                jw0 = z + m + 1;

                if (jw0 != -1) {
                    l[jw0] = l[jw0] - tl * l[ua[jrow] + m + 1];

                }

                if (jw0 != -2) {
                    l[jw0 + 1] = l[jw0 + 1] - tl * l[ua[jrow] + m + 2];

                }

                if (jw0 != -3) {
                    l[jw0 + 2] = l[jw0 + 2] - tl * l[ua[jrow] + m + 3];

                }

                if (jw0 != -4) {
                    l[jw0 + 3] = l[jw0 + 3] - tl * l[ua[jrow] + m + 4];

                }


                if (jw0 != -5) {
                    l[jw0 + 4] = l[jw0 + 4] - tl * l[ua[jrow] + m + 5];

                }

                if (jw0 != -6) {
                    l[jw0 + 5] = l[jw0 + 5] - tl * l[ua[jrow] + m + 6];

                }

                if (jw0 != -7) {
                    l[jw0 + 6] = l[jw0 + 6] - tl * l[ua[jrow] + m + 7];

                }

                if (jw0 != -8) {
                    l[jw0 + 7] = l[jw0 + 7] - tl * l[ua[jrow] + m + 8];

                }

                if (jw0 != -9) {
                    l[jw0 + 8] = l[jw0 + 8] - tl * l[ua[jrow] + m + 9];

                }

                if (jw0 != -10) {
                    l[jw0 + 9] = l[jw0 + 9] - tl * l[ua[jrow] + m + 10];

                }

                if (jw0 != -11) {
                    l[jw0 + 10] = l[jw0 + 10] - tl * l[ua[jrow] + m + 11];

                }

                if (jw0 != -12) {
                    l[jw0 + 11] = l[jw0 + 11] - tl * l[ua[jrow] + m + 12];

                }


                if (jw0 != -13) {
                    l[jw0 + 12] = l[jw0 + 12] - tl * l[ua[jrow] + m + 13];

                }

                if (jw0 != -14) {
                    l[jw0 + 13] = l[jw0 + 13] - tl * l[ua[jrow] + m + 14];

                }

                if (jw0 != -15) {
                    l[jw0 + 14] = l[jw0 + 14] - tl * l[ua[jrow] + m + 15];

                }

                if (jw0 != -16) {
                    l[jw0 + 15] = l[jw0 + 15] - tl * l[ua[jrow] + m + 16];

                }




                if (jw0 != -17) {
                    l[jw0 + 16] = l[jw0 + 16] - tl * l[ua[jrow] + m + 17];

                }

                if (jw0 != -18) {
                    l[jw0 + 17] = l[jw0 + 17] - tl * l[ua[jrow] + m + 18];

                }

                if (jw0 != -19) {
                    l[jw0 + 18] = l[jw0 + 18] - tl * l[ua[jrow] + m + 19];

                }

                if (jw0 != -20) {
                    l[jw0 + 19] = l[jw0 + 19] - tl * l[ua[jrow] + m + 20];

                }


                if (jw0 != -21) {
                    l[jw0 + 20] = l[jw0 + 20] - tl * l[ua[jrow] + m + 21];

                }

                if (jw0 != -22) {
                    l[jw0 + 21] = l[jw0 + 21] - tl * l[ua[jrow] + m + 22];

                }

                if (jw0 != -23) {
                    l[jw0 + 22] = l[jw0 + 22] - tl * l[ua[jrow] + m + 23];

                }

                if (jw0 != -24) {
                    l[jw0 + 23] = l[jw0 + 23] - tl * l[ua[jrow] + m + 24];

                }

                if (jw0 != -25) {
                    l[jw0 + 24] = l[jw0 + 24] - tl * l[ua[jrow] + m + 25];

                }

                if (jw0 != -26) {
                    l[jw0 + 25] = l[jw0 + 25] - tl * l[ua[jrow] + m + 26];

                }

                if (jw0 != -27) {
                    l[jw0 + 26] = l[jw0 + 26] - tl * l[ua[jrow] + m + 27];

                }

                if (jw0 != -28) {
                    l[jw0 + 27] = l[jw0 + 27] - tl * l[ua[jrow] + m + 28];

                }


                if (jw0 != -29) {
                    l[jw0 + 28] = l[jw0 + 28] - tl * l[ua[jrow] + m + 29];

                }

                if (jw0 != -30) {
                    l[jw0 + 29] = l[jw0 + 29] - tl * l[ua[jrow] + m + 30];

                }

                if (jw0 != -31) {
                    l[jw0 + 30] = l[jw0 + 30] - tl * l[ua[jrow] + m + 31];

                }

                if (jw0 != -32) {
                    l[jw0 + 31] = l[jw0 + 31] - tl * l[ua[jrow] + m + 32];

                }


                m += 32;
            }

            // Process remainder
            for (jj = (K / 32) * 32; jj < K; jj++) {
                jw = iw[ja[ua[jrow] + 1 + jj]];
                if (jw != -1) {
                    l[jw] = l[jw] - tl * l[ua[jrow] + 1 + jj];

                }
            }

            j = j + 1;
        } while (j <= ia[i + 1] - 1);

        ua[i] = j;

        if (jrow != i) {
            printf("\n");
            printf("ILU_CSR_VI_ILP - Fatal error!\n");
            printf("  JROW != I\n");
            printf("  JROW = %d\n", jrow);
            printf("  I    = %d\n", i);
            exit(1);
        }

        if (l[j] == 0.0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "ILU_CSR_VI_ILP - Fatal error!\n");
            fprintf(stderr, "  Zero pivot on step I = \n", i);
            fprintf(stderr, "  L[%d] = 0.0\n", j);
            exit(1);
        }

        l[j] = 1.0 / l[j];
    }

    m = 0;
    for (k = 0; k < n / 32; k++) {
        l[ua[m]] = 1.0 / l[ua[m]];
        l[ua[m + 1]] = 1.0 / l[ua[m + 1]];
        l[ua[m + 2]] = 1.0 / l[ua[m + 2]];
        l[ua[m + 3]] = 1.0 / l[ua[m + 3]];
        l[ua[m + 4]] = 1.0 / l[ua[m + 4]];
        l[ua[m + 5]] = 1.0 / l[ua[m + 5]];
        l[ua[m + 6]] = 1.0 / l[ua[m + 6]];
        l[ua[m + 7]] = 1.0 / l[ua[m + 7]];
        l[ua[m + 8]] = 1.0 / l[ua[m + 8]];
        l[ua[m + 9]] = 1.0 / l[ua[m + 9]];
        l[ua[m + 10]] = 1.0 / l[ua[m + 10]];
        l[ua[m + 11]] = 1.0 / l[ua[m + 11]];
        l[ua[m + 12]] = 1.0 / l[ua[m + 12]];
        l[ua[m + 13]] = 1.0 / l[ua[m + 13]];
        l[ua[m + 14]] = 1.0 / l[ua[m + 14]];
        l[ua[m + 15]] = 1.0 / l[ua[m + 15]];
        l[ua[m + 16]] = 1.0 / l[ua[m + 16]];
        l[ua[m + 17]] = 1.0 / l[ua[m + 17]];
        l[ua[m + 18]] = 1.0 / l[ua[m + 18]];
        l[ua[m + 19]] = 1.0 / l[ua[m + 19]];
        l[ua[m + 20]] = 1.0 / l[ua[m + 20]];
        l[ua[m + 21]] = 1.0 / l[ua[m + 21]];
        l[ua[m + 22]] = 1.0 / l[ua[m + 22]];
        l[ua[m + 23]] = 1.0 / l[ua[m + 23]];
        l[ua[m + 24]] = 1.0 / l[ua[m + 24]];
        l[ua[m + 25]] = 1.0 / l[ua[m + 25]];
        l[ua[m + 26]] = 1.0 / l[ua[m + 26]];
        l[ua[m + 27]] = 1.0 / l[ua[m + 27]];
        l[ua[m + 28]] = 1.0 / l[ua[m + 28]];
        l[ua[m + 29]] = 1.0 / l[ua[m + 29]];
        l[ua[m + 30]] = 1.0 / l[ua[m + 30]];
        l[ua[m + 31]] = 1.0 / l[ua[m + 31]];

        m += 32;
    }

    // Process remainder
    for (k = (n / 32) * 32; k < n; k++) {
        l[ua[k]] = 1.0 / l[ua[k]];
    }


/*
  Free memory.
*/
    free(iw);

    return;
}


/* [FastCode] LUS_CR applies the incomplete LU preconditioner.
              The linear system M * Z = R is solved for Z.  M is the incomplete
              LU preconditioner matrix, and R is a vector supplied by the user.
              So essentially, we're solving L * U * Z = R.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double L[NZ_NUM], the matrix values.

    Input, int32_t UA[N], the index of the diagonal element of each row.

    Input, double R[N], the right hand side.

    Output, double Z[N], the solution of the system M * Z = R.
*/
void lus_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                          double *r, double *z) {

    // TODO: Add some heuristics
    switch(lus_unroll_factor) {
        case 1:
            lus_csrvi(n, nz_num, ia, ja, l, ua, r, z);
            break;
        case 4:
            lus_csrvi_ilp_unroll_4(n, nz_num, ia, ja, l, ua, r, z);
            break;
        case 8:
            lus_csrvi_ilp_unroll_8(n, nz_num, ia, ja, l, ua, r, z);
            break;
        case 16:
            lus_csrvi_ilp_unroll_16(n, nz_num, ia, ja, l, ua, r, z);
            break;
        case 32:
            lus_csrvi_ilp_unroll_32(n, nz_num, ia, ja, l, ua, r, z);
            break;
        default:
            lus_csrvi(n, nz_num, ia, ja, l, ua, r, z);
            break;
    }
}


/*
 * [FastCode] LU application (unroll factor: 4)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void lus_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                            double *r, double *z) {
    int32_t i;
    int32_t j;
    double *w;

    w = (double *) malloc(n * sizeof(double));
/*
  Copy R in.
*/

    /* [OLD] [START]
    for (i = 0; i < n; i++) {
        w[i] = r[i];
    }
       [OLD] [END] */

    // First optimization
//    for (i = 0; i < n/4; i++) {
//        w[4*i] = r[4*i];
//        w[4*i + 1] = r[4*i + 1];
//        w[4*i + 2] = r[4*i + 2];
//        w[4*i + 3] = r[4*i + 3];
//    }
//
//    // Process remainder
//    for (i = (n/4) * 4; i < n; i++) {
//        w[i] = r[i];
//    }

    // Second optimization
    int32_t m = 0;
    for (i = 0; i < n/4; i++) {
        w[m] = r[m];
        w[m + 1] = r[m + 1];
        w[m + 2] = r[m + 2];
        w[m + 3] = r[m + 3];

        m += 4;
    }

    // Process remainder
    for (i = (n/4) * 4; i < n; i++) {
        w[i] = r[i];
    }
/*
  Solve L * w = w where L is unit lower triangular.
*/

    /* [OLD] [START]
    for (i = 1; i < n; i++) {
        for (j = ia[i]; j < ua[i]; j++) {
            w[i] = w[i] - l[j] * w[ja[j]];
        }
    }
       [OLD] [END] */

    // Unroll inner loop
    for (i = 1; i < n; i++) {

        /* [OLD] [START]
            for (j = ia[i]; j < ua[i]; j++) {
                w[i] = w[i] - l[j] * w[ja[j]];
            }
       [OLD] [END] */

        // First modification
//        for (j = 0; j < (ua[i] - ia[i]); j++) {
//            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
//        }

        int32_t K = ua[i] - ia[i];

        // First optimization
//        for (j = 0; j < K/4; j++) {
//            w[i] = w[i] - l[ia[i] + 4 * j] * w[ja[ia[i] + 4 * j]];
//            w[i] = w[i] - l[ia[i] + 4 * j + 1] * w[ja[ia[i] + 4 * j + 1]];
//            w[i] = w[i] - l[ia[i] + 4 * j + 2] * w[ja[ia[i] + 4 * j + 2]];
//            w[i] = w[i] - l[ia[i] + 4 * j + 3] * w[ja[ia[i] + 4 * j + 3]];
//        }

        // Process remainder
//        for (j = (K/4) * 4; j < K; j++) {
//            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
//
//        }

        // Second optimization
        m = 0;
        for (j = 0; j < K/4; j++) {
            w[i] = w[i] - l[ia[i] + m] * w[ja[ia[i] + m]];
            w[i] = w[i] - l[ia[i] + m + 1] * w[ja[ia[i] + m + 1]];
            w[i] = w[i] - l[ia[i] + m + 2] * w[ja[ia[i] + m + 2]];
            w[i] = w[i] - l[ia[i] + m + 3] * w[ja[ia[i] + m + 3]];

            m += 4;
        }

        // Process remainder
        for (j = (K/4) * 4; j < K; j++) {
            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
        }

    }

    /*
      Solve U * w = w, where U is upper triangular.
    */
    for (i = n - 1; 0 <= i; i--) {

        /* [OLD] [START]
            for (j = ua[i] + 1; j < ia[i + 1]; j++) {
                w[i] = w[i] - l[j] * w[ja[j]];
            }
            w[i] = w[i] / l[ua[i]];
       [OLD] [END] */


        int32_t K = ia[i + 1] - ua[i] - 1;
        // First modification
//        for (j = 0; j < K; j++) {
//            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
//        }

        // Optimization (steps similar to last optimizations in this method)

        m = 0;
        for (j = 0; j < K/4; j++) {
            w[i] = w[i] - l[ua[i] + m + 1] * w[ja[ua[i] + m + 1]];
            w[i] = w[i] - l[ua[i] + m + 2] * w[ja[ua[i] + m + 2]];
            w[i] = w[i] - l[ua[i] + m + 3] * w[ja[ua[i] + m + 3]];
            w[i] = w[i] - l[ua[i] + m + 4] * w[ja[ua[i] + m + 4]];

            m += 4;
        }

        // Process remainder
        for (j = (K/4) * 4; j < K; j++) {
            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
        }


        w[i] = w[i] / l[ua[i]];

    }
    /*
      Copy Z out.
    */

    /* [OLD] [START]
    for (i = 0; i < n; i++) {
        z[i] = w[i];
    }
       [OLD] [END] */

    // First optimization
    m = 0;
    for (i = 0; i < n/4; i++) {
        z[m] = w[m];
        z[m + 1] = w[m + 1];
        z[m + 2] = w[m + 2];
        z[m + 3] = w[m + 3];

        m += 4;
    }

    // Process remainder
    for (i = (n/4) * 4; i < n; i++) {
        z[i] = w[i];
    }

/*
  Free memory.
*/
    free(w);

    return;
}


/*
 * [FastCode] LU application (unroll factor: 8)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void lus_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                            double *r, double *z) {
    int32_t i;
    int32_t j;
    double *w;

    w = (double *) malloc(n * sizeof(double));
/*
  Copy R in.
*/

    // Second optimization
    int32_t m = 0;
    for (i = 0; i < n/8; i++) {
        w[m] = r[m];
        w[m + 1] = r[m + 1];
        w[m + 2] = r[m + 2];
        w[m + 3] = r[m + 3];
        w[m + 4] = r[m + 4];
        w[m + 5] = r[m + 5];
        w[m + 6] = r[m + 6];
        w[m + 7] = r[m + 7];

        m += 8;
    }

    // Process remainder
    for (i = (n/8) * 8; i < n; i++) {
        w[i] = r[i];
    }
/*
  Solve L * w = w where L is unit lower triangular.
*/

    // Unroll inner loop
    for (i = 1; i < n; i++) {

        int32_t K = ua[i] - ia[i];

        // Second optimization
        m = 0;
        for (j = 0; j < K/8; j++) {
            w[i] = w[i] - l[ia[i] + m] * w[ja[ia[i] + m]];
            w[i] = w[i] - l[ia[i] + m + 1] * w[ja[ia[i] + m + 1]];
            w[i] = w[i] - l[ia[i] + m + 2] * w[ja[ia[i] + m + 2]];
            w[i] = w[i] - l[ia[i] + m + 3] * w[ja[ia[i] + m + 3]];
            w[i] = w[i] - l[ia[i] + m + 4] * w[ja[ia[i] + m + 4]];
            w[i] = w[i] - l[ia[i] + m + 5] * w[ja[ia[i] + m + 5]];
            w[i] = w[i] - l[ia[i] + m + 6] * w[ja[ia[i] + m + 6]];
            w[i] = w[i] - l[ia[i] + m + 7] * w[ja[ia[i] + m + 7]];

            m += 8;
        }

        // Process remainder
        for (j = (K/8) * 8; j < K; j++) {
            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
        }

    }

    /*
      Solve U * w = w, where U is upper triangular.
    */
    for (i = n - 1; 0 <= i; i--) {

        int32_t K = ia[i + 1] - ua[i] - 1;

        // Optimization (steps similar to last optimizations in this method)
        m = 0;
        for (j = 0; j < K/8; j++) {
            w[i] = w[i] - l[ua[i] + m + 1] * w[ja[ua[i] + m + 1]];
            w[i] = w[i] - l[ua[i] + m + 2] * w[ja[ua[i] + m + 2]];
            w[i] = w[i] - l[ua[i] + m + 3] * w[ja[ua[i] + m + 3]];
            w[i] = w[i] - l[ua[i] + m + 4] * w[ja[ua[i] + m + 4]];
            w[i] = w[i] - l[ua[i] + m + 5] * w[ja[ua[i] + m + 5]];
            w[i] = w[i] - l[ua[i] + m + 6] * w[ja[ua[i] + m + 6]];
            w[i] = w[i] - l[ua[i] + m + 7] * w[ja[ua[i] + m + 7]];
            w[i] = w[i] - l[ua[i] + m + 8] * w[ja[ua[i] + m + 8]];

            m += 8;
        }

        // Process remainder
        for (j = (K/8) * 8; j < K; j++) {
            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
        }

        w[i] = w[i] / l[ua[i]];

    }
    /*
      Copy Z out.
    */

    // First optimization
    m = 0;
    for (i = 0; i < n/8; i++) {
        z[m] = w[m];
        z[m + 1] = w[m + 1];
        z[m + 2] = w[m + 2];
        z[m + 3] = w[m + 3];
        z[m + 4] = w[m + 4];
        z[m + 5] = w[m + 5];
        z[m + 6] = w[m + 6];
        z[m + 7] = w[m + 7];

        m += 8;
    }

    // Process remainder
    for (i = (n/8) * 8; i < n; i++) {
        z[i] = w[i];
    }

/*
  Free memory.
*/
    free(w);

    return;
}


/*
 * [FastCode] LU application (unroll factor: 16)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void lus_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                            double *r, double *z) {
    int32_t i;
    int32_t j;
    double *w;

    w = (double *) malloc(n * sizeof(double));
/*
  Copy R in.
*/

    // Second optimization
    int32_t m = 0;
    for (i = 0; i < n/16; i++) {
        w[m] = r[m];
        w[m + 1] = r[m + 1];
        w[m + 2] = r[m + 2];
        w[m + 3] = r[m + 3];
        w[m + 4] = r[m + 4];
        w[m + 5] = r[m + 5];
        w[m + 6] = r[m + 6];
        w[m + 7] = r[m + 7];
        w[m + 8] = r[m + 8];
        w[m + 9] = r[m + 9];
        w[m + 10] = r[m + 10];
        w[m + 11] = r[m + 11];
        w[m + 12] = r[m + 12];
        w[m + 13] = r[m + 13];
        w[m + 14] = r[m + 14];
        w[m + 15] = r[m + 15];

        m += 16;
    }

    // Process remainder
    for (i = (n/16) * 16; i < n; i++) {
        w[i] = r[i];
    }
/*
  Solve L * w = w where L is unit lower triangular.
*/

    // Unroll inner loop
    for (i = 1; i < n; i++) {

        int32_t K = ua[i] - ia[i];

        // Second optimization
        m = 0;
        for (j = 0; j < K/16; j++) {
            w[i] = w[i] - l[ia[i] + m] * w[ja[ia[i] + m]];
            w[i] = w[i] - l[ia[i] + m + 1] * w[ja[ia[i] + m + 1]];
            w[i] = w[i] - l[ia[i] + m + 2] * w[ja[ia[i] + m + 2]];
            w[i] = w[i] - l[ia[i] + m + 3] * w[ja[ia[i] + m + 3]];
            w[i] = w[i] - l[ia[i] + m + 4] * w[ja[ia[i] + m + 4]];
            w[i] = w[i] - l[ia[i] + m + 5] * w[ja[ia[i] + m + 5]];
            w[i] = w[i] - l[ia[i] + m + 6] * w[ja[ia[i] + m + 6]];
            w[i] = w[i] - l[ia[i] + m + 7] * w[ja[ia[i] + m + 7]];
            w[i] = w[i] - l[ia[i] + m + 8] * w[ja[ia[i] + m + 8]];
            w[i] = w[i] - l[ia[i] + m + 9] * w[ja[ia[i] + m + 9]];
            w[i] = w[i] - l[ia[i] + m + 10] * w[ja[ia[i] + m + 10]];
            w[i] = w[i] - l[ia[i] + m + 11] * w[ja[ia[i] + m + 11]];
            w[i] = w[i] - l[ia[i] + m + 12] * w[ja[ia[i] + m + 12]];
            w[i] = w[i] - l[ia[i] + m + 13] * w[ja[ia[i] + m + 13]];
            w[i] = w[i] - l[ia[i] + m + 14] * w[ja[ia[i] + m + 14]];
            w[i] = w[i] - l[ia[i] + m + 15] * w[ja[ia[i] + m + 15]];

            m += 16;
        }

        // Process remainder
        for (j = (K/16) * 16; j < K; j++) {
            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
        }

    }

    /*
      Solve U * w = w, where U is upper triangular.
    */
    for (i = n - 1; 0 <= i; i--) {

        int32_t K = ia[i + 1] - ua[i] - 1;

        // Optimization (steps similar to last optimizations in this method)
        m = 0;
        for (j = 0; j < K/16; j++) {
            w[i] = w[i] - l[ua[i] + m + 1] * w[ja[ua[i] + m + 1]];
            w[i] = w[i] - l[ua[i] + m + 2] * w[ja[ua[i] + m + 2]];
            w[i] = w[i] - l[ua[i] + m + 3] * w[ja[ua[i] + m + 3]];
            w[i] = w[i] - l[ua[i] + m + 4] * w[ja[ua[i] + m + 4]];
            w[i] = w[i] - l[ua[i] + m + 5] * w[ja[ua[i] + m + 5]];
            w[i] = w[i] - l[ua[i] + m + 6] * w[ja[ua[i] + m + 6]];
            w[i] = w[i] - l[ua[i] + m + 7] * w[ja[ua[i] + m + 7]];
            w[i] = w[i] - l[ua[i] + m + 8] * w[ja[ua[i] + m + 8]];
            w[i] = w[i] - l[ua[i] + m + 9] * w[ja[ua[i] + m + 9]];
            w[i] = w[i] - l[ua[i] + m + 10] * w[ja[ua[i] + m + 10]];
            w[i] = w[i] - l[ua[i] + m + 11] * w[ja[ua[i] + m + 11]];
            w[i] = w[i] - l[ua[i] + m + 12] * w[ja[ua[i] + m + 12]];
            w[i] = w[i] - l[ua[i] + m + 13] * w[ja[ua[i] + m + 13]];
            w[i] = w[i] - l[ua[i] + m + 14] * w[ja[ua[i] + m + 14]];
            w[i] = w[i] - l[ua[i] + m + 15] * w[ja[ua[i] + m + 15]];
            w[i] = w[i] - l[ua[i] + m + 16] * w[ja[ua[i] + m + 16]];

            m += 16;
        }

        // Process remainder
        for (j = (K/16) * 16; j < K; j++) {
            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
        }

        w[i] = w[i] / l[ua[i]];

    }
    /*
      Copy Z out.
    */

    // First optimization
    m = 0;
    for (i = 0; i < n/16; i++) {
        z[m] = w[m];
        z[m + 1] = w[m + 1];
        z[m + 2] = w[m + 2];
        z[m + 3] = w[m + 3];
        z[m + 4] = w[m + 4];
        z[m + 5] = w[m + 5];
        z[m + 6] = w[m + 6];
        z[m + 7] = w[m + 7];
        z[m + 8] = w[m + 8];
        z[m + 9] = w[m + 9];
        z[m + 10] = w[m + 10];
        z[m + 11] = w[m + 11];
        z[m + 12] = w[m + 12];
        z[m + 13] = w[m + 13];
        z[m + 14] = w[m + 14];
        z[m + 15] = w[m + 15];

        m += 16;
    }

    // Process remainder
    for (i = (n/16) * 16; i < n; i++) {
        z[i] = w[i];
    }

/*
  Free memory.
*/
    free(w);

    return;
}


/*
 * [FastCode] LU application (unroll factor: 32)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void lus_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, double *l, int32_t *ua,
                             double *r, double *z) {
    int32_t i;
    int32_t j;
    double *w;

    w = (double *) malloc(n * sizeof(double));
/*
  Copy R in.
*/

    // Second optimization
    int32_t m = 0;
    for (i = 0; i < n/32; i++) {
        w[m] = r[m];
        w[m + 1] = r[m + 1];
        w[m + 2] = r[m + 2];
        w[m + 3] = r[m + 3];
        w[m + 4] = r[m + 4];
        w[m + 5] = r[m + 5];
        w[m + 6] = r[m + 6];
        w[m + 7] = r[m + 7];
        w[m + 8] = r[m + 8];
        w[m + 9] = r[m + 9];
        w[m + 10] = r[m + 10];
        w[m + 11] = r[m + 11];
        w[m + 12] = r[m + 12];
        w[m + 13] = r[m + 13];
        w[m + 14] = r[m + 14];
        w[m + 15] = r[m + 15];
        w[m + 16] = r[m + 16];
        w[m + 17] = r[m + 17];
        w[m + 18] = r[m + 18];
        w[m + 19] = r[m + 19];
        w[m + 20] = r[m + 20];
        w[m + 21] = r[m + 21];
        w[m + 22] = r[m + 22];
        w[m + 23] = r[m + 23];
        w[m + 24] = r[m + 24];
        w[m + 25] = r[m + 25];
        w[m + 26] = r[m + 26];
        w[m + 27] = r[m + 27];
        w[m + 28] = r[m + 28];
        w[m + 29] = r[m + 29];
        w[m + 30] = r[m + 30];
        w[m + 31] = r[m + 31];

        m += 32;
    }

    // Process remainder
    for (i = (n/32) * 32; i < n; i++) {
        w[i] = r[i];
    }
/*
  Solve L * w = w where L is unit lower triangular.
*/

    // Unroll inner loop
    for (i = 1; i < n; i++) {

        int32_t K = ua[i] - ia[i];

        // Second optimization
        m = 0;
        for (j = 0; j < K/32; j++) {
            w[i] = w[i] - l[ia[i] + m] * w[ja[ia[i] + m]];
            w[i] = w[i] - l[ia[i] + m + 1] * w[ja[ia[i] + m + 1]];
            w[i] = w[i] - l[ia[i] + m + 2] * w[ja[ia[i] + m + 2]];
            w[i] = w[i] - l[ia[i] + m + 3] * w[ja[ia[i] + m + 3]];
            w[i] = w[i] - l[ia[i] + m + 4] * w[ja[ia[i] + m + 4]];
            w[i] = w[i] - l[ia[i] + m + 5] * w[ja[ia[i] + m + 5]];
            w[i] = w[i] - l[ia[i] + m + 6] * w[ja[ia[i] + m + 6]];
            w[i] = w[i] - l[ia[i] + m + 7] * w[ja[ia[i] + m + 7]];
            w[i] = w[i] - l[ia[i] + m + 8] * w[ja[ia[i] + m + 8]];
            w[i] = w[i] - l[ia[i] + m + 9] * w[ja[ia[i] + m + 9]];
            w[i] = w[i] - l[ia[i] + m + 10] * w[ja[ia[i] + m + 10]];
            w[i] = w[i] - l[ia[i] + m + 11] * w[ja[ia[i] + m + 11]];
            w[i] = w[i] - l[ia[i] + m + 12] * w[ja[ia[i] + m + 12]];
            w[i] = w[i] - l[ia[i] + m + 13] * w[ja[ia[i] + m + 13]];
            w[i] = w[i] - l[ia[i] + m + 14] * w[ja[ia[i] + m + 14]];
            w[i] = w[i] - l[ia[i] + m + 15] * w[ja[ia[i] + m + 15]];
            w[i] = w[i] - l[ia[i] + m + 16] * w[ja[ia[i] + m + 16]];
            w[i] = w[i] - l[ia[i] + m + 17] * w[ja[ia[i] + m + 17]];
            w[i] = w[i] - l[ia[i] + m + 18] * w[ja[ia[i] + m + 18]];
            w[i] = w[i] - l[ia[i] + m + 19] * w[ja[ia[i] + m + 19]];
            w[i] = w[i] - l[ia[i] + m + 20] * w[ja[ia[i] + m + 20]];
            w[i] = w[i] - l[ia[i] + m + 21] * w[ja[ia[i] + m + 21]];
            w[i] = w[i] - l[ia[i] + m + 22] * w[ja[ia[i] + m + 22]];
            w[i] = w[i] - l[ia[i] + m + 23] * w[ja[ia[i] + m + 23]];
            w[i] = w[i] - l[ia[i] + m + 24] * w[ja[ia[i] + m + 24]];
            w[i] = w[i] - l[ia[i] + m + 25] * w[ja[ia[i] + m + 25]];
            w[i] = w[i] - l[ia[i] + m + 26] * w[ja[ia[i] + m + 26]];
            w[i] = w[i] - l[ia[i] + m + 27] * w[ja[ia[i] + m + 27]];
            w[i] = w[i] - l[ia[i] + m + 28] * w[ja[ia[i] + m + 28]];
            w[i] = w[i] - l[ia[i] + m + 29] * w[ja[ia[i] + m + 29]];
            w[i] = w[i] - l[ia[i] + m + 30] * w[ja[ia[i] + m + 30]];
            w[i] = w[i] - l[ia[i] + m + 31] * w[ja[ia[i] + m + 31]];

            m += 32;
        }

        // Process remainder
        for (j = (K/32) * 32; j < K; j++) {
            w[i] = w[i] - l[ia[i] + j] * w[ja[ia[i] + j]];
        }

    }

    /*
      Solve U * w = w, where U is upper triangular.
    */
    for (i = n - 1; 0 <= i; i--) {

        int32_t K = ia[i + 1] - ua[i] - 1;

        // Optimization (steps similar to last optimizations in this method)
        m = 0;
        for (j = 0; j < K/32; j++) {
            w[i] = w[i] - l[ua[i] + m + 1] * w[ja[ua[i] + m + 1]];
            w[i] = w[i] - l[ua[i] + m + 2] * w[ja[ua[i] + m + 2]];
            w[i] = w[i] - l[ua[i] + m + 3] * w[ja[ua[i] + m + 3]];
            w[i] = w[i] - l[ua[i] + m + 4] * w[ja[ua[i] + m + 4]];
            w[i] = w[i] - l[ua[i] + m + 5] * w[ja[ua[i] + m + 5]];
            w[i] = w[i] - l[ua[i] + m + 6] * w[ja[ua[i] + m + 6]];
            w[i] = w[i] - l[ua[i] + m + 7] * w[ja[ua[i] + m + 7]];
            w[i] = w[i] - l[ua[i] + m + 8] * w[ja[ua[i] + m + 8]];
            w[i] = w[i] - l[ua[i] + m + 9] * w[ja[ua[i] + m + 9]];
            w[i] = w[i] - l[ua[i] + m + 10] * w[ja[ua[i] + m + 10]];
            w[i] = w[i] - l[ua[i] + m + 11] * w[ja[ua[i] + m + 11]];
            w[i] = w[i] - l[ua[i] + m + 12] * w[ja[ua[i] + m + 12]];
            w[i] = w[i] - l[ua[i] + m + 13] * w[ja[ua[i] + m + 13]];
            w[i] = w[i] - l[ua[i] + m + 14] * w[ja[ua[i] + m + 14]];
            w[i] = w[i] - l[ua[i] + m + 15] * w[ja[ua[i] + m + 15]];
            w[i] = w[i] - l[ua[i] + m + 16] * w[ja[ua[i] + m + 16]];
            w[i] = w[i] - l[ua[i] + m + 17] * w[ja[ua[i] + m + 17]];
            w[i] = w[i] - l[ua[i] + m + 18] * w[ja[ua[i] + m + 18]];
            w[i] = w[i] - l[ua[i] + m + 19] * w[ja[ua[i] + m + 19]];
            w[i] = w[i] - l[ua[i] + m + 20] * w[ja[ua[i] + m + 20]];
            w[i] = w[i] - l[ua[i] + m + 21] * w[ja[ua[i] + m + 21]];
            w[i] = w[i] - l[ua[i] + m + 22] * w[ja[ua[i] + m + 22]];
            w[i] = w[i] - l[ua[i] + m + 23] * w[ja[ua[i] + m + 23]];
            w[i] = w[i] - l[ua[i] + m + 24] * w[ja[ua[i] + m + 24]];
            w[i] = w[i] - l[ua[i] + m + 25] * w[ja[ua[i] + m + 25]];
            w[i] = w[i] - l[ua[i] + m + 26] * w[ja[ua[i] + m + 26]];
            w[i] = w[i] - l[ua[i] + m + 27] * w[ja[ua[i] + m + 27]];
            w[i] = w[i] - l[ua[i] + m + 28] * w[ja[ua[i] + m + 28]];
            w[i] = w[i] - l[ua[i] + m + 29] * w[ja[ua[i] + m + 29]];
            w[i] = w[i] - l[ua[i] + m + 30] * w[ja[ua[i] + m + 30]];
            w[i] = w[i] - l[ua[i] + m + 31] * w[ja[ua[i] + m + 31]];
            w[i] = w[i] - l[ua[i] + m + 32] * w[ja[ua[i] + m + 32]];

            m += 32;
        }

        // Process remainder
        for (j = (K/32) * 32; j < K; j++) {
            w[i] = w[i] - l[ua[i] + 1 + j] * w[ja[ua[i] + 1 + j]];
        }

        w[i] = w[i] / l[ua[i]];

    }
    /*
      Copy Z out.
    */

    // First optimization
    m = 0;
    for (i = 0; i < n/32; i++) {
        z[m] = w[m];
        z[m + 1] = w[m + 1];
        z[m + 2] = w[m + 2];
        z[m + 3] = w[m + 3];
        z[m + 4] = w[m + 4];
        z[m + 5] = w[m + 5];
        z[m + 6] = w[m + 6];
        z[m + 7] = w[m + 7];
        z[m + 8] = w[m + 8];
        z[m + 9] = w[m + 9];
        z[m + 10] = w[m + 10];
        z[m + 11] = w[m + 11];
        z[m + 12] = w[m + 12];
        z[m + 13] = w[m + 13];
        z[m + 14] = w[m + 14];
        z[m + 15] = w[m + 15];
        z[m + 16] = w[m + 16];
        z[m + 17] = w[m + 17];
        z[m + 18] = w[m + 18];
        z[m + 19] = w[m + 19];
        z[m + 20] = w[m + 20];
        z[m + 21] = w[m + 21];
        z[m + 22] = w[m + 22];
        z[m + 23] = w[m + 23];
        z[m + 24] = w[m + 24];
        z[m + 25] = w[m + 25];
        z[m + 26] = w[m + 26];
        z[m + 27] = w[m + 27];
        z[m + 28] = w[m + 28];
        z[m + 29] = w[m + 29];
        z[m + 30] = w[m + 30];
        z[m + 31] = w[m + 31];

        m += 32;
    }

    // Process remainder
    for (i = (n/32) * 32; i < n; i++) {
        z[i] = w[i];
    }

/*
  Free memory.
*/
    free(w);

    return;
}

/*
  [FastCode] AX_CR computes A*x for a matrix stored in sparse compressed row form.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.

    Input, double A[NZ_NUM], the matrix values.

    Input, double X[N], the vector to be multiplied by A.

    Output, double W[N], the value of A*X.
*/
void ax_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, double *x,
                         double *w) {

    // TODO: Add some heuristics
    switch(ax_unroll_factor) {
        case 1:
            ax_csrvi(n, nz_num, ia, ja, va, vala, x, w);
            break;
        case 4:
            ax_csrvi_ilp_unroll_4(n, nz_num, ia, ja, va, vala, x, w);
            break;
        case 8:
            ax_csrvi_ilp_unroll_8(n, nz_num, ia, ja, va, vala, x, w);
            break;
        case 16:
            ax_csrvi_ilp_unroll_16(n, nz_num, ia, ja, va, vala, x, w);
            break;
        case 32:
            ax_csrvi_ilp_unroll_32(n, nz_num, ia, ja, va, vala, x, w);
            break;
        default:
            ax_csrvi(n, nz_num, ia, ja, va, vala, x, w);
            break;
    }

}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 4)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void ax_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, double *x,
                         double *w) {
    int32_t i;
    int32_t k;
    int32_t k1;
    int32_t k2;


    for (i = 0; i < n; i++) {
        w[i] = 0.0;

        // k1, k2 define the index of the first (and the last element) of the row i
        k1 = ia[i];
        k2 = ia[i + 1];


        /* [OLD] [START]
        for (k = k1; k < k2; k++) {
            w[i] = w[i] + vala[va[k]] * x[ja[k]];
        }
          [OLD] [END] */

        /* First modification
        for (k = 0; k < (k2 - k1); k++) {
            w[i] = w[i] + vala[va[k1 + k]] * x[ja[k1 + k]];
        } */

        int32_t K = k2 - k1;

/*      // First optimization
        // Loop unrolling
        for (k = 0; k < K/4; k++) {
            w[i] = w[i] + vala[va[k1 + 4 * k]] * x[ja[k1 + 4 * k]];
            w[i] = w[i] + vala[va[k1 + 4 * k + 1]] * x[ja[k1 + 4 * k + 1]];
            w[i] = w[i] + vala[va[k1 + 4 * k + 2]] * x[ja[k1 + 4 * k + 2]];
            w[i] = w[i] + vala[va[k1 + 4 * k + 3]] * x[ja[k1 + 4 * k + 3]];

        }

        // Remainder
        for (k = (K/4) * 4; k < K; k++) {
            w[i] = w[i] + vala[va[k1 + k]] * x[ja[k1 + k]];
        }*/


        double tmp = 0.0;
        // Loop unrolling
        int32_t m = 0;
        for (k = 0; k < K/4; k++) {
            tmp += vala[va[k1 + m]] * x[ja[k1 + m]];
            tmp += vala[va[k1 + m + 1]] * x[ja[k1 + m + 1]];
            tmp += vala[va[k1 + m + 2]] * x[ja[k1 + m + 2]];
            tmp += vala[va[k1 + m + 3]] * x[ja[k1 + m + 3]];

            m += 4;
        }

        // Remainder
        for (k = (K/4) * 4; k < K; k++) {
            tmp += + vala[va[k1 + k]] * x[ja[k1 + k]];
        }

        // Set final value
        w[i] = tmp;
    }


    return;
}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 8)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void ax_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, double *x,
                            double *w) {
    int32_t i;
    int32_t k;
    int32_t k1;
    int32_t k2;


    for (i = 0; i < n; i++) {
        w[i] = 0.0;

        // k1, k2 define the index of the first (and the last element) of the row i
        k1 = ia[i];
        k2 = ia[i + 1];

        int32_t K = k2 - k1;

        double tmp = 0.0;
        // Loop unrolling
        int32_t m = 0;
        for (k = 0; k < K/8; k++) {
            tmp += vala[va[k1 + m]] * x[ja[k1 + m]];
            tmp += vala[va[k1 + m + 1]] * x[ja[k1 + m + 1]];
            tmp += vala[va[k1 + m + 2]] * x[ja[k1 + m + 2]];
            tmp += vala[va[k1 + m + 3]] * x[ja[k1 + m + 3]];
            tmp += vala[va[k1 + m + 4]] * x[ja[k1 + m + 4]];
            tmp += vala[va[k1 + m + 5]] * x[ja[k1 + m + 5]];
            tmp += vala[va[k1 + m + 6]] * x[ja[k1 + m + 6]];
            tmp += vala[va[k1 + m + 7]] * x[ja[k1 + m + 7]];

            m += 8;
        }

        // Remainder
        for (k = (K/8) * 8; k < K; k++) {
            tmp += + vala[va[k1 + k]] * x[ja[k1 + k]];
        }

        // Set final value
        w[i] = tmp;
    }


    return;
}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 16)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void ax_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, double *x,
                           double *w) {
    int32_t i;
    int32_t k;
    int32_t k1;
    int32_t k2;


    for (i = 0; i < n; i++) {
        w[i] = 0.0;

        // k1, k2 define the index of the first (and the last element) of the row i
        k1 = ia[i];
        k2 = ia[i + 1];

        int32_t K = k2 - k1;

        double tmp = 0.0;
        // Loop unrolling
        int32_t m = 0;
        for (k = 0; k < K/16; k++) {
            tmp += vala[va[k1 + m]] * x[ja[k1 + m]];
            tmp += vala[va[k1 + m + 1]] * x[ja[k1 + m + 1]];
            tmp += vala[va[k1 + m + 2]] * x[ja[k1 + m + 2]];
            tmp += vala[va[k1 + m + 3]] * x[ja[k1 + m + 3]];
            tmp += vala[va[k1 + m + 4]] * x[ja[k1 + m + 4]];
            tmp += vala[va[k1 + m + 5]] * x[ja[k1 + m + 5]];
            tmp += vala[va[k1 + m + 6]] * x[ja[k1 + m + 6]];
            tmp += vala[va[k1 + m + 7]] * x[ja[k1 + m + 7]];
            tmp += vala[va[k1 + m + 8]] * x[ja[k1 + m + 8]];
            tmp += vala[va[k1 + m + 9]] * x[ja[k1 + m + 9]];
            tmp += vala[va[k1 + m + 10]] * x[ja[k1 + m + 10]];
            tmp += vala[va[k1 + m + 11]] * x[ja[k1 + m + 11]];
            tmp += vala[va[k1 + m + 12]] * x[ja[k1 + m + 12]];
            tmp += vala[va[k1 + m + 13]] * x[ja[k1 + m + 13]];
            tmp += vala[va[k1 + m + 14]] * x[ja[k1 + m + 14]];
            tmp += vala[va[k1 + m + 15]] * x[ja[k1 + m + 15]];

            m += 16;
        }

        // Remainder
        for (k = (K/16) * 16; k < K; k++) {
            tmp += + vala[va[k1 + k]] * x[ja[k1 + k]];
        }

        // Set final value
        w[i] = tmp;
    }


    return;
}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 32)
 *
 * For documentation see 'ax_csrvi_ilp_unroll'
 */
void ax_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala, double *x,
                            double *w) {
    int32_t i;
    int32_t k;
    int32_t k1;
    int32_t k2;


    for (i = 0; i < n; i++) {
        w[i] = 0.0;

        // k1, k2 define the index of the first (and the last element) of the row i
        k1 = ia[i];
        k2 = ia[i + 1];

        int32_t K = k2 - k1;

        double tmp = 0.0;
        // Loop unrolling
        int32_t m = 0;
        for (k = 0; k < K/32; k++) {
            tmp += vala[va[k1 + m]] * x[ja[k1 + m]];
            tmp += vala[va[k1 + m + 1]] * x[ja[k1 + m + 1]];
            tmp += vala[va[k1 + m + 2]] * x[ja[k1 + m + 2]];
            tmp += vala[va[k1 + m + 3]] * x[ja[k1 + m + 3]];
            tmp += vala[va[k1 + m + 4]] * x[ja[k1 + m + 4]];
            tmp += vala[va[k1 + m + 5]] * x[ja[k1 + m + 5]];
            tmp += vala[va[k1 + m + 6]] * x[ja[k1 + m + 6]];
            tmp += vala[va[k1 + m + 7]] * x[ja[k1 + m + 7]];
            tmp += vala[va[k1 + m + 8]] * x[ja[k1 + m + 8]];
            tmp += vala[va[k1 + m + 9]] * x[ja[k1 + m + 9]];
            tmp += vala[va[k1 + m + 10]] * x[ja[k1 + m + 10]];
            tmp += vala[va[k1 + m + 11]] * x[ja[k1 + m + 11]];
            tmp += vala[va[k1 + m + 12]] * x[ja[k1 + m + 12]];
            tmp += vala[va[k1 + m + 13]] * x[ja[k1 + m + 13]];
            tmp += vala[va[k1 + m + 14]] * x[ja[k1 + m + 14]];
            tmp += vala[va[k1 + m + 15]] * x[ja[k1 + m + 15]];
            tmp += vala[va[k1 + m + 16]] * x[ja[k1 + m + 16]];
            tmp += vala[va[k1 + m + 17]] * x[ja[k1 + m + 17]];
            tmp += vala[va[k1 + m + 18]] * x[ja[k1 + m + 18]];
            tmp += vala[va[k1 + m + 19]] * x[ja[k1 + m + 19]];
            tmp += vala[va[k1 + m + 20]] * x[ja[k1 + m + 20]];
            tmp += vala[va[k1 + m + 21]] * x[ja[k1 + m + 21]];
            tmp += vala[va[k1 + m + 22]] * x[ja[k1 + m + 22]];
            tmp += vala[va[k1 + m + 23]] * x[ja[k1 + m + 23]];
            tmp += vala[va[k1 + m + 24]] * x[ja[k1 + m + 24]];
            tmp += vala[va[k1 + m + 25]] * x[ja[k1 + m + 25]];
            tmp += vala[va[k1 + m + 26]] * x[ja[k1 + m + 26]];
            tmp += vala[va[k1 + m + 27]] * x[ja[k1 + m + 27]];
            tmp += vala[va[k1 + m + 28]] * x[ja[k1 + m + 28]];
            tmp += vala[va[k1 + m + 29]] * x[ja[k1 + m + 29]];
            tmp += vala[va[k1 + m + 30]] * x[ja[k1 + m + 30]];
            tmp += vala[va[k1 + m + 31]] * x[ja[k1 + m + 31]];

            m += 32;
        }

        // Remainder
        for (k = (K/32) * 32; k < K; k++) {
            tmp += + vala[va[k1 + k]] * x[ja[k1 + k]];
        }

        // Set final value
        w[i] = tmp;
    }


    return;
}
/*
    This routine guarantees that the entries in the CSR-VI matrix
    are properly sorted.

    After the sorting, the entries of the matrix are rearranged in such
    a way that the entries of each column are listed in ascending order
    of their column values.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], the compressed row index vector.

    Input/output, int32_t JA[NZ_NUM], the column indices of the matrix values.
    On output, the order of the entries of JA may have changed because of
    the sorting.

    Input/output, double A[NZ_NUM], the matrix values.  On output, the
    order of the entries may have changed because of the sorting.
*/
void rearrange_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *va, double *vala) {


    if (use_quicksort) {
        rearrange_csrvi_qs(n, nz_num, ia, ja, va, vala);
    } else {

        switch (rearrange_unroll_factor) {
            case 1:
                rearrange_csrvi(n, nz_num, ia, ja, va, vala);
                break;
            case 4:
                rearrange_csrvi_ilp_unroll_4(n, nz_num, ia, ja, va, vala);
                break;
            case 8:
                rearrange_csrvi_ilp_unroll_8(n, nz_num, ia, ja, va, vala);
                break;
            case 16:
                rearrange_csrvi_ilp_unroll_16(n, nz_num, ia, ja, va, vala);
                break;
            case 32:
                rearrange_csrvi_ilp_unroll_32(n, nz_num, ia, ja, va, vala);
                break;
            default:
                rearrange_csrvi(n, nz_num, ia, ja, va, vala);
                break;
        }
    }
}


/*
 * [FastCode] Sort matrix in CSR-VI format (unroll factor: 4)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'rearrange_csrvi_ilp_unroll'
 */
void rearrange_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[]) {

    double dtemp;
    int32_t i;
    int32_t is;
    int32_t itemp;
    int32_t j;
    int32_t j1;
    int32_t j2;
    int32_t k;

    for (i = 0; i < n; i++) {
        j1 = ia[i];
        j2 = ia[i + 1];

        // Number of elements in current row
        is = j2 - j1;

        for (k = 1; k < is; k++) {

            /* [BEGIN] [OLD]
            for (j = j1; j < j2 - k; j++) {
                if (ja[j + 1] < ja[j]) {

                    // Swap column indices
                    itemp = ja[j + 1];
                    ja[j + 1] = ja[j];
                    ja[j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j + 1];
                    va[j + 1] = va[j];
                    va[j] = dtemp;
                }
            } [END] [OLD] */

/*
            // First modification
            int32_t K = j2 - k - j1;
            for (j = 0; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }*/

/*
            // First optimization
            int32_t K = j2 - k - j1;
            for (j = 0; j < K/4; j++) {
                if (ja[j1 + 4 * j + 1] < ja[j1 + 4 * j]) {

                    // Swap column indices
                    itemp = ja[j1 + 4 * j + 1];
                    ja[j1 + 4 * j + 1] = ja[j1 + 4 * j];
                    ja[j1 + 4 * j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + 4 * j + 1];
                    va[j1 + 4 * j + 1] = va[j1 + 4 * j];
                    va[j1 + 4 * j] = dtemp;
                }

                if (ja[j1 + 4 * j + 1 + 1] < ja[j1 + 4 * j + 1]) {

                    // Swap column indices
                    itemp = ja[j1 + 4 * j + 1 + 1];
                    ja[j1 + 4 * j + 1 + 1] = ja[j1 + 4 * j + 1];
                    ja[j1 + 4 * j + 1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + 4 * j + 1 + 1];
                    va[j1 + 4 * j + 1 + 1] = va[j1 + 4 * j + 1];
                    va[j1 + 4 * j + 1] = dtemp;
                }

                if (ja[j1 + 4 * j + 2 + 1] < ja[j1 + 4 * j + 2]) {

                    // Swap column indices
                    itemp = ja[j1 + 4 * j + 2 + 1];
                    ja[j1 + 4 * j + 2 + 1] = ja[j1 + 4 * j + 2];
                    ja[j1 + 4 * j + 2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + 4 * j + 2 + 1];
                    va[j1 + 4 * j + 2 + 1] = va[j1 + 4 * j + 2];
                    va[j1 + 4 * j + 2] = dtemp;
                }

                if (ja[j1 + 4 * j + 3 + 1] < ja[j1 + 4 * j + 3]) {

                    // Swap column indices
                    itemp = ja[j1 + 4 * j + 3 + 1];
                    ja[j1 + 4 * j + 3 + 1] = ja[j1 + 4 * j + 3];
                    ja[j1 + 4 * j + 3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + 4 * j + 3 + 1];
                    va[j1 + 4 * j + 3 + 1] = va[j1 + 4 * j + 3];
                    va[j1 + 4 * j + 3] = dtemp;
                }
            }

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }*/

/*
            // Second optimization
            int32_t m = 0;
            int32_t K = j2 - k - j1;
            for (j = 0; j < K/4; j++) {
                if (ja[j1 + m + 1] < ja[j1 + m]) {

                    // Swap column indices
                    itemp = ja[j1 + m + 1];
                    ja[j1 + m + 1] = ja[j1 + m];
                    ja[j1 + m] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + m + 1];
                    va[j1 + m + 1] = va[j1 + m];
                    va[j1 + m] = dtemp;
                }

                if (ja[j1 + m + 2] < ja[j1 + m + 1]) {

                    // Swap column indices
                    itemp = ja[j1 + m + 2];
                    ja[j1 + m + 2] = ja[j1 + m + 1];
                    ja[j1 + m + 1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + m + 2];
                    va[j1 + m + 2] = va[j1 + m + 1];
                    va[j1 + m + 1] = dtemp;
                }

                if (ja[j1 + m + 3] < ja[j1 + m + 2]) {

                    // Swap column indices
                    itemp = ja[j1 + m + 3];
                    ja[j1 + m + 3] = ja[j1 + m + 2];
                    ja[j1 + m + 2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + m + 3];
                    va[j1 + m + 3] = va[j1 + m + 2];
                    va[j1 + m + 2] = dtemp;
                }

                if (ja[j1 + m + 4] < ja[j1 + m + 3]) {

                    // Swap column indices
                    itemp = ja[j1 + m + 4];
                    ja[j1 + m + 4] = ja[j1 + m + 3];
                    ja[j1 + m + 3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + m + 4];
                    va[j1 + m + 4] = va[j1 + m + 3];
                    va[j1 + m + 3] = dtemp;
                }

                m += 4;
            }

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }*/

/*
            // Third optimization (Introduce more variables)
            int32_t m = 0;
            int32_t m0, m1, m2, m3;
            m0 = m1 = m2 = m3 = j1;
            int32_t K = j2 - k - j1;
            for (j = 0; j < K/4; j++) {
                if (ja[m0 + 1] < ja[m0]) {

                    // Swap column indices
                    itemp = ja[m0 + 1];
                    ja[m0 + 1] = ja[m0];
                    ja[m0] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m0 + 1];
                    va[m0 + 1] = va[m0];
                    va[m0] = dtemp;
                }

                m0 += 4;

                if (ja[m1 + 2] < ja[m1 + 1]) {

                    // Swap column indices
                    itemp = ja[m1 + 2];
                    ja[m1 + 2] = ja[m1 + 1];
                    ja[m1 + 1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m1 + 2];
                    va[m1 + 2] = va[m1 + 1];
                    va[m1 + 1] = dtemp;
                }

                m1 += 4;

                if (ja[m2 + 3] < ja[m2 + 2]) {

                    // Swap column indices
                    itemp = ja[m2 + 3];
                    ja[m2 + 3] = ja[m2 + 2];
                    ja[m2 + 2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m2 + 3];
                    va[m2 + 3] = va[m2 + 2];
                    va[m2 + 2] = dtemp;
                }

                m2 += 4;

                if (ja[m3 + 4] < ja[m3 + 3]) {

                    // Swap column indices
                    itemp = ja[m3 + 4];
                    ja[m3 + 4] = ja[m3 + 3];
                    ja[m3 + 3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m3 + 4];
                    va[m3 + 4] = va[m3 + 3];
                    va[m3 + 3] = dtemp;
                }

                m3 += 4;
            }

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }*/

            // Fourth optimization (Shift variables)
            int32_t m0, m1, m2, m3;

            m0 = j1;
            m1 = j1 + 1;
            m2 = j1 + 2;
            m3 = j1 + 3;

            int32_t K = j2 - k - j1;
            for (j = 0; j < K/4; j++) {
                if (ja[m0 + 1] < ja[m0]) {

                    // Swap column indices
                    itemp = ja[m0 + 1];
                    ja[m0 + 1] = ja[m0];
                    ja[m0] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m0 + 1];
                    va[m0 + 1] = va[m0];
                    va[m0] = dtemp;
                }

                m0 += 4;

                if (ja[m1 + 1] < ja[m1]) {

                    // Swap column indices
                    itemp = ja[m1 + 1];
                    ja[m1 + 1] = ja[m1];
                    ja[m1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m1 + 1];
                    va[m1 + 1] = va[m1];
                    va[m1] = dtemp;
                }

                m1 += 4;

                if (ja[m2 + 1] < ja[m2]) {

                    // Swap column indices
                    itemp = ja[m2 + 1];
                    ja[m2 + 1] = ja[m2];
                    ja[m2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m2 + 1];
                    va[m2 + 1] = va[m2];
                    va[m2] = dtemp;
                }

                m2 += 4;

                if (ja[m3 + 1] < ja[m3]) {

                    // Swap column indices
                    itemp = ja[m3 + 1];
                    ja[m3 + 1] = ja[m3];
                    ja[m3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m3 + 1];
                    va[m3 + 1] = va[m3];
                    va[m3] = dtemp;
                }

                m3 += 4;
            }

            // Process remainder
            for (j = (K/4) * 4; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }
        }
    }
    return;
}


/*
 * [FastCode] Sort matrix in CSR-VI format (unroll factor: 8)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'rearrange_csrvi_ilp_unroll'
 */
void rearrange_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[]) {

    double dtemp;
    int32_t i;
    int32_t is;
    int32_t itemp;
    int32_t j;
    int32_t j1;
    int32_t j2;
    int32_t k;

    for (i = 0; i < n; i++) {
        j1 = ia[i];
        j2 = ia[i + 1];

        // Number of elements in current row
        is = j2 - j1;

        for (k = 1; k < is; k++) {


            int32_t m0, m1, m2, m3;
            int32_t m4, m5, m6, m7;

            m0 = j1;
            m1 = j1 + 1;
            m2 = j1 + 2;
            m3 = j1 + 3;
            m4 = j1 + 4;
            m5 = j1 + 5;
            m6 = j1 + 6;
            m7 = j1 + 7;

            int32_t K = j2 - k - j1;
            for (j = 0; j < K/8; j++) {
                if (ja[m0 + 1] < ja[m0]) {

                    // Swap column indices
                    itemp = ja[m0 + 1];
                    ja[m0 + 1] = ja[m0];
                    ja[m0] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m0 + 1];
                    va[m0 + 1] = va[m0];
                    va[m0] = dtemp;
                }

                m0 += 8;

                if (ja[m1 + 1] < ja[m1]) {

                    // Swap column indices
                    itemp = ja[m1 + 1];
                    ja[m1 + 1] = ja[m1];
                    ja[m1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m1 + 1];
                    va[m1 + 1] = va[m1];
                    va[m1] = dtemp;
                }

                m1 += 8;

                if (ja[m2 + 1] < ja[m2]) {

                    // Swap column indices
                    itemp = ja[m2 + 1];
                    ja[m2 + 1] = ja[m2];
                    ja[m2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m2 + 1];
                    va[m2 + 1] = va[m2];
                    va[m2] = dtemp;
                }

                m2 += 8;

                if (ja[m3 + 1] < ja[m3]) {

                    // Swap column indices
                    itemp = ja[m3 + 1];
                    ja[m3 + 1] = ja[m3];
                    ja[m3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m3 + 1];
                    va[m3 + 1] = va[m3];
                    va[m3] = dtemp;
                }

                m3 += 8;

                if (ja[m4 + 1] < ja[m4]) {

                    // Swap column indices
                    itemp = ja[m4 + 1];
                    ja[m4 + 1] = ja[m4];
                    ja[m4] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m4 + 1];
                    va[m4 + 1] = va[m4];
                    va[m4] = dtemp;
                }

                m4 += 8;

                if (ja[m5 + 1] < ja[m5]) {

                    // Swap column indices
                    itemp = ja[m5 + 1];
                    ja[m5 + 1] = ja[m5];
                    ja[m5] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m5 + 1];
                    va[m5 + 1] = va[m5];
                    va[m5] = dtemp;
                }

                m5 += 8;

                if (ja[m6 + 1] < ja[m6]) {

                    // Swap column indices
                    itemp = ja[m6 + 1];
                    ja[m6 + 1] = ja[m6];
                    ja[m6] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m6 + 1];
                    va[m6 + 1] = va[m6];
                    va[m6] = dtemp;
                }

                m6 += 8;

                if (ja[m7 + 1] < ja[m7]) {

                    // Swap column indices
                    itemp = ja[m7 + 1];
                    ja[m7 + 1] = ja[m7];
                    ja[m7] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m7 + 1];
                    va[m7 + 1] = va[m7];
                    va[m7] = dtemp;
                }

                m7 += 8;
            }

            // Process remainder
            for (j = (K/8) * 8; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }
        }
    }
    return;
}


/*
 * [FastCode] Sort matrix in CSR-VI format (unroll factor: 16)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'rearrange_csrvi_ilp_unroll'
 */
void rearrange_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[]) {

    double dtemp;
    int32_t i;
    int32_t is;
    int32_t itemp;
    int32_t j;
    int32_t j1;
    int32_t j2;
    int32_t k;

    for (i = 0; i < n; i++) {
        j1 = ia[i];
        j2 = ia[i + 1];

        // Number of elements in current row
        is = j2 - j1;

        for (k = 1; k < is; k++) {


            int32_t m0, m1, m2, m3;
            int32_t m4, m5, m6, m7;
            int32_t m8, m9, m10, m11;
            int32_t m12, m13, m14, m15;

            m0 = j1;
            m1 = j1 + 1;
            m2 = j1 + 2;
            m3 = j1 + 3;
            m4 = j1 + 4;
            m5 = j1 + 5;
            m6 = j1 + 6;
            m7 = j1 + 7;
            m8 = j1 + 8;
            m9 = j1 + 9;
            m10 = j1 + 10;
            m11 = j1 + 11;
            m12 = j1 + 12;
            m13 = j1 + 13;
            m14 = j1 + 14;
            m15 = j1 + 15;

            int32_t K = j2 - k - j1;
            for (j = 0; j < K/16; j++) {
                if (ja[m0 + 1] < ja[m0]) {

                    // Swap column indices
                    itemp = ja[m0 + 1];
                    ja[m0 + 1] = ja[m0];
                    ja[m0] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m0 + 1];
                    va[m0 + 1] = va[m0];
                    va[m0] = dtemp;
                }

                m0 += 16;

                if (ja[m1 + 1] < ja[m1]) {

                    // Swap column indices
                    itemp = ja[m1 + 1];
                    ja[m1 + 1] = ja[m1];
                    ja[m1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m1 + 1];
                    va[m1 + 1] = va[m1];
                    va[m1] = dtemp;
                }

                m1 += 16;

                if (ja[m2 + 1] < ja[m2]) {

                    // Swap column indices
                    itemp = ja[m2 + 1];
                    ja[m2 + 1] = ja[m2];
                    ja[m2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m2 + 1];
                    va[m2 + 1] = va[m2];
                    va[m2] = dtemp;
                }

                m2 += 16;

                if (ja[m3 + 1] < ja[m3]) {

                    // Swap column indices
                    itemp = ja[m3 + 1];
                    ja[m3 + 1] = ja[m3];
                    ja[m3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m3 + 1];
                    va[m3 + 1] = va[m3];
                    va[m3] = dtemp;
                }

                m3 += 16;

                if (ja[m4 + 1] < ja[m4]) {

                    // Swap column indices
                    itemp = ja[m4 + 1];
                    ja[m4 + 1] = ja[m4];
                    ja[m4] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m4 + 1];
                    va[m4 + 1] = va[m4];
                    va[m4] = dtemp;
                }

                m4 += 16;

                if (ja[m5 + 1] < ja[m5]) {

                    // Swap column indices
                    itemp = ja[m5 + 1];
                    ja[m5 + 1] = ja[m5];
                    ja[m5] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m5 + 1];
                    va[m5 + 1] = va[m5];
                    va[m5] = dtemp;
                }

                m5 += 16;

                if (ja[m6 + 1] < ja[m6]) {

                    // Swap column indices
                    itemp = ja[m6 + 1];
                    ja[m6 + 1] = ja[m6];
                    ja[m6] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m6 + 1];
                    va[m6 + 1] = va[m6];
                    va[m6] = dtemp;
                }

                m6 += 16;

                if (ja[m7 + 1] < ja[m7]) {

                    // Swap column indices
                    itemp = ja[m7 + 1];
                    ja[m7 + 1] = ja[m7];
                    ja[m7] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m7 + 1];
                    va[m7 + 1] = va[m7];
                    va[m7] = dtemp;
                }

                m7 += 16;

                if (ja[m8 + 1] < ja[m8]) {

                    // Swap column indices
                    itemp = ja[m8 + 1];
                    ja[m8 + 1] = ja[m8];
                    ja[m8] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m8 + 1];
                    va[m8 + 1] = va[m8];
                    va[m8] = dtemp;
                }

                m8 += 16;

                if (ja[m9 + 1] < ja[m9]) {

                    // Swap column indices
                    itemp = ja[m9 + 1];
                    ja[m9 + 1] = ja[m9];
                    ja[m9] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m9 + 1];
                    va[m9 + 1] = va[m9];
                    va[m9] = dtemp;
                }

                m9 += 16;

                if (ja[m10 + 1] < ja[m10]) {

                    // Swap column indices
                    itemp = ja[m10 + 1];
                    ja[m10 + 1] = ja[m10];
                    ja[m10] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m10 + 1];
                    va[m10 + 1] = va[m10];
                    va[m10] = dtemp;
                }

                m10 += 16;

                if (ja[m11 + 1] < ja[m11]) {

                    // Swap column indices
                    itemp = ja[m11 + 1];
                    ja[m11 + 1] = ja[m11];
                    ja[m11] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m11 + 1];
                    va[m11 + 1] = va[m11];
                    va[m11] = dtemp;
                }

                m11 += 16;

                if (ja[m12 + 1] < ja[m12]) {

                    // Swap column indices
                    itemp = ja[m12 + 1];
                    ja[m12 + 1] = ja[m12];
                    ja[m12] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m12 + 1];
                    va[m12 + 1] = va[m12];
                    va[m12] = dtemp;
                }

                m12 += 16;

                if (ja[m13 + 1] < ja[m13]) {

                    // Swap column indices
                    itemp = ja[m13 + 1];
                    ja[m13 + 1] = ja[m13];
                    ja[m13] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m13 + 1];
                    va[m13 + 1] = va[m13];
                    va[m13] = dtemp;
                }

                m13 += 16;

                if (ja[m14 + 1] < ja[m14]) {

                    // Swap column indices
                    itemp = ja[m14 + 1];
                    ja[m14 + 1] = ja[m14];
                    ja[m14] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m14 + 1];
                    va[m14 + 1] = va[m14];
                    va[m14] = dtemp;
                }

                m14 += 16;

                if (ja[m15 + 1] < ja[m15]) {

                    // Swap column indices
                    itemp = ja[m15 + 1];
                    ja[m15 + 1] = ja[m15];
                    ja[m15] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m15 + 1];
                    va[m15 + 1] = va[m15];
                    va[m15] = dtemp;
                }

                m15 += 16;

            }

            // Process remainder
            for (j = (K/16) * 16; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }
        }
    }
    return;
}


/*
 * [FastCode] Sort matrix in CSR-VI format (unroll factor: 32)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'rearrange_csrvi_ilp_unroll'
 */
void rearrange_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t va[], double vala[]) {

    double dtemp;
    int32_t i;
    int32_t is;
    int32_t itemp;
    int32_t j;
    int32_t j1;
    int32_t j2;
    int32_t k;

    for (i = 0; i < n; i++) {
        j1 = ia[i];
        j2 = ia[i + 1];

        // Number of elements in current row
        is = j2 - j1;

        for (k = 1; k < is; k++) {


            int32_t m0, m1, m2, m3;
            int32_t m4, m5, m6, m7;
            int32_t m8, m9, m10, m11;
            int32_t m12, m13, m14, m15;
            int32_t m16, m17, m18, m19;
            int32_t m20, m21, m22, m23;
            int32_t m24, m25, m26, m27;
            int32_t m28, m29, m30, m31;

            m0 = j1;
            m1 = j1 + 1;
            m2 = j1 + 2;
            m3 = j1 + 3;
            m4 = j1 + 4;
            m5 = j1 + 5;
            m6 = j1 + 6;
            m7 = j1 + 7;
            m8 = j1 + 8;
            m9 = j1 + 9;
            m10 = j1 + 10;
            m11 = j1 + 11;
            m12 = j1 + 12;
            m13 = j1 + 13;
            m14 = j1 + 14;
            m15 = j1 + 15;
            m16 = j1 + 16;
            m17 = j1 + 17;
            m18 = j1 + 18;
            m19 = j1 + 19;
            m20 = j1 + 20;
            m21 = j1 + 21;
            m22 = j1 + 22;
            m23 = j1 + 23;
            m24 = j1 + 24;
            m25 = j1 + 25;
            m26 = j1 + 26;
            m27 = j1 + 27;
            m28 = j1 + 28;
            m29 = j1 + 29;
            m30 = j1 + 30;
            m31 = j1 + 31;

            int32_t K = j2 - k - j1;
            for (j = 0; j < K/32; j++) {
                if (ja[m0 + 1] < ja[m0]) {

                    // Swap column indices
                    itemp = ja[m0 + 1];
                    ja[m0 + 1] = ja[m0];
                    ja[m0] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m0 + 1];
                    va[m0 + 1] = va[m0];
                    va[m0] = dtemp;
                }

                m0 += 32;

                if (ja[m1 + 1] < ja[m1]) {

                    // Swap column indices
                    itemp = ja[m1 + 1];
                    ja[m1 + 1] = ja[m1];
                    ja[m1] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m1 + 1];
                    va[m1 + 1] = va[m1];
                    va[m1] = dtemp;
                }

                m1 += 32;

                if (ja[m2 + 1] < ja[m2]) {

                    // Swap column indices
                    itemp = ja[m2 + 1];
                    ja[m2 + 1] = ja[m2];
                    ja[m2] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m2 + 1];
                    va[m2 + 1] = va[m2];
                    va[m2] = dtemp;
                }

                m2 += 32;

                if (ja[m3 + 1] < ja[m3]) {

                    // Swap column indices
                    itemp = ja[m3 + 1];
                    ja[m3 + 1] = ja[m3];
                    ja[m3] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m3 + 1];
                    va[m3 + 1] = va[m3];
                    va[m3] = dtemp;
                }

                m3 += 32;

                if (ja[m4 + 1] < ja[m4]) {

                    // Swap column indices
                    itemp = ja[m4 + 1];
                    ja[m4 + 1] = ja[m4];
                    ja[m4] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m4 + 1];
                    va[m4 + 1] = va[m4];
                    va[m4] = dtemp;
                }

                m4 += 32;

                if (ja[m5 + 1] < ja[m5]) {

                    // Swap column indices
                    itemp = ja[m5 + 1];
                    ja[m5 + 1] = ja[m5];
                    ja[m5] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m5 + 1];
                    va[m5 + 1] = va[m5];
                    va[m5] = dtemp;
                }

                m5 += 32;

                if (ja[m6 + 1] < ja[m6]) {

                    // Swap column indices
                    itemp = ja[m6 + 1];
                    ja[m6 + 1] = ja[m6];
                    ja[m6] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m6 + 1];
                    va[m6 + 1] = va[m6];
                    va[m6] = dtemp;
                }

                m6 += 32;

                if (ja[m7 + 1] < ja[m7]) {

                    // Swap column indices
                    itemp = ja[m7 + 1];
                    ja[m7 + 1] = ja[m7];
                    ja[m7] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m7 + 1];
                    va[m7 + 1] = va[m7];
                    va[m7] = dtemp;
                }

                m7 += 32;

                if (ja[m8 + 1] < ja[m8]) {

                    // Swap column indices
                    itemp = ja[m8 + 1];
                    ja[m8 + 1] = ja[m8];
                    ja[m8] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m8 + 1];
                    va[m8 + 1] = va[m8];
                    va[m8] = dtemp;
                }

                m8 += 32;

                if (ja[m9 + 1] < ja[m9]) {

                    // Swap column indices
                    itemp = ja[m9 + 1];
                    ja[m9 + 1] = ja[m9];
                    ja[m9] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m9 + 1];
                    va[m9 + 1] = va[m9];
                    va[m9] = dtemp;
                }

                m9 += 32;

                if (ja[m10 + 1] < ja[m10]) {

                    // Swap column indices
                    itemp = ja[m10 + 1];
                    ja[m10 + 1] = ja[m10];
                    ja[m10] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m10 + 1];
                    va[m10 + 1] = va[m10];
                    va[m10] = dtemp;
                }

                m10 += 32;

                if (ja[m11 + 1] < ja[m11]) {

                    // Swap column indices
                    itemp = ja[m11 + 1];
                    ja[m11 + 1] = ja[m11];
                    ja[m11] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m11 + 1];
                    va[m11 + 1] = va[m11];
                    va[m11] = dtemp;
                }

                m11 += 32;

                if (ja[m12 + 1] < ja[m12]) {

                    // Swap column indices
                    itemp = ja[m12 + 1];
                    ja[m12 + 1] = ja[m12];
                    ja[m12] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m12 + 1];
                    va[m12 + 1] = va[m12];
                    va[m12] = dtemp;
                }

                m12 += 32;

                if (ja[m13 + 1] < ja[m13]) {

                    // Swap column indices
                    itemp = ja[m13 + 1];
                    ja[m13 + 1] = ja[m13];
                    ja[m13] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m13 + 1];
                    va[m13 + 1] = va[m13];
                    va[m13] = dtemp;
                }

                m13 += 32;

                if (ja[m14 + 1] < ja[m14]) {

                    // Swap column indices
                    itemp = ja[m14 + 1];
                    ja[m14 + 1] = ja[m14];
                    ja[m14] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m14 + 1];
                    va[m14 + 1] = va[m14];
                    va[m14] = dtemp;
                }

                m14 += 32;

                if (ja[m15 + 1] < ja[m15]) {

                    // Swap column indices
                    itemp = ja[m15 + 1];
                    ja[m15 + 1] = ja[m15];
                    ja[m15] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m15 + 1];
                    va[m15 + 1] = va[m15];
                    va[m15] = dtemp;
                }

                m15 += 32;

                if (ja[m16 + 1] < ja[m16]) {

                    // Swap column indices
                    itemp = ja[m16 + 1];
                    ja[m16 + 1] = ja[m16];
                    ja[m16] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m16 + 1];
                    va[m16 + 1] = va[m16];
                    va[m16] = dtemp;
                }

                m16 += 32;

                if (ja[m17 + 1] < ja[m17]) {

                    // Swap column indices
                    itemp = ja[m17 + 1];
                    ja[m17 + 1] = ja[m17];
                    ja[m17] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m17 + 1];
                    va[m17 + 1] = va[m17];
                    va[m17] = dtemp;
                }

                m17 += 32;

                if (ja[m18 + 1] < ja[m18]) {

                    // Swap column indices
                    itemp = ja[m18 + 1];
                    ja[m18 + 1] = ja[m18];
                    ja[m18] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m18 + 1];
                    va[m18 + 1] = va[m18];
                    va[m18] = dtemp;
                }

                m18 += 32;

                if (ja[m19 + 1] < ja[m19]) {

                    // Swap column indices
                    itemp = ja[m19 + 1];
                    ja[m19 + 1] = ja[m19];
                    ja[m19] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m19 + 1];
                    va[m19 + 1] = va[m19];
                    va[m19] = dtemp;
                }

                m19 += 32;

                if (ja[m20 + 1] < ja[m20]) {

                    // Swap column indices
                    itemp = ja[m20 + 1];
                    ja[m20 + 1] = ja[m20];
                    ja[m20] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m20 + 1];
                    va[m20 + 1] = va[m20];
                    va[m20] = dtemp;
                }

                m20 += 32;

                if (ja[m21 + 1] < ja[m21]) {

                    // Swap column indices
                    itemp = ja[m21 + 1];
                    ja[m21 + 1] = ja[m21];
                    ja[m21] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m21 + 1];
                    va[m21 + 1] = va[m21];
                    va[m21] = dtemp;
                }

                m21 += 32;

                if (ja[m22 + 1] < ja[m22]) {

                    // Swap column indices
                    itemp = ja[m22 + 1];
                    ja[m22 + 1] = ja[m22];
                    ja[m22] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m22 + 1];
                    va[m22 + 1] = va[m22];
                    va[m22] = dtemp;
                }

                m22 += 32;

                if (ja[m23 + 1] < ja[m23]) {

                    // Swap column indices
                    itemp = ja[m23 + 1];
                    ja[m23 + 1] = ja[m23];
                    ja[m23] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m23 + 1];
                    va[m23 + 1] = va[m23];
                    va[m23] = dtemp;
                }

                m23 += 32;

                if (ja[m24 + 1] < ja[m24]) {

                    // Swap column indices
                    itemp = ja[m24 + 1];
                    ja[m24 + 1] = ja[m24];
                    ja[m24] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m24 + 1];
                    va[m24 + 1] = va[m24];
                    va[m24] = dtemp;
                }

                m24 += 32;

                if (ja[m25 + 1] < ja[m25]) {

                    // Swap column indices
                    itemp = ja[m25 + 1];
                    ja[m25 + 1] = ja[m25];
                    ja[m25] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m25 + 1];
                    va[m25 + 1] = va[m25];
                    va[m25] = dtemp;
                }

                m25 += 32;

                if (ja[m26 + 1] < ja[m26]) {

                    // Swap column indices
                    itemp = ja[m26 + 1];
                    ja[m26 + 1] = ja[m26];
                    ja[m26] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m26 + 1];
                    va[m26 + 1] = va[m26];
                    va[m26] = dtemp;
                }

                m26 += 32;

                if (ja[m27 + 1] < ja[m27]) {

                    // Swap column indices
                    itemp = ja[m27 + 1];
                    ja[m27 + 1] = ja[m27];
                    ja[m27] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m27 + 1];
                    va[m27 + 1] = va[m27];
                    va[m27] = dtemp;
                }

                m27 += 32;

                if (ja[m28 + 1] < ja[m28]) {

                    // Swap column indices
                    itemp = ja[m28 + 1];
                    ja[m28 + 1] = ja[m28];
                    ja[m28] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m28 + 1];
                    va[m28 + 1] = va[m28];
                    va[m28] = dtemp;
                }

                m28 += 32;

                if (ja[m29 + 1] < ja[m29]) {

                    // Swap column indices
                    itemp = ja[m29 + 1];
                    ja[m29 + 1] = ja[m29];
                    ja[m29] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m29 + 1];
                    va[m29 + 1] = va[m29];
                    va[m29] = dtemp;
                }

                m29 += 32;

                if (ja[m30 + 1] < ja[m30]) {

                    // Swap column indices
                    itemp = ja[m30 + 1];
                    ja[m30 + 1] = ja[m30];
                    ja[m30] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m30 + 1];
                    va[m30 + 1] = va[m30];
                    va[m30] = dtemp;
                }

                m30 += 32;

                if (ja[m31 + 1] < ja[m31]) {

                    // Swap column indices
                    itemp = ja[m31 + 1];
                    ja[m31 + 1] = ja[m31];
                    ja[m31] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[m31 + 1];
                    va[m31 + 1] = va[m31];
                    va[m31] = dtemp;
                }

                m31 += 32;
            }

            // Process remainder
            for (j = (K/32) * 32; j < K; j++) {
                if (ja[j1 + j + 1] < ja[j1 + j]) {

                    // Swap column indices
                    itemp = ja[j1 + j + 1];
                    ja[j1 + j + 1] = ja[j1 + j];
                    ja[j1 + j] = itemp;

                    // Swap values (Relink va)
                    dtemp = va[j1 + j + 1];
                    va[j1 + j + 1] = va[j1 + j];
                    va[j1 + j] = dtemp;
                }
            }
        }
    }
    return;
}


// NO CSR-VI TRANSFORMATION REQUIRED!
/*
  Purpose:

    DIAGONAL_POINTER_CR finds diagonal entries in a sparse compressed row matrix.

  Discussion:

    The matrix A is assumed to be stored in compressed row format.  Only
    the nonzero entries of A are stored.  The vector JA stores the
    column index of the nonzero value.  The nonzero values are sorted
    by row, and the compressed row vector IA then has the property that
    the entries in A and JA that correspond to row I occur in indices
    IA[I] through IA[I+1]-1.

    The array UA can be used to locate the diagonal elements of the matrix.

    It is assumed that every row of the matrix includes a diagonal element,
    and that the elements of each row have been ascending sorted.

  Parameters:

    Input, int32_t N, the order of the system.

    Input, int32_t NZ_NUM, the number of nonzeros.

    Input, int32_t IA[N+1], JA[NZ_NUM], the row and column indices
    of the matrix values.  The row vector has been compressed.  On output,
    the order of the entries of JA may have changed because of the sorting.

    Output, int32_t UA[N], the index of the diagonal element of each row.
*/
void diagonal_pointer_csrvi_ilp_unroll(int32_t n, int32_t nz_num, int32_t *ia, int32_t *ja, int32_t *ua) {

    // TODO: Add some heuristics
    switch(diag_unroll_factor) {
        case 1:
            diagonal_pointer_csrvi(n, nz_num, ia, ja, ua);
            break;
        case 4:
            diagonal_pointer_csrvi_ilp_unroll_4(n, nz_num, ia, ja, ua);
            break;
        case 8:
            diagonal_pointer_csrvi_ilp_unroll_8(n, nz_num, ia, ja, ua);
            break;
        case 16:
            diagonal_pointer_csrvi_ilp_unroll_16(n, nz_num, ia, ja, ua);
            break;
        case 32:
            diagonal_pointer_csrvi_ilp_unroll_32(n, nz_num, ia, ja, ua);
            break;
        default:
            diagonal_pointer_csrvi(n, nz_num, ia, ja, ua);
            break;
    }

}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 4)
 *
 * PROCESS OF OPTIMIZATION IS ANNOTATED IN THE CODE
 *
 * For documentation see 'diagonal_pointer_csrvi_ilp_unroll'
 */
void diagonal_pointer_csrvi_ilp_unroll_4(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[]) {
    int32_t i;
    int32_t j;
    int32_t j1;
    int32_t j2;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        /* [OLD] [BEGIN]
        for (j = j1; j < j2; j++) {
            if (ja[j] == i) {
                ua[i] = j;
            }
        }
           [OLD] [END] */

        // First modification
/*      for (j = 0; j < (j2 - j1); j++) {
            if (ja[j1 + j] == i) {
                ua[i] = j1 + j;
            }
        }*/

        int K = j2 - j1;
/*
        // First optimization
        for (j = 0; j < K/4; j++) {
            if (ja[j1 + 4 * j] == i) {
                ua[i] = j1 + 4 * j;
            }

            if (ja[j1 + 4 * j + 1] == i) {
                ua[i] = j1 + 4 * j + 1;
            }

            if (ja[j1 + 4 * j + 2] == i) {
                ua[i] = j1 + 4 * j + 2;
            }

            if (ja[j1 + 4 * j + 3] == i) {
                ua[i] = j1 + 4 * j + 3;
            }
        }

        // Process remainder
        for (j = (K/4) * 4; j < K; j++) {
            if (ja[j1 + j] == i) {
                ua[i] = j1 + j;
            }
        }*/

/*
        // Second optimization
        int m = 0;
        for (j = 0; j < K/4; j++) {
            if (ja[j1 + m] == i) {
                ua[i] = j1 + m;
            }

            if (ja[j1 + m + 1] == i) {
                ua[i] = j1 + m + 1;
            }

            if (ja[j1 + m + 2] == i) {
                ua[i] = j1 + m + 2;
            }

            if (ja[j1 + m + 3] == i) {
                ua[i] = j1 + m + 3;
            }

            m += 4;
        }

        // Process remainder
        for (j = (K/4) * 4; j < K; j++) {
            if (ja[j1 + j] == i) {
                ua[i] = j1 + j;
            }
        }*/


        // Third optimization (Only one if clause can evaluate to true for one given i because there is only one diagonal element in every row)
        // Scalar replacement is useless here because there is -at most- one write to memory
        int m = 0;
        int found_diag = 0;
        for (j = 0; j < K/4; j++) {
            if (ja[j1 + m] == i) {
                ua[i] = j1 + m;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 1] == i) {
                ua[i] = j1 + m + 1;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 2] == i) {
                ua[i] = j1 + m + 2;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 3] == i) {
                ua[i] = j1 + m + 3;
                found_diag = 1;
                break;
            }

            m += 4;
        }

        // Process remainder
        if (!found_diag) {
            for (j = (K/4) * 4; j < K; j++) {
                if (ja[j1 + j] == i) {
                    ua[i] = j1 + j;
                }
            }

        }
    }
    return;
}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 8)
 *
 * For documentation see 'diagonal_pointer_csrvi_ilp_unroll'
 */
void diagonal_pointer_csrvi_ilp_unroll_8(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[]) {
    int32_t i;
    int32_t j;
    int32_t j1;
    int32_t j2;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        int K = j2 - j1;

        int m = 0;
        int found_diag = 0;
        for (j = 0; j < K/8; j++) {
            if (ja[j1 + m] == i) {
                ua[i] = j1 + m;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 1] == i) {
                ua[i] = j1 + m + 1;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 2] == i) {
                ua[i] = j1 + m + 2;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 3] == i) {
                ua[i] = j1 + m + 3;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 4] == i) {
                ua[i] = j1 + m + 4;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 5] == i) {
                ua[i] = j1 + m + 5;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 6] == i) {
                ua[i] = j1 + m + 6;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 7] == i) {
                ua[i] = j1 + m + 7;
                found_diag = 1;
                break;
            }

            m += 8;
        }

        // Process remainder
        if (!found_diag) {
            for (j = (K/8) * 8; j < K; j++) {
                if (ja[j1 + j] == i) {
                    ua[i] = j1 + j;
                }
            }

        }
    }
    return;
}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 16)
 *
 * For documentation see 'diagonal_pointer_csrvi_ilp_unroll'
 */
void diagonal_pointer_csrvi_ilp_unroll_16(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[]) {
    int32_t i;
    int32_t j;
    int32_t j1;
    int32_t j2;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        int K = j2 - j1;

        int m = 0;
        int found_diag = 0;
        for (j = 0; j < K/16; j++) {
            if (ja[j1 + m] == i) {
                ua[i] = j1 + m;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 1] == i) {
                ua[i] = j1 + m + 1;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 2] == i) {
                ua[i] = j1 + m + 2;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 3] == i) {
                ua[i] = j1 + m + 3;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 4] == i) {
                ua[i] = j1 + m + 4;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 5] == i) {
                ua[i] = j1 + m + 5;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 6] == i) {
                ua[i] = j1 + m + 6;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 7] == i) {
                ua[i] = j1 + m + 7;
                found_diag = 1;
                break;
            }
            if (ja[j1 + m + 8] == i) {
                ua[i] = j1 + m + 8;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 9] == i) {
                ua[i] = j1 + m + 9;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 10] == i) {
                ua[i] = j1 + m + 10;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 11] == i) {
                ua[i] = j1 + m + 11;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 12] == i) {
                ua[i] = j1 + m + 12;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 13] == i) {
                ua[i] = j1 + m + 13;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 14] == i) {
                ua[i] = j1 + m + 14;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 15] == i) {
                ua[i] = j1 + m + 15;
                found_diag = 1;
                break;
            }

            m += 16;
        }

        // Process remainder
        if (!found_diag) {
            for (j = (K/16) * 16; j < K; j++) {
                if (ja[j1 + j] == i) {
                    ua[i] = j1 + j;
                }
            }

        }
    }
    return;
}


/*
 * [FastCode] Matrix-Vector computation (unroll factor: 32)
 *
 * For documentation see 'diagonal_pointer_csrvi_ilp_unroll'
 */
void diagonal_pointer_csrvi_ilp_unroll_32(int32_t n, int32_t nz_num, int32_t ia[], int32_t ja[], int32_t ua[]) {
    int32_t i;
    int32_t j;
    int32_t j1;
    int32_t j2;

    for (i = 0; i < n; i++) {
        ua[i] = -1;
        j1 = ia[i];
        j2 = ia[i + 1];

        int K = j2 - j1;

        int m = 0;
        int found_diag = 0;
        for (j = 0; j < K/32; j++) {
            if (ja[j1 + m] == i) {
                ua[i] = j1 + m;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 1] == i) {
                ua[i] = j1 + m + 1;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 2] == i) {
                ua[i] = j1 + m + 2;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 3] == i) {
                ua[i] = j1 + m + 3;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 4] == i) {
                ua[i] = j1 + m + 4;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 5] == i) {
                ua[i] = j1 + m + 5;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 6] == i) {
                ua[i] = j1 + m + 6;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 7] == i) {
                ua[i] = j1 + m + 7;
                found_diag = 1;
                break;
            }
            if (ja[j1 + m + 8] == i) {
                ua[i] = j1 + m + 8;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 9] == i) {
                ua[i] = j1 + m + 9;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 10] == i) {
                ua[i] = j1 + m + 10;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 11] == i) {
                ua[i] = j1 + m + 11;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 12] == i) {
                ua[i] = j1 + m + 12;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 13] == i) {
                ua[i] = j1 + m + 13;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 14] == i) {
                ua[i] = j1 + m + 14;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 15] == i) {
                ua[i] = j1 + m + 15;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 16] == i) {
                ua[i] = j1 + m + 16;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 17] == i) {
                ua[i] = j1 + m + 17;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 18] == i) {
                ua[i] = j1 + m + 18;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 19] == i) {
                ua[i] = j1 + m + 19;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 20] == i) {
                ua[i] = j1 + m + 20;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 21] == i) {
                ua[i] = j1 + m + 21;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 22] == i) {
                ua[i] = j1 + m + 22;
                found_diag = 1;
                break;
            }
            if (ja[j1 + m + 23] == i) {
                ua[i] = j1 + m + 23;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 24] == i) {
                ua[i] = j1 + m + 24;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 25] == i) {
                ua[i] = j1 + m + 25;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 26] == i) {
                ua[i] = j1 + m + 26;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 27] == i) {
                ua[i] = j1 + m + 27;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 28] == i) {
                ua[i] = j1 + m + 28;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 29] == i) {
                ua[i] = j1 + m + 29;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 30] == i) {
                ua[i] = j1 + m + 30;
                found_diag = 1;
                break;
            }

            if (ja[j1 + m + 31] == i) {
                ua[i] = j1 + m + 31;
                found_diag = 1;
                break;
            }

            m += 32;
        }

        // Process remainder
        if (!found_diag) {
            for (j = (K/32) * 32; j < K; j++) {
                if (ja[j1 + j] == i) {
                    ua[i] = j1 + j;
                }
            }

        }
    }
    return;
}

