/**
 *      _________   _____________________  ____  ______
 *     / ____/   | / ___/_  __/ ____/ __ \/ __ \/ ____/
 *    / /_  / /| | \__ \ / / / /   / / / / / / / __/
 *   / __/ / ___ |___/ // / / /___/ /_/ / /_/ / /___
 *  /_/   /_/  |_/____//_/  \____/\____/_____/_____/
 *
 *  http://www.inf.ethz.ch/personal/markusp/teaching/
 *  How to Write Fast Numerical Code 263-2300 - ETH Zurich
 *  Copyright (C) 2016  Alen Stojanov      (astojanov@inf.ethz.ch)
 *                      Daniele Spampinato (daniele.spampinato@inf.ethz.ch)
 *                      Singh Gagandeep    (gsingh@inf.ethz.ch)
 *	                    Markus Pueschel    (pueschel@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef VALIDATE_H
#define VALIDATE_H

void validate (int n, int nz_num, int ia[], int ja[], double a[],
               double x[], double rhs[], int itr_max, int mr, double tol_abs,
               double tol_rel);
               
//validate_csrvi(N, nz_num, ia_csrvi, ja_csrvi, va_csrvi, vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);
void validate_csrvi(int n, int nz_num, int32_t ia[], int32_t ja[], int32_t va[],
               double vala[], double x[], double rhs[], int itr_max, int mr, double tol_abs,
               double tol_rel);

//validate_csrvi(N, nz_num, int32_t *ia_csrvi, int32_t *ja_csrvi, int32_t *va_csrvi, double *vala_csrvi, x_estimate, rhs, itr_max, mr, tol_abs, tol_rel);

void validate_csrvi_vec(int n, int nz_num, int32_t ia_csr[], int32_t ja_csr[], int32_t a_csr[], int32_t ia_csrvi[], int32_t ja_csrvi[], int va_csrvi[], double vala_csrvi[], double x_estimate[], double rhs[], int itr_max, int mr, double tol_abs, double tol_rel);

#endif /* VALIDATE_H */

